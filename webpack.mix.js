const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/pages/home.js', 'public/js/pages');
mix.js('resources/js/pages/new_element.js', 'public/js/pages');
mix.js('resources/js/pages/manage_packages.js', 'public/js/pages');

mix.js('resources/js/pages/note_form.js', 'public/js/pages');
mix.js('resources/js/pages/calculators.js', 'public/js/pages');

mix.sass('resources/sass/manage_packages.scss', 'public/css');
mix.sass('resources/sass/midi_to_rttl.scss', 'public/css');

mix.vue();

mix.sourceMaps(false);

mix.version();
