<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.04.2021
 * Time: 15:21
 */


namespace Core\Elements;


use Core\Elements\Package\Package;
use Core\Elements\Type\Type;
use JsonSerializable;

class ElementData implements JsonSerializable {
  private Element $element;
  private Type $type;
  private Package $package;
  private Qty $qty;

  /**
   * Result constructor.
   *
   * @param Element $element
   * @param Type $type
   * @param Package $package
   * @param Qty $qty
   */
  public function __construct(Element $element, Type $type, Package $package, Qty $qty) {
    $this->element = $element;
    $this->type = $type;
    $this->package = $package;
    $this->qty = $qty;
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return Type
   */
  public function getType(): Type {
    return $this->type;
  }

  /**
   * @return Package
   */
  public function getPackage(): Package {
    return $this->package;
  }

  /**
   * @return Qty
   */
  public function getQty(): Qty {
    return $this->qty;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'element' => $this->element,
      'type' => $this->type,
      'package' => $this->package,
      'qty' => $this->qty,
    ];
  }
}
