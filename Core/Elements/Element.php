<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:45
 */


namespace Core\Elements;


use Core\Elements\Type\TypeId;
use JsonSerializable;

class Element implements JsonSerializable {

  private ElementId $id;
  private string $title;
  private TypeId $type_id;


  /**
   * Element constructor.
   *
   * @param ElementId $id
   * @param string $title
   * @param TypeId $type_id
   */
  public function __construct(ElementId $id, string $title, TypeId $type_id) {
    $this->id = $id;
    $this->title = $title;
    $this->type_id = $type_id;
  }

  /**
   * @return ElementId
   */
  public function getId(): ElementId {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return TypeId
   */
  public function getTypeId(): TypeId {
    return $this->type_id;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'id' => (string)$this->getId(),
      'title' => $this->getTitle(),
      'type_id' => (string)$this->getTypeId(),
    ];
  }
}
