<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.03.2021
 * Time: 5:49
 */


namespace Core\Elements;


use DomainException;

/**
 * Class Series
 *
 * @package Core\Elements
 */
class Series {

  public const E24 = [
    1.0,
    1.1,
    1.2,
    1.3,
    1.5,
    1.6,
    1.8,
    2.0,
    2.2,
    2.4,
    2.7,
    3.0,
    3.3,
    3.6,
    3.9,
    4.3,
    4.7,
    5.1,
    5.6,
    6.2,
    6.8,
    7.5,
    8.2,
    9.1,
  ];

  public const SERIES = [
    'E24',
  ];

  public const UNITS = [
    '',
    'K',
    'M',
  ];

  /**
   * @param string $series
   * @param string $unit
   * @param int $multiplier
   *
   * @return array|float[]
   */
  public function generateNamesBySeries(string $series, string $unit = '', int $multiplier = 1): array {
    if (!in_array($series, self::SERIES)) {
      throw new DomainException("Ряд '$series' не реализован.");
    }

    return $this->generateNamesE24($unit, $multiplier);
  }

  /**
   * @param string $unit
   * @param int $multiplier
   *
   * @return array|float[]
   */
  public function generateNamesE24(string $unit = '', int $multiplier = 1): array {
    if ('' !== $unit && !in_array($unit, self::UNITS)) {
      throw new DomainException("Ед. '$unit' недопустима.");
    }
    if ('' === $unit && 1 === $multiplier) {
      return self::E24;
    }

    $names = [];
    foreach(self::E24 as $value) {
      $names[] = ($value * $multiplier) . $unit;
    }
    return $names;
  }
}
