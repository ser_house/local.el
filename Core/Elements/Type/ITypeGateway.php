<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:08
 */


namespace Core\Elements\Type;


interface ITypeGateway {
  /**
   * @param Type $type
   */
  public function add(Type $type): void;

  /**
   * @param TypeId $typeId
   *
   * @return Type|null
   */
  public function getById(TypeId $typeId): ?Type;

  /**
   * @param string $title
   *
   * @return Type|null
   */
  public function getByTitle(string $title): ?Type;

  /**
   * @param TypeId $typeId
   *
   * @return int
   */
  public function getElementsCount(TypeId $typeId): int;

  /**
   * @param TypeId $typeId
   *
   * @return TypeId[]
   */
  public function getChildrenIds(TypeId $typeId): array;

  /**
   *
   * @return array
   */
  public function allAsFlat(): array;

  /**
   *
   * @return array
   */
  public function allAsTree(): array;

  /**
   * @param Type $type
   */
  public function update(Type $type): void;

  /**
   * @param TypeId $typeId
   */
  public function remove(TypeId $typeId): void;
}
