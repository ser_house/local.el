<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 9:56
 */


namespace Core\Elements\Type;


use Core\Elements\Units\UnitsType;
use JsonSerializable;

class Type implements JsonSerializable {

  private TypeId $id;
  private ?TypeId $parentId;
  private string $title;
  private bool $has_series;
  private ?UnitsType $unitsType;

  /**
   * Type constructor.
   *
   * @param TypeId $id
   * @param TypeId|null $parentId
   * @param string $title
   * @param bool $has_series
   * @param UnitsType|null $unitsType
   */
  public function __construct(TypeId $id, ?TypeId $parentId, string $title, bool $has_series, ?UnitsType $unitsType) {
    $this->id = $id;
    $this->parentId = $parentId;
    $this->title = $title;
    $this->has_series = $has_series;
    $this->unitsType = $unitsType;
  }


  /**
   * @return TypeId
   */
  public function id(): TypeId {
    return $this->id;
  }

  /**
   * @return string
   */
  public function title(): string {
    return $this->title;
  }

  /**
   * @return bool
   */
  public function hasSeries(): bool {
    return $this->has_series;
  }

  /**
   * @return TypeId|null
   */
  public function parentId(): ?TypeId {
    return $this->parentId;
  }

  /**
   * @return UnitsType|null
   */
  public function unitsType(): ?UnitsType {
    return $this->unitsType;
  }

  /**
   * @param Type $type
   *
   * @return bool
   */
  public function equals(Type $type): bool {
    $is_equals = $this->id()->equals($type->id())
      && $this->title() === $type->title()
      && $this->hasSeries() === $type->hasSeries();

    $thisParentId = $this->parentId();
    $typeParentId = $type->parentId();
    if ($thisParentId && $typeParentId) {
      $is_parents_equals = $thisParentId->equals($typeParentId);
    }
    elseif (!$thisParentId && !$typeParentId) {
      $is_parents_equals = true;
    }
    else {
      $is_parents_equals = false;
    }

    $thisUnitsType = $this->unitsType();
    $typeUnitsType = $type->unitsType();
    if ($thisUnitsType && $typeUnitsType) {
      $is_units_type_equals = $thisUnitsType->equals($typeUnitsType);
    }
    elseif (!$thisUnitsType && !$typeUnitsType) {
      $is_units_type_equals = true;
    }
    else {
      $is_units_type_equals = false;
    }

    return $is_equals && $is_parents_equals && $is_units_type_equals;
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->title;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'id' => (string)$this->id(),
      'title' => $this->title(),
      'has_series' => $this->hasSeries(),
      'parent_id' => (string)$this->parentId(),
      'units_type' => (string)$this->unitsType(),
    ];
  }
}
