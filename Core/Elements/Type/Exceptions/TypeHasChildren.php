<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 17:21
 */


namespace Core\Elements\Type\Exceptions;


use Core\Elements\Type\Type;
use DomainException;
use Throwable;

final class TypeHasChildren extends DomainException {
  /**
   * @inheritDoc
   */
  public function __construct(Type $type, $code = 0, Throwable $previous = null) {
    $type_title = $type->title();
    $message = "Невозможно удалить тип '$type_title': есть подтипы.";
    parent::__construct($message, $code, $previous);
  }
}
