<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 17:21
 */


namespace Core\Elements\Type\Exceptions;


use DomainException;
use Throwable;

final class TypeNotFound extends DomainException {
  /**
   * @inheritDoc
   */
  public function __construct($message = '', $code = 0, Throwable $previous = null) {
    if (empty($message)) {
      $message = 'Тип не найден.';
    }

    parent::__construct($message, $code, $previous);
  }

}
