<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 11:50
 */


namespace Core\Elements\Type\UseCase\Change;


use Core\Elements\Type\Type;

class Result {

  private Type $originalType;
  private Type $changedType;

  /**
   * Result constructor.
   *
   * @param Type $originalType
   * @param Type $changedType
   */
  public function __construct(Type $originalType, Type $changedType) {
    $this->originalType = $originalType;
    $this->changedType = $changedType;
  }

  /**
   * @return Type
   */
  public function getOriginalType(): Type {
    return $this->originalType;
  }

  /**
   * @return Type
   */
  public function getChangedType(): Type {
    return $this->changedType;
  }
}
