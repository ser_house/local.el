<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 10:05
 */


namespace Core\Elements\Type\UseCase\Change;


use Core\Elements\Type\Exceptions\TypeNotFound;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;
use Core\Elements\Units\UnitsType;
use DomainException;

class Input {
  private Type $type;
  private string $new_title;
  private bool $has_series;
  private ?UnitsType $unitsType;


  /**
   * Input constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param string $type_id
   * @param string $new_title
   * @param bool $has_series
   * @param string|null $units_type
   */
  public function __construct(ITypeGateway $typeGateway, string $type_id, string $new_title, bool $has_series, string $units_type = null) {
    if (empty($new_title)) {
      throw new DomainException('Название корпуса не может быть пустым.');
    }

    $type = $typeGateway->getById(new TypeId($type_id));
    if (null === $type) {
      throw new TypeNotFound();
    }

    $unitsType = $units_type ? new UnitsType($units_type) : null;

    $typeUnitsCode = $type->unitsType();
    if ($unitsType && $typeUnitsCode) {
      $is_units_type_equals = $unitsType->equals($typeUnitsCode);
    }
    elseif (!$unitsType && !$typeUnitsCode) {
      $is_units_type_equals = true;
    }
    else {
      $is_units_type_equals = false;
    }

    if ($type->title() === $new_title && $type->hasSeries() === $has_series && $is_units_type_equals) {
      throw new DomainException("Тип с названием '$new_title' уже существует и у него такие же флаг значений по рядам и единицы измерения.");
    }

    $this->type = $type;
    $this->new_title = $new_title;
    $this->has_series = $has_series;
    $this->unitsType = $unitsType;
  }

  /**
   * @return Type
   */
  public function getType(): Type {
    return $this->type;
  }

  /**
   * @return string
   */
  public function getNewTitle(): string {
    return $this->new_title;
  }

  /**
   * @return bool
   */
  public function hasSeries(): bool {
    return $this->has_series;
  }

  /**
   * @return UnitsType|null
   */
  public function unitsType(): ?UnitsType {
    return $this->unitsType;
  }
}
