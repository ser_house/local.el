<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\Type\UseCase\Change;


use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\Type;
use Core\Infrastructure\ITransaction;
use Throwable;

class Change {

  private ITypeGateway $typeGateway;
  private ITransaction $transaction;

  /**
   * Rename constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param ITransaction $transaction
   */
  public function __construct(ITypeGateway $typeGateway, ITransaction $transaction) {
    $this->typeGateway = $typeGateway;
    $this->transaction = $transaction;
  }

  /**
   * @param Input $input
   *
   * @return Result
   * @throws Throwable
   */
  public function run(Input $input): Result {
    $type = $input->getType();
    $new_title = $input->getNewTitle();

    $changedType = new Type($type->id(), $type->parentId(), $new_title, $input->hasSeries(), $input->unitsType());

    $this->transaction->begin();
    try {
      $this->typeGateway->update($changedType);
      $this->transaction->commit();

      return new Result($type, $changedType);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
