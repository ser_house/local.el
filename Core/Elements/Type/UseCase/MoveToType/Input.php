<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 10:05
 */


namespace Core\Elements\Type\UseCase\MoveToType;


use Core\Elements\Type\Exceptions\TypeNotFound;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;

class Input {
  private Type $type;
  private ?Type $parent;


  public function __construct(ITypeGateway $typeGateway, string $id, string $parent_id = null) {

    $typeId = new TypeId($id);
    $type = $typeGateway->getById($typeId);
    if (null === $type) {
      throw new TypeNotFound('Перемещаемый тип не найден.');
    }

    $parent = null;
    if (!empty($parent_id)) {
      $parent = $typeGateway->getById(new TypeId($parent_id));
      if (null === $parent) {
        throw new TypeNotFound('Тип назначения не найден.');
      }
    }

    $this->type = $type;
    $this->parent = $parent;
  }

  /**
   * @return Type
   */
  public function getType(): Type {
    return $this->type;
  }

  /**
   * @return Type|null
   */
  public function getParent(): ?Type {
    return $this->parent;
  }
}
