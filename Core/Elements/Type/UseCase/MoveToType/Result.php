<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 10:05
 */


namespace Core\Elements\Type\UseCase\MoveToType;


use Core\Elements\Type\Type;

class Result {
  private Type $type;
  private ?Type $parent;


  public function __construct(Type $type, ?Type $parent) {
    $this->type = $type;
    $this->parent = $parent;
  }

  /**
   * @return Type
   */
  public function getType(): Type {
    return $this->type;
  }

  /**
   * @return Type|null
   */
  public function getParent(): ?Type {
    return $this->parent;
  }
}
