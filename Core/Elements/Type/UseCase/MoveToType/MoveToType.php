<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\Type\UseCase\MoveToType;


use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\Type;
use Core\Infrastructure\ITransaction;
use Throwable;

class MoveToType {

  private ITypeGateway $typeGateway;
  private ITransaction $transaction;

  /**
   * MoveToType constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param ITransaction $transaction
   */
  public function __construct(ITypeGateway $typeGateway, ITransaction $transaction) {
    $this->typeGateway = $typeGateway;
    $this->transaction = $transaction;
  }

  /**
   * @param Input $input
   *
   * @return Result
   * @throws Throwable
   */
  public function run(Input $input): Result {
    $type = $input->getType();
    $parent = $input->getParent();

    $type = new Type($type->id(), $parent ? $parent->id() : null, $type->title(), $type->hasSeries(), $type->unitsType());
    $this->transaction->begin();
    try {
      $this->typeGateway->update($type);
      $this->transaction->commit();

      return new Result($type, $parent);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
