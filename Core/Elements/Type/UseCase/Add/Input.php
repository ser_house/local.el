<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 10:05
 */


namespace Core\Elements\Type\UseCase\Add;


use Core\Elements\Type\Exceptions\TypeNotFound;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\TypeId;
use Core\Elements\Units\UnitsType;
use DomainException;

class Input {

  private ?TypeId $parentId;
  private string $title;
  private bool $has_series;
  private ?UnitsType $unitsType;


  /**
   * Input constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param string $title
   * @param bool $has_series
   * @param string|null $parent_id
   * @param string|null $units_type
   */
  public function __construct(ITypeGateway $typeGateway, string $title, bool $has_series, string $parent_id = null, string $units_type = null) {
    if (empty($title)) {
      throw new DomainException('Название типа не может быть пустым.');
    }

    $this->parentId = null;
    if (!empty($parent_id)) {
      $this->parentId = new TypeId($parent_id);
      $parentType = $typeGateway->getById($this->parentId);
      if (null === $parentType) {
        throw new TypeNotFound('Родительский тип не найден.');
      }
    }

    $this->unitsType = $units_type ? new UnitsType($units_type) : null;

    $this->title = $title;
    $this->has_series = $has_series;
  }

  /**
   * @return TypeId|null
   */
  public function getParentId(): ?TypeId {
    return $this->parentId;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return bool
   */
  public function hasSeries(): bool {
    return $this->has_series;
  }

  /**
   * @return UnitsType|null
   */
  public function unitsType(): ?UnitsType {
    return $this->unitsType;
  }
}
