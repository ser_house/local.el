<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\Type\UseCase\Add;


use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;
use Core\Infrastructure\ITransaction;
use DomainException;
use Throwable;

class Add {

  private ITypeGateway $typeGateway;
  private ITransaction $transaction;

  /**
   * Add constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param ITransaction $transaction
   */
  public function __construct(ITypeGateway $typeGateway, ITransaction $transaction) {
    $this->typeGateway = $typeGateway;
    $this->transaction = $transaction;
  }


  public function run(Input $input): Type {
    $title = $input->getTitle();
    $type = $this->typeGateway->getByTitle($title);
    if (null !== $type) {
      throw new DomainException("Корпус с таким названием ('$title') уже существует.");
    }

    $type = new Type(new TypeId(), $input->getParentId(), $title, $input->hasSeries(), $input->unitsType());
    $this->transaction->begin();
    try {
      $this->typeGateway->add($type);
      $this->transaction->commit();

      return $type;
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
