<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\Type\UseCase\Delete;


use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\Type;
use Core\Infrastructure\ITransaction;
use Throwable;

class Delete {

  private ITypeGateway $typeGateway;
  private ITransaction $transaction;

  /**
   * Delete constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param ITransaction $transaction
   */
  public function __construct(ITypeGateway $typeGateway, ITransaction $transaction) {
    $this->typeGateway = $typeGateway;
    $this->transaction = $transaction;
  }

  /**
   * @param Input $input
   *
   * @return Type
   * @throws Throwable
   */
  public function run(Input $input): Type {
    $type = $input->getType();

    $this->transaction->begin();
    try {
      $this->typeGateway->remove($type->id());
      $this->transaction->commit();

      return $type;
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
