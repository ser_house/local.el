<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 10:05
 */


namespace Core\Elements\Type\UseCase\Delete;


use Core\Elements\Type\Exceptions\TypeHasChildren;
use Core\Elements\Type\Exceptions\TypeHasElements;
use Core\Elements\Type\Exceptions\TypeNotFound;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;

class Input {
  private Type $type;

  /**
   * Input constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param TypeId $typeId
   */
  public function __construct(ITypeGateway $typeGateway, TypeId $typeId) {
    $type = $typeGateway->getById($typeId);
    if (null === $type) {
      throw new TypeNotFound();
    }

    if ($typeGateway->getElementsCount($typeId) > 0) {
      throw new TypeHasElements($type);
    }

    $children_ids = $typeGateway->getChildrenIds($typeId);
    if (count($children_ids) > 0) {
      throw new TypeHasChildren($type);
    }

    $this->type = $type;
  }

  /**
   * @return Type
   */
  public function getType(): Type {
    return $this->type;
  }
}
