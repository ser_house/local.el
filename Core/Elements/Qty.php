<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 9:56
 */


namespace Core\Elements;


use DomainException;
use Exception;
use JsonSerializable;

class Qty implements JsonSerializable {

  private int $value;

  /**
   * Qty constructor.
   *
   * @param string $value
   *
   * @throws Exception
   */
  public function __construct(string $value = '') {
    if (empty($value)) {
      $this->value = 0;
      return;
    }
    if (!is_numeric($value)) {
      throw new Exception("Invalid qty value: $value");
    }

    $this->value = (int)$value;
  }

  /**
   * @param Qty $qty
   *
   * @return Qty
   * @throws Exception
   */
  public function add(Qty $qty): Qty {
    return new Qty($this->value + $qty->asInt());
  }

  /**
   * @param Qty $qty
   *
   * @return Qty
   * @throws Exception
   */
  public function subtract(Qty $qty): Qty {
    $new_value = $this->value - $qty->asInt();
    if ($new_value < 0) {
      throw new DomainException("Результат не может быть отрицательным: $this->value - {$qty->asInt()} = $new_value");
    }
    return new Qty($new_value);
  }

  /**
   * @return int
   */
  public function asInt(): int {
    return $this->value;
  }

  /**
   * @param Qty $value
   *
   * @return bool
   */
  public function equals(Qty $value): bool {
    return $this->asInt() === $value->asInt();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return (string)$this->asInt();
  }

  /**
   *
   * @return bool
   * @throws Exception
   */
  public function isEmpty(): bool {
    return $this->equals(new Qty(0));
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return $this->asInt();
  }
}
