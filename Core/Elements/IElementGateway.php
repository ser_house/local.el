<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 9:05
 */


namespace Core\Elements;


use Core\Elements\Package\PackageId;
use Core\Elements\Type\TypeId;

interface IElementGateway {
  /**
   * @param Element $element
   */
  public function add(Element $element): void;

  /**
   * @param ElementId $elementId
   *
   * @return Element|null
   */
  public function getById(ElementId $elementId): ?Element;

  /**
   * @param string $title
   * @param TypeId $typeId
   *
   * @return Element|null
   */
  public function getByTitleAndType(string $title, TypeId $typeId): ?Element;

  /**
   * @param TypeId $typeId
   *
   * @return ElementData[]
   */
  public function listByType(TypeId $typeId): array;

  /**
   * @param PackageId $packageId
   *
   * @return array
   */
  public function listByPackage(PackageId $packageId): array;

  /**
   * @param ElementId $elementId
   */
  public function remove(ElementId $elementId): void;

  /**
   * @param Element $element
   */
  public function update(Element $element): void;
}
