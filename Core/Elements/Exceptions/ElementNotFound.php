<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 17:21
 */


namespace Core\Elements\Exceptions;


use DomainException;
use Throwable;

final class ElementNotFound extends DomainException {
  /**
   * @inheritDoc
   */
  public function __construct($message = '', $code = 0, Throwable $previous = null) {
    if (empty($message)) {
      $message = 'Компонент не найден.';
    }

    parent::__construct($message, $code, $previous);
  }

}
