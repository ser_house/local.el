<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:06
 */


namespace Core\Elements\UseCase\ReplacePackage;


use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Storage\IStorageGateway;

final class Context {
  private IElementGateway $elementGateway;
  private IPackageGateway $packageGateway;
  private IStorageGateway $storageGateway;

  /**
   * Context constructor.
   *
   * @param IElementGateway $elementGateway
   * @param IPackageGateway $packageGateway
   * @param IStorageGateway $storageGateway
   */
  public function __construct(IElementGateway $elementGateway, IPackageGateway $packageGateway, IStorageGateway $storageGateway) {
    $this->elementGateway = $elementGateway;
    $this->packageGateway = $packageGateway;
    $this->storageGateway = $storageGateway;
  }

  /**
   * @return IElementGateway
   */
  public function getElementGateway(): IElementGateway {
    return $this->elementGateway;
  }

  /**
   * @return IPackageGateway
   */
  public function getPackageGateway(): IPackageGateway {
    return $this->packageGateway;
  }

  /**
   * @return IStorageGateway
   */
  public function getStorageGateway(): IStorageGateway {
    return $this->storageGateway;
  }
}
