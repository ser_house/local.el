<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 17:35
 */


namespace Core\Elements\UseCase\ReplacePackage;


use Core\Elements\Element;
use Core\Elements\Package\Package;

class Result {
  private Element $element;
  private Package $currentPackage;
  private Package $newPackage;

  /**
   * Result constructor.
   *
   * @param Element $element
   * @param Package $currentPackage
   * @param Package $newPackage
   */
  public function __construct(Element $element, Package $currentPackage, Package $newPackage) {
    $this->element = $element;
    $this->currentPackage = $currentPackage;
    $this->newPackage = $newPackage;
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return Package
   */
  public function getCurrentPackage(): Package {
    return $this->currentPackage;
  }

  /**
   * @return Package
   */
  public function getNewPackage(): Package {
    return $this->newPackage;
  }
}
