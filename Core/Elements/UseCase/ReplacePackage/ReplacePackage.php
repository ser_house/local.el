<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.04.2021
 * Time: 9:21
 */


namespace Core\Elements\UseCase\ReplacePackage;


use Core\Elements\Storage\IStorageGateway;
use Core\Infrastructure\ITransaction;
use Throwable;

class ReplacePackage {
  private IStorageGateway $storageGateway;
  private ITransaction $transaction;

  /**
   * ReplacePackage constructor.
   *
   * @param IStorageGateway $storageGateway
   * @param ITransaction $transaction
   */
  public function __construct(IStorageGateway $storageGateway, ITransaction $transaction) {
    $this->storageGateway = $storageGateway;
    $this->transaction = $transaction;
  }


  public function run(Input $input): Result {

    $element = $input->getElement();
    $currentPackage = $input->getCurrentPackage();
    $newPackage = $input->getNewPackage();

    $this->transaction->begin();
    try {
      $this->storageGateway->replacePackage($element->getId(), $currentPackage->getId(), $newPackage->getId());
      $this->transaction->commit();

      return new Result($element, $currentPackage, $newPackage);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
