<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 13:25
 */


namespace Core\Elements\UseCase\ReplacePackage;


use Core\Elements\Element;
use Core\Elements\ElementId;
use Core\Elements\Exceptions\ElementNotFound;
use Core\Elements\Package\Exceptions\PackageNotFound;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;
use DomainException;

class Input {

  private Element $element;
  private Package $currentPackage;
  private Package $newPackage;

  /**
   * Input constructor.
   *
   * @param Context $context
   * @param string $element_id
   * @param string $current_package_id
   * @param string $new_package_id
   */
  public function __construct(Context $context, string $element_id, string $current_package_id, string $new_package_id) {
    $elementId = new ElementId($element_id);
    $element = $context->getElementGateway()->getById($elementId);
    if (null === $element) {
      throw new ElementNotFound();
    }

    $currentPackageId = new PackageId($current_package_id);
    $currentPackage = $context->getPackageGateway()->getById($currentPackageId);
    if (null === $currentPackage) {
      throw new PackageNotFound('Текущий корпус не найден.');
    }

    $newPackageId = new PackageId($new_package_id);
    $newPackage = $context->getPackageGateway()->getById($newPackageId);
    if (null === $newPackage) {
      throw new PackageNotFound('Новый корпус не найден.');
    }

    $qty = $context->getStorageGateway()->getQty($elementId, $currentPackageId);
    if (null === $qty) {
      $element_title = $element->getTitle();
      $current_package_title = $currentPackage->getTitle();
      $msg = "Компонент '$element_title' в корпусе '$current_package_title' не найден.";
      throw new DomainException($msg);
    }

    $this->element = $element;
    $this->currentPackage = $currentPackage;
    $this->newPackage = $newPackage;
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return Package
   */
  public function getCurrentPackage(): Package {
    return $this->currentPackage;
  }

  /**
   * @return Package
   */
  public function getNewPackage(): Package {
    return $this->newPackage;
  }
}
