<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:02
 */


namespace Core\Elements\UseCase\Rename;


use Core\Elements\Element;
use Core\Infrastructure\ITransaction;
use Throwable;

class Rename {

  private ITransaction $transaction;

  /**
   * Rename constructor.
   *
   * @param ITransaction $transaction
   */
  public function __construct(ITransaction $transaction) {
    $this->transaction = $transaction;
  }

  /**
   * @param Context $context
   * @param Input $input
   *
   * @return Result
   * @throws Throwable
   */
  public function run(Context $context, Input $input): Result {
    $element = $input->getElement();

    $renamedElement = new Element($element->getId(), $input->getNewTitle(), $element->getTypeId());

    $this->transaction->begin();
    try {
      $context->getElementGateway()->update($renamedElement);
      $this->transaction->commit();

      return new Result($renamedElement, $element->getTitle());
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
