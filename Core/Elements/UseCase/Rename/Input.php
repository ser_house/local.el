<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:01
 */


namespace Core\Elements\UseCase\Rename;


use Core\Elements\Element;
use Core\Elements\ElementId;
use Core\Elements\Exceptions\ElementNotFound;
use DomainException;

final class Input {
  private Element $element;
  private string $new_title;

  /**
   * Input constructor.
   *
   * @param Context $context
   * @param string $element_id
   * @param string $new_title
   */
  public function __construct(Context $context, string $element_id, string $new_title) {
    $elementId = new ElementId($element_id);
    $element = $context->getElementGateway()->getById($elementId);
    if (null === $element) {
      throw new ElementNotFound();
    }

    if (empty($new_title)) {
      throw new DomainException('Новое название компонента не может быть пустым.');
    }

    $current_title = $element->getTitle();
    if ($new_title === $element->getTitle()) {
      throw new DomainException("Название не может быть изменено, поскольку текущее название '$current_title' совпадает с новым '$new_title'.");
    }

    $this->element = $element;
    $this->new_title = $new_title;
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return string
   */
  public function getNewTitle(): string {
    return $this->new_title;
  }
}
