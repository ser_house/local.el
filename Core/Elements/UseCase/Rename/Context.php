<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:06
 */


namespace Core\Elements\UseCase\Rename;


use Core\Elements\IElementGateway;

final class Context {
  private IElementGateway $elementGateway;

  /**
   * Context constructor.
   *
   * @param IElementGateway $elementGateway
   */
  public function __construct(IElementGateway $elementGateway) {
    $this->elementGateway = $elementGateway;
  }

  /**
   * @return IElementGateway
   */
  public function getElementGateway(): IElementGateway {
    return $this->elementGateway;
  }
}
