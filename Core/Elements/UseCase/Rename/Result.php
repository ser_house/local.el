<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:03
 */


namespace Core\Elements\UseCase\Rename;


use Core\Elements\Element;

class Result {

  private Element $element;
  private string $old_title;

  /**
   * Result constructor.
   *
   * @param Element $element
   * @param string $old_title
   */
  public function __construct(Element $element, string $old_title) {
    $this->element = $element;
    $this->old_title = $old_title;
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return string
   */
  public function getOldTitle(): string {
    return $this->old_title;
  }
}
