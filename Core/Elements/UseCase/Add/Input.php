<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 13:25
 */


namespace Core\Elements\UseCase\Add;


use Core\Elements\Package\Exceptions\PackageNotFound;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;
use Core\Elements\Qty;
use Core\Elements\Type\Exceptions\TypeNotFound;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;
use DomainException;
use Exception;

class Input {

  private string $title;
  private Type $type;
  private Package $package;
  private Qty $qty;

  /**
   * Input constructor.
   *
   * @param Context $context
   * @param string $title
   * @param string $type_id
   * @param string $package_id
   * @param int $qty
   *
   * @throws Exception
   */
  public function __construct(Context $context, string $title, string $type_id, string $package_id, int $qty) {

    if (empty($title)) {
      throw new DomainException('Новое название компонента не может быть пустым.');
    }

    $typeId = new TypeId($type_id);
    $type = $context->getTypeGateway()->getById($typeId);
    if (null === $type) {
      throw new TypeNotFound();
    }

    $packageId = new PackageId($package_id);
    $package = $context->getPackageGateway()->getById($packageId);
    if (null === $package) {
      throw new PackageNotFound();
    }

    $this->title = $title;
    $this->type = $type;
    $this->package = $package;
    $this->qty = new Qty($qty);
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return Type
   */
  public function getType(): Type {
    return $this->type;
  }

  /**
   * @return Package
   */
  public function getPackage(): Package {
    return $this->package;
  }

  /**
   * @return Qty
   */
  public function getQty(): Qty {
    return $this->qty;
  }
}
