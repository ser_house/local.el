<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\UseCase\Add;


use Core\Elements\Element;
use Core\Elements\ElementData;
use Core\Elements\ElementId;
use Core\Elements\IElementGateway;
use Core\Elements\Storage\IStorageGateway;
use Core\Infrastructure\ITransaction;
use Throwable;

class Add {

  private IElementGateway $elementGateway;
  private IStorageGateway $storageGateway;
  private ITransaction $transaction;

  /**
   * Add constructor.
   *
   * @param IElementGateway $elementGateway
   * @param IStorageGateway $storageGateway
   * @param ITransaction $transaction
   */
  public function __construct(IElementGateway $elementGateway, IStorageGateway $storageGateway, ITransaction $transaction) {
    $this->elementGateway = $elementGateway;
    $this->storageGateway = $storageGateway;
    $this->transaction = $transaction;
  }


  public function run(Input $input): ElementData {
    $qtyToAdd = $input->getQty();
    $newQty = $qtyToAdd;

    $type = $input->getType();
    $typeId = $type->id();
    $element = $this->elementGateway->getByTitleAndType($input->getTitle(), $typeId);

    $package = $input->getPackage();
    $packageId = $package->getId();
    $this->transaction->begin();
    try {
      if ($element) {
        $elementId = $element->getId();
        $currentQty = $this->storageGateway->getQty($elementId, $packageId);
        if ($currentQty) {
          $newQty = $currentQty->add($qtyToAdd);
          $this->storageGateway->updateQty($elementId, $packageId, $newQty);
        }
        else {
          $this->storageGateway->addQty($elementId, $packageId, $newQty);
        }
      }
      else {
        $id = new ElementId();
        $element = new Element($id, $input->getTitle(), $typeId);
        $this->elementGateway->add($element);

        $this->storageGateway->addQty($id, $packageId, $newQty);
      }
      $this->transaction->commit();

      return new ElementData($element, $type, $package, $newQty);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
