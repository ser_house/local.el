<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:06
 */


namespace Core\Elements\UseCase\Add;

use Core\Elements\Package\IPackageGateway;
use Core\Elements\Type\ITypeGateway;

final class Context {
  private ITypeGateway $typeGateway;
  private IPackageGateway $packageGateway;

  /**
   * Context constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param IPackageGateway $packageGateway
   */
  public function __construct(ITypeGateway $typeGateway, IPackageGateway $packageGateway) {
    $this->typeGateway = $typeGateway;
    $this->packageGateway = $packageGateway;
  }

  /**
   * @return ITypeGateway
   */
  public function getTypeGateway(): ITypeGateway {
    return $this->typeGateway;
  }

  /**
   * @return IPackageGateway
   */
  public function getPackageGateway(): IPackageGateway {
    return $this->packageGateway;
  }
}
