<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:06
 */


namespace Core\Elements\UseCase\Merge;


use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;

final class Context {
  private IElementGateway $elementGateway;
  private IPackageGateway $packageGateway;

  /**
   * Context constructor.
   *
   * @param IElementGateway $elementGateway
   * @param IPackageGateway $packageGateway
   */
  public function __construct(IElementGateway $elementGateway, IPackageGateway $packageGateway) {
    $this->elementGateway = $elementGateway;
    $this->packageGateway = $packageGateway;
  }

  /**
   * @return IElementGateway
   */
  public function getElementGateway(): IElementGateway {
    return $this->elementGateway;
  }

  /**
   * @return IPackageGateway
   */
  public function getPackageGateway(): IPackageGateway {
    return $this->packageGateway;
  }
}
