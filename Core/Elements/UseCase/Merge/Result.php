<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 17:35
 */


namespace Core\Elements\UseCase\Merge;


use Core\Elements\Element;
use Core\Elements\Qty;

class Result {
  private Element $srcElement;
  private Element $targetElement;
  private Qty $qty;

  /**
   * Result constructor.
   *
   * @param Element $srcElement
   * @param Element $targetElement
   * @param Qty $qty
   */
  public function __construct(Element $srcElement, Element $targetElement, Qty $qty) {
    $this->srcElement = $srcElement;
    $this->targetElement = $targetElement;
    $this->qty = $qty;
  }

  /**
   * @return Element
   */
  public function getSrcElement(): Element {
    return $this->srcElement;
  }

  /**
   * @return Element
   */
  public function getTargetElement(): Element {
    return $this->targetElement;
  }

  /**
   * @return Qty
   */
  public function getQty(): Qty {
    return $this->qty;
  }
}
