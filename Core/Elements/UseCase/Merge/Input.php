<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 13:25
 */


namespace Core\Elements\UseCase\Merge;


use Core\Elements\Element;
use Core\Elements\ElementId;
use Core\Elements\Exceptions\ElementNotFound;
use Core\Elements\Package\Exceptions\PackageNotFound;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;

class Input {

  private Element $srcElement;
  private Element $targetElement;
  private Package $srcPackage;
  private Package $targetPackage;

  /**
   * Input constructor.
   *
   * @param Context $context
   * @param string $src_element_id
   * @param string $target_element_id
   * @param string $src_package_id
   * @param string $target_package_id
   */
  public function __construct(Context $context, string $src_element_id, string $target_element_id, string $src_package_id, string $target_package_id) {

    $elementGateway = $context->getElementGateway();

    $srcElementId = new ElementId($src_element_id);
    $srcElement = $elementGateway->getById($srcElementId);
    if (null === $srcElement) {
      throw new ElementNotFound('Компонент-источник не найден.');
    }

    $targetElementId = new ElementId($target_element_id);
    $targetElement = $elementGateway->getById($targetElementId);
    if (null === $targetElement) {
      throw new ElementNotFound('Компонент назначения не найден.');
    }

    $packageGateway = $context->getPackageGateway();

    $srcPackageId = new PackageId($src_package_id);
    $srcPackage = $packageGateway->getById($srcPackageId);
    if (null === $srcPackage) {
      throw new PackageNotFound('Корпус источника не найден.');
    }

    $targetPackageId = new PackageId($target_package_id);
    $targetPackage = $packageGateway->getById($targetPackageId);
    if (null === $targetPackage) {
      throw new PackageNotFound('Корпус назначения не найден.');
    }

    $this->srcElement = $srcElement;
    $this->targetElement = $targetElement;
    $this->srcPackage = $srcPackage;
    $this->targetPackage = $targetPackage;
  }

  /**
   * @return Element
   */
  public function getSrcElement(): Element {
    return $this->srcElement;
  }

  /**
   * @return Element
   */
  public function getTargetElement(): Element {
    return $this->targetElement;
  }

  /**
   * @return Package
   */
  public function getSrcPackage(): Package {
    return $this->srcPackage;
  }

  /**
   * @return Package
   */
  public function getTargetPackage(): Package {
    return $this->targetPackage;
  }
}
