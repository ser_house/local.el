<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 13:24
 */


namespace Core\Elements\UseCase\Merge;


use Core\Elements\IElementGateway;
use Core\Elements\Qty;
use Core\Elements\Storage\IStorageGateway;
use Core\Infrastructure\ITransaction;
use Throwable;

class Merge {
  private ITransaction $transaction;
  private IElementGateway $elementGateway;
  private IStorageGateway $storageGateway;

  /**
   * Merge constructor.
   *
   * @param ITransaction $transaction
   * @param IElementGateway $elementGateway
   * @param IStorageGateway $storageGateway
   */
  public function __construct(ITransaction $transaction, IElementGateway $elementGateway, IStorageGateway $storageGateway) {
    $this->transaction = $transaction;
    $this->elementGateway = $elementGateway;
    $this->storageGateway = $storageGateway;
  }

  /**
   * @param Input $input
   *
   * @return Result
   * @throws Throwable
   */
  public function run(Input $input): Result {
    $srcElement = $input->getSrcElement();
    $srcPackage = $input->getSrcPackage();
    $targetElement = $input->getTargetElement();
    $targetPackage = $input->getTargetPackage();

    $qty = new Qty();
    $targetQty = $this->storageGateway->getQty($targetElement->getId(), $targetPackage->getId());

    if ($targetQty) {
      $qty = $qty->add($targetQty);
    }

    $srcQty = $this->storageGateway->getQty($srcElement->getId(), $srcPackage->getId());
    if ($srcQty) {
      $qty = $qty->add($srcQty);
    }

    $this->transaction->begin();
    try {
      $this->storageGateway->updateQty($targetElement->getId(), $targetPackage->getId(), $qty);

      if ($srcQty) {
        $this->storageGateway->remove($srcElement->getId(), $srcPackage->getId());
      }
      $this->elementGateway->remove($srcElement->getId());
      $this->transaction->commit();

      return new Result($srcElement, $targetElement, $qty);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
