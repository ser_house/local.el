<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 13:25
 */


namespace Core\Elements\UseCase\GenerateBySeries;


use Core\Elements\Package\Exceptions\PackageNotFound;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;
use Core\Elements\Qty;
use Core\Elements\Type\Exceptions\TypeNotFound;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;
use DomainException;
use Exception;

class Input {
  private const MULTIPLIERS = [1, 10, 100];

  private string $series;
  private string $unit;
  private int $multiplier;
  private Type $type;
  private Package $package;
  private Qty $qty;

  /**
   * Input constructor.
   *
   * @param Context $context
   * @param string $series
   * @param string $type_id
   * @param string $package_id
   * @param string $unit
   * @param int $multiplier
   * @param int $qty
   *
   * @throws Exception
   */
  public function __construct(Context $context, string $series, string $type_id, string $package_id, string $unit, int $multiplier, int $qty) {

    if (!in_array($multiplier, self::MULTIPLIERS)) {
      $multipliers = implode(',', self::MULTIPLIERS);
      throw new \InvalidArgumentException("Недопустимая размерность '$multiplier'. Возможные значения: $multipliers");
    }

    $typeId = new TypeId($type_id);
    $type = $context->getTypeGateway()->getById($typeId);
    if (null === $type) {
      throw new TypeNotFound();
    }

    if (!$type->hasSeries()) {
      $type_title = $type->title();
      throw new DomainException("Для типа '$type_title' не установлен флаг значений по рядам.");
    }

    $packageId = new PackageId($package_id);
    $package = $context->getPackageGateway()->getById($packageId);
    if (null === $package) {
      throw new PackageNotFound();
    }

    $this->series = $series;
    $this->unit = $unit;
    $this->multiplier = $multiplier;
    $this->type = $type;
    $this->package = $package;
    $this->qty = new Qty($qty);
  }

  /**
   * @return string
   */
  public function getSeriesName(): string {
    return $this->series;
  }

  /**
   * @return string
   */
  public function getUnit(): string {
    return $this->unit;
  }

  /**
   * @return int
   */
  public function getMultiplier(): int {
    return $this->multiplier;
  }

  /**
   * @return Type
   */
  public function getType(): Type {
    return $this->type;
  }

  /**
   * @return Package
   */
  public function getPackage(): Package {
    return $this->package;
  }

  /**
   * @return Qty
   */
  public function getQty(): Qty {
    return $this->qty;
  }
}
