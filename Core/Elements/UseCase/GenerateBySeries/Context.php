<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2021
 * Time: 12:14
 */


namespace Core\Elements\UseCase\GenerateBySeries;


use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Type\ITypeGateway;

class Context {
  private IElementGateway $elementGateway;
  private IPackageGateway $packageGateway;
  private ITypeGateway $typeGateway;

  /**
   * Context constructor.
   *
   * @param IElementGateway $elementGateway
   * @param IPackageGateway $packageGateway
   * @param ITypeGateway $typeGateway
   */
  public function __construct(IElementGateway $elementGateway, IPackageGateway $packageGateway, ITypeGateway $typeGateway) {
    $this->elementGateway = $elementGateway;
    $this->packageGateway = $packageGateway;
    $this->typeGateway = $typeGateway;
  }


  /**
   * @return IElementGateway
   */
  public function getElementGateway(): IElementGateway {
    return $this->elementGateway;
  }

  /**
   * @return IPackageGateway
   */
  public function getPackageGateway(): IPackageGateway {
    return $this->packageGateway;
  }

  /**
   * @return ITypeGateway
   */
  public function getTypeGateway(): ITypeGateway {
    return $this->typeGateway;
  }
}
