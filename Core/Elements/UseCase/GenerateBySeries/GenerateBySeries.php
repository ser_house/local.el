<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\UseCase\GenerateBySeries;


use Core\Elements\Element;
use Core\Elements\ElementData;
use Core\Elements\ElementId;
use Core\Elements\IElementGateway;
use Core\Elements\Series\SeriesFactory;
use Core\Elements\Series\SeriesGenerator;
use Core\Elements\Storage\IStorageGateway;
use Core\Infrastructure\ITransaction;
use Throwable;

class GenerateBySeries {
  private SeriesGenerator $seriesGenerator;
  private IElementGateway $elementGateway;
  private IStorageGateway $storageGateway;
  private ITransaction $transaction;
  private SeriesFactory $seriesFactory;

  /**
   * GenerateBySeries constructor.
   *
   * @param SeriesGenerator $seriesGenerator
   * @param IElementGateway $elementGateway
   * @param IStorageGateway $storageGateway
   * @param ITransaction $transaction
   * @param SeriesFactory $seriesFactory
   */
  public function __construct(
    SeriesGenerator $seriesGenerator,
    IElementGateway $elementGateway,
    IStorageGateway $storageGateway,
    ITransaction $transaction,
    SeriesFactory $seriesFactory) {

    $this->seriesGenerator = $seriesGenerator;
    $this->elementGateway = $elementGateway;
    $this->storageGateway = $storageGateway;
    $this->transaction = $transaction;
    $this->seriesFactory = $seriesFactory;
  }


  /**
   * @param Input $input
   *
   * @return array
   * @throws Throwable
   */
  public function run(Input $input): array {
    $qty = $input->getQty();

    $type = $input->getType();
    $typeId = $type->id();

    $package = $input->getPackage();
    $packageId = $package->getId();

    $series = $this->seriesFactory->getSeriesByName($input->getSeriesName());

    $titles = $this->seriesGenerator->generate($series, $input->getUnit(), $input->getMultiplier());

    $this->transaction->begin();
    try {
      $result = [];
      foreach ($titles as $title) {
        $elementId = new ElementId();
        $element = new Element($elementId, $title, $typeId);
        $this->elementGateway->add($element);
        $this->storageGateway->addQty($elementId, $packageId, $qty);
        $result[] = new ElementData($element, $type, $package, $qty);
      }

      $this->transaction->commit();

      return $result;
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
