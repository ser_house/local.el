<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.04.2021
 * Time: 9:21
 */


namespace Core\Elements\UseCase\AddPackageQty;


use Core\Elements\Storage\IStorageGateway;
use Core\Infrastructure\ITransaction;
use Throwable;

class AddPackageQty {
  private IStorageGateway $storageGateway;
  private ITransaction $transaction;

  /**
   * AddPackageQty constructor.
   *
   * @param IStorageGateway $storageGateway
   * @param ITransaction $transaction
   */
  public function __construct(IStorageGateway $storageGateway, ITransaction $transaction) {
    $this->storageGateway = $storageGateway;
    $this->transaction = $transaction;
  }


  public function run(Input $input): Result {

    $element = $input->getElement();
    $package = $input->getPackage();
    $qty = $input->getQty();

    $this->transaction->begin();
    try {
      $this->storageGateway->addQty($element->getId(), $package->getId(), $qty);
      $this->transaction->commit();

      return new Result($element, $package, $qty);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
