<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 17:35
 */


namespace Core\Elements\UseCase\AddPackageQty;


use Core\Elements\Element;
use Core\Elements\Package\Package;
use Core\Elements\Qty;
use JsonSerializable;

class Result implements JsonSerializable {
  private Element $element;
  private Package $package;
  private Qty $qty;

  /**
   * Result constructor.
   *
   * @param Element $element
   * @param Package $package
   * @param Qty $qty
   */
  public function __construct(Element $element, Package $package, Qty $qty) {
    $this->element = $element;
    $this->package = $package;
    $this->qty = $qty;
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return Package
   */
  public function getPackage(): Package {
    return $this->package;
  }

  /**
   * @return Qty
   */
  public function getQty(): Qty {
    return $this->qty;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'element' => $this->getElement(),
      'package' => $this->getPackage(),
      'qty' => $this->getQty(),
    ];
  }


}
