<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2021
 * Time: 12:14
 */


namespace Core\Elements\UseCase\ChangeQty;


use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;

class Context {
  private IElementGateway $elementGateway;
  private IPackageGateway $packageGateway;

  /**
   * Context constructor.
   *
   * @param IElementGateway $elementGateway
   * @param IPackageGateway $packageGateway
   */
  public function __construct(IElementGateway $elementGateway, IPackageGateway $packageGateway) {
    $this->elementGateway = $elementGateway;
    $this->packageGateway = $packageGateway;
  }

  /**
   * @return IElementGateway
   */
  public function getElementGateway(): IElementGateway {
    return $this->elementGateway;
  }

  /**
   * @return IPackageGateway
   */
  public function getPackageGateway(): IPackageGateway {
    return $this->packageGateway;
  }
}
