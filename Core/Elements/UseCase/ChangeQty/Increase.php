<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.04.2021
 * Time: 9:21
 */


namespace Core\Elements\UseCase\ChangeQty;


use Core\Elements\Storage\IStorageGateway;
use Core\Infrastructure\ITransaction;
use DomainException;
use Throwable;

class Increase {
  private IStorageGateway $storageGateway;
  private ITransaction $transaction;

  /**
   * Decrease constructor.
   *
   * @param IStorageGateway $storageGateway
   * @param ITransaction $transaction
   */
  public function __construct(IStorageGateway $storageGateway, ITransaction $transaction) {
    $this->storageGateway = $storageGateway;
    $this->transaction = $transaction;
  }

  /**
   * @param Input $input
   *
   * @return Result
   * @throws Throwable
   */
  public function run(Input $input): Result {
    $element = $input->getElement();
    $elementId = $element->getId();
    $package = $input->getPackage();
    $packageId = $package->getId();
    $qty = $input->getQty();

    $currentQty = $this->storageGateway->getQty($elementId, $packageId);
    if (null === $currentQty) {
      throw new DomainException('Текущее кол-во отсутствует.');
    }
    $newQty = $currentQty->add($qty);
    $this->transaction->begin();
    try {
      $this->storageGateway->updateQty($elementId, $packageId, $newQty);
      $this->transaction->commit();

      return new Result($element, $package, $newQty);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
