<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.04.2021
 * Time: 12:13
 */


namespace Core\Elements\UseCase\ChangeQty;


use Core\Elements\Element;
use Core\Elements\ElementId;
use Core\Elements\Exceptions\ElementNotFound;
use Core\Elements\Package\Exceptions\PackageNotFound;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;
use Core\Elements\Qty;
use Exception;

class Input {

  protected Element $element;
  protected Package $package;
  protected Qty $qty;

  /**
   * Input constructor.
   *
   * @param Context $context
   * @param string $element_id
   * @param string $package_id
   * @param int $qty
   *
   * @throws Exception
   */
  public function __construct(Context $context, string $element_id, string $package_id, int $qty) {
    $elementId = new ElementId($element_id);
    $element = $context->getElementGateway()->getById($elementId);
    if (null === $element) {
      throw new ElementNotFound();
    }

    $packageId = new PackageId($package_id);
    $package = $context->getPackageGateway()->getById($packageId);
    if (null === $package) {
      throw new PackageNotFound();
    }

    $this->element = $element;
    $this->package = $package;
    $this->qty = new Qty($qty);
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return Package
   */
  public function getPackage(): Package {
    return $this->package;
  }

  /**
   * @return Qty
   */
  public function getQty(): Qty {
    return $this->qty;
  }
}
