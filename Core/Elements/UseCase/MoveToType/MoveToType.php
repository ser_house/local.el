<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:02
 */


namespace Core\Elements\UseCase\MoveToType;


use Core\Elements\Element;
use Core\Infrastructure\ITransaction;
use Throwable;

class MoveToType {

  private ITransaction $transaction;

  /**
   * MoveToType constructor.
   *
   * @param ITransaction $transaction
   */
  public function __construct(ITransaction $transaction) {
    $this->transaction = $transaction;
  }

  /**
   * @param Context $context
   * @param Input $input
   *
   * @return Result
   * @throws Throwable
   */
  public function run(Context $context, Input $input): Result {
    $element = $input->getElement();
    $elementCurrentType = $context->getTypeGateway()->getById($element->getTypeId());

    $movedElement = new Element($element->getId(), $element->getTitle(), $input->getNewType()->id());

    $this->transaction->begin();
    try {
      $context->getElementGateway()->update($movedElement);
      $this->transaction->commit();

      return new Result($movedElement, $elementCurrentType, $input->getNewType());
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
