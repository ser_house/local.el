<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:01
 */


namespace Core\Elements\UseCase\MoveToType;


use Core\Elements\Element;
use Core\Elements\ElementId;
use Core\Elements\Exceptions\ElementNotFound;
use Core\Elements\Type\Exceptions\TypeNotFound;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;
use DomainException;

final class Input {
  private Element $element;
  private Type $newType;

  /**
   * Input constructor.
   *
   * @param Context $context
   * @param string $element_id
   * @param string $new_type_id
   */
  public function __construct(Context $context, string $element_id, string $new_type_id) {
    $elementId = new ElementId($element_id);
    $element = $context->getElementGateway()->getById($elementId);
    if (null === $element) {
      throw new ElementNotFound();
    }

    $newTypeId = new TypeId($new_type_id);
    $newType = $context->getTypeGateway()->getById($newTypeId);
    if (null === $newType) {
      throw new TypeNotFound('Тип назначения не найден.');
    }

    if ($element->getTypeId()->equals($newTypeId)) {
      throw new DomainException('Компонент не может быть перемещен, поскольку его текущий и новый типы совпадают.');
    }

    $this->element = $element;
    $this->newType = $newType;
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return Type
   */
  public function getNewType(): Type {
    return $this->newType;
  }
}
