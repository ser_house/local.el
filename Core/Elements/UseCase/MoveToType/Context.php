<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:06
 */


namespace Core\Elements\UseCase\MoveToType;


use Core\Elements\IElementGateway;
use Core\Elements\Type\ITypeGateway;

final class Context {
  private IElementGateway $elementGateway;
  private ITypeGateway $typeGateway;

  /**
   * Context constructor.
   *
   * @param IElementGateway $elementGateway
   * @param ITypeGateway $typeGateway
   */
  public function __construct(IElementGateway $elementGateway, ITypeGateway $typeGateway) {
    $this->elementGateway = $elementGateway;
    $this->typeGateway = $typeGateway;
  }

  /**
   * @return IElementGateway
   */
  public function getElementGateway(): IElementGateway {
    return $this->elementGateway;
  }

  /**
   * @return ITypeGateway
   */
  public function getTypeGateway(): ITypeGateway {
    return $this->typeGateway;
  }
}
