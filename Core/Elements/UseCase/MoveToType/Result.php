<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 18:03
 */


namespace Core\Elements\UseCase\MoveToType;


use Core\Elements\Element;
use Core\Elements\Type\Type;

class Result {

  private Element $element;
  private Type $currentType;
  private Type $newType;

  /**
   * Result constructor.
   *
   * @param Element $element
   * @param Type $currentType
   * @param Type $newType
   */
  public function __construct(Element $element, Type $currentType, Type $newType) {
    $this->element = $element;
    $this->currentType = $currentType;
    $this->newType = $newType;
  }

  /**
   * @return Element
   */
  public function getElement(): Element {
    return $this->element;
  }

  /**
   * @return Type
   */
  public function getCurrentType(): Type {
    return $this->currentType;
  }

  /**
   * @return Type
   */
  public function getNewType(): Type {
    return $this->newType;
  }
}
