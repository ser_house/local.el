<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 18.05.2021
 * Time: 19:22
 */


namespace Core\Elements\Series;


interface ISeries {
  /**
   *
   * @return string
   */
  public function name(): string;
  /**
   *
   * @return array
   */
  public function values(): array;
}
