<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.05.2021
 * Time: 18:56
 */


namespace Core\Elements\Series;

use Core\Elements\Units\UnitsType;

class SeriesGenerator {


  /**
   * @param ISeries $series
   * @param string $unit
   * @param int $multiplier
   *
   * @return string[]
   */
  public function generate(ISeries $series, string $unit, int $multiplier = 1): array {
    $values = $series->values();

    if (UnitsType::isBaseUnit($unit) && 1 === $multiplier) {
      return $values;
    }

    $names = [];
    foreach ($values as $value) {
      $names[] = ($value * $multiplier) . $unit;
    }

    return $names;
  }
}
