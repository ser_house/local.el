<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 14.05.2021
 * Time: 18:54
 */


namespace Core\Elements\Series;


class SeriesFactory {

  public function getSeriesByName(string $name): ISeries {
    switch($name) {
      case 'E24':
        return new E24();

      default:
        throw new \Exception("Ряд '$name' не реализован.");
    }
  }
}
