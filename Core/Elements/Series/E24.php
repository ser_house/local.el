<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.03.2021
 * Time: 5:49
 */


namespace Core\Elements\Series;

/**
 * Class Series
 *
 * @package Core\Elements\Series
 */
class E24 implements ISeries {

  /**
   * @inheritDoc
   */
  public function name(): string {
    return 'E24';
  }

  /**
   * @inheritDoc
   */
  public function values(): array {
    return [
      1.0,
      1.1,
      1.2,
      1.3,
      1.5,
      1.6,
      1.8,
      2.0,
      2.2,
      2.4,
      2.7,
      3.0,
      3.3,
      3.6,
      3.9,
      4.3,
      4.7,
      5.1,
      5.6,
      6.2,
      6.8,
      7.5,
      8.2,
      9.1,
    ];
  }
}
