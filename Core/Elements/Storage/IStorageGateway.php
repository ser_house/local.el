<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 9:46
 */


namespace Core\Elements\Storage;


use Core\Elements\ElementId;
use Core\Elements\Package\PackageId;
use Core\Elements\Qty;

interface IStorageGateway {

  /**
   * @param ElementId $elementId
   * @param PackageId $packageId
   *
   * @return Qty|null
   */
  public function getQty(ElementId $elementId, PackageId $packageId): ?Qty;

  /**
   * @param ElementId $elementId
   * @param PackageId $packageId
   * @param Qty $qty
   */
  public function addQty(ElementId $elementId, PackageId $packageId, Qty $qty): void;

  /**
   * @param ElementId $elementId
   * @param PackageId $packageId
   * @param Qty $qty
   */
  public function updateQty(ElementId $elementId, PackageId $packageId, Qty $qty): void;

  /**
   * @param ElementId $elementId
   * @param PackageId $currentPackageId
   * @param PackageId $newPackageId
   */
  public function replacePackage(ElementId $elementId, PackageId $currentPackageId, PackageId $newPackageId): void;

  /**
   * @param ElementId $elementId
   * @param PackageId $packageId
   */
  public function remove(ElementId $elementId, PackageId $packageId): void;
}
