<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 9:56
 */


namespace Core\Elements\Package;


use JsonSerializable;

final class Package implements JsonSerializable {

  private PackageId $id;
  private string $title;

  /**
   * Package constructor.
   *
   * @param PackageId $id
   * @param string $title
   */
  public function __construct(PackageId $id, string $title) {
    $this->id = $id;
    $this->title = $title;
  }

  /**
   * @return PackageId
   */
  public function getId(): PackageId {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }


  /**
   * @param Package $type
   *
   * @return bool
   */
  public function equals(Package $type): bool {
    return $this->getId()->equals($type->getId()) && $this->getTitle() === $type->getTitle();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->title;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'id' => (string)$this->getId(),
      'title' => $this->getTitle(),
    ];
  }
}
