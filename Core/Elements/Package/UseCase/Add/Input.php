<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 10:05
 */


namespace Core\Elements\Package\UseCase\Add;


use DomainException;

class Input {
  private string $title;

  /**
   * Input constructor.
   *
   * @param string $title
   */
  public function __construct(string $title) {
    if (empty($title)) {
      throw new DomainException('Название корпуса не может быть пустым.');
    }
    $this->title = $title;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }
}
