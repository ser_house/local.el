<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\Package\UseCase\Add;


use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;
use Core\Infrastructure\ITransaction;
use DomainException;
use Throwable;

class Add {

  private IPackageGateway $packageGateway;
  private ITransaction $transaction;

  /**
   * Add constructor.
   *
   * @param IPackageGateway $packageGateway
   * @param ITransaction $transaction
   */
  public function __construct(IPackageGateway $packageGateway, ITransaction $transaction) {
    $this->packageGateway = $packageGateway;
    $this->transaction = $transaction;
  }


  public function run(Input $input): Package {
    $title = $input->getTitle();
    $package = $this->packageGateway->getByTitle($title);
    if (null !== $package) {
      throw new DomainException("Корпус с таким названием ('$title') уже существует.");
    }

    $package = new Package(new PackageId(), $title);
    $this->transaction->begin();
    try {
      $this->packageGateway->add($package);
      $this->transaction->commit();

      return $package;
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
