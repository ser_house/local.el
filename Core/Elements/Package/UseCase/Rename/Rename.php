<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\Package\UseCase\Rename;


use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\Package;
use Core\Infrastructure\ITransaction;
use Throwable;

class Rename {

  private IPackageGateway $packageGateway;
  private ITransaction $transaction;

  /**
   * Rename constructor.
   *
   * @param IPackageGateway $packageGateway
   * @param ITransaction $transaction
   */
  public function __construct(IPackageGateway $packageGateway, ITransaction $transaction) {
    $this->packageGateway = $packageGateway;
    $this->transaction = $transaction;
  }

  /**
   * @param Input $input
   *
   * @return Result
   * @throws Throwable
   */
  public function run(Input $input): Result {
    $package = $input->getPackage();
    $new_title = $input->getNewTitle();

    $renamedPackage = new Package($package->getId(), $new_title);
    $existsPackage = $this->packageGateway->getByTitle($new_title);

    $this->transaction->begin();
    try {
      // Если корпус с новым названием уже есть, то делаем замену.
      if ($existsPackage) {
        $this->packageGateway->replace($existsPackage->getId(), $package->getId());
        $this->packageGateway->remove($existsPackage->getId());
      }
      else {
        $this->packageGateway->update($renamedPackage);
      }

      $this->transaction->commit();

      return new Result($package, $renamedPackage);
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
