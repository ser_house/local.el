<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 10:05
 */


namespace Core\Elements\Package\UseCase\Rename;


use Core\Elements\Package\Exceptions\PackageNotFound;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;
use DomainException;

class Input {
  private Package $package;
  private string $new_title;

  /**
   * Input constructor.
   *
   * @param IPackageGateway $packageGateway
   * @param string $package_id
   * @param string $new_title
   */
  public function __construct(IPackageGateway $packageGateway, string $package_id, string $new_title) {
    if (empty($new_title)) {
      throw new DomainException('Название корпуса не может быть пустым.');
    }

    $package = $packageGateway->getById(new PackageId($package_id));
    if (null === $package) {
      throw new PackageNotFound();
    }

    $this->package = $package;
    $this->new_title = $new_title;
  }

  /**
   * @return Package
   */
  public function getPackage(): Package {
    return $this->package;
  }

  /**
   * @return string
   */
  public function getNewTitle(): string {
    return $this->new_title;
  }
}
