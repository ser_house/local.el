<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 11:50
 */


namespace Core\Elements\Package\UseCase\Rename;


use Core\Elements\Package\Package;

class Result {

  private Package $originalPackage;
  private Package $renamedPackage;

  /**
   * Result constructor.
   *
   * @param Package $originalPackage
   * @param Package $renamedPackage
   */
  public function __construct(Package $originalPackage, Package $renamedPackage) {
    $this->originalPackage = $originalPackage;
    $this->renamedPackage = $renamedPackage;
  }

  /**
   * @return Package
   */
  public function getOriginalPackage(): Package {
    return $this->originalPackage;
  }

  /**
   * @return Package
   */
  public function getRenamedPackage(): Package {
    return $this->renamedPackage;
  }
}
