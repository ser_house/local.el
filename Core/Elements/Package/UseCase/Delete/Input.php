<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 10:05
 */


namespace Core\Elements\Package\UseCase\Delete;


use Core\Elements\Package\Exceptions\PackageHasElements;
use Core\Elements\Package\Exceptions\PackageNotFound;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\PackageId;

class Input {
  private PackageId $packageId;

  /**
   * Input constructor.
   *
   * @param IPackageGateway $packageGateway
   * @param PackageId $packageId
   */
  public function __construct(IPackageGateway $packageGateway, PackageId $packageId) {
    $package = $packageGateway->getById($packageId);
    if (null === $package) {
      throw new PackageNotFound();
    }

    if ($packageGateway->getElementsCount($packageId) > 0) {
      throw new PackageHasElements($package);
    }

    $this->packageId = $packageId;
  }

  /**
   * @return PackageId
   */
  public function getPackageId(): PackageId {
    return $this->packageId;
  }
}
