<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:41
 */


namespace Core\Elements\Package\UseCase\Delete;


use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\Package;
use Core\Infrastructure\ITransaction;
use Throwable;

class Delete {

  private IPackageGateway $packageGateway;
  private ITransaction $transaction;

  /**
   * Delete constructor.
   *
   * @param IPackageGateway $packageGateway
   * @param ITransaction $transaction
   */
  public function __construct(IPackageGateway $packageGateway, ITransaction $transaction) {
    $this->packageGateway = $packageGateway;
    $this->transaction = $transaction;
  }

  /**
   * @param Input $input
   *
   * @return Package
   * @throws Throwable
   */
  public function run(Input $input): Package {
    $packageId = $input->getPackageId();
    $package = $this->packageGateway->getById($packageId);

    $this->transaction->begin();
    try {
      $this->packageGateway->remove($packageId);
      $this->transaction->commit();

      return $package;
    }
    catch (Throwable $e) {
      $this->transaction->rollback();
      throw $e;
    }
  }
}
