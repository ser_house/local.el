<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:40
 */


namespace Core\Elements\Package;


interface IPackageGateway {

  /**
   * @param Package $package
   */
  public function add(Package $package): void;

  /**
   * @param PackageId $packageId
   *
   * @return Package|null
   */
  public function getById(PackageId $packageId): ?Package;

  /**
   * @param string $title
   *
   * @return Package|null
   */
  public function getByTitle(string $title): ?Package;

  /**
   * @param PackageId $packageId
   *
   * @return int
   */
  public function getElementsCount(PackageId $packageId): int;

  /**
   *
   * @return PackageWithElementsQty[]
   */
  public function getPackagesWithElementsCount(): array;

  /**
   * @param Package $package
   */
  public function update(Package $package): void;

  /**
   * @param PackageId $targetPackageId
   * @param PackageId $newPackageId
   */
  public function replace(PackageId $targetPackageId, PackageId $newPackageId): void;

  /**
   * @param PackageId $packageId
   */
  public function remove(PackageId $packageId): void;
}
