<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 06.04.2021
 * Time: 17:20
 */


namespace Core\Elements\Package;


use Core\Elements\Qty;
use JsonSerializable;

class PackageWithElementsQty implements JsonSerializable {

  private Package $package;
  private Qty $qty;

  /**
   * PackageWithElementsQty constructor.
   *
   * @param Package $package
   * @param Qty $qty
   */
  public function __construct(Package $package, Qty $qty) {
    $this->package = $package;
    $this->qty = $qty;
  }

  /**
   * @return Package
   */
  public function getPackage(): Package {
    return $this->package;
  }

  /**
   * @return Qty
   */
  public function getQty(): Qty {
    return $this->qty;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'id' => (string)$this->package->getId(),
      'title' => $this->package->getTitle(),
      'qty' => $this->qty->asInt(),
    ];
  }
}
