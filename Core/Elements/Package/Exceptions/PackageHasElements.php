<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 17:21
 */


namespace Core\Elements\Package\Exceptions;


use Core\Elements\Package\Package;
use DomainException;
use Throwable;

final class PackageHasElements extends DomainException {
  /**
   * @inheritDoc
   */
  public function __construct(Package $package, $code = 0, Throwable $previous = null) {
    $package_title = $package->getTitle();
    $message = "Невозможно удалить корпус '$package_title': есть компоненты.";

    parent::__construct($message, $code, $previous);
  }

}
