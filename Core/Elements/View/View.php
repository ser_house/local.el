<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.03.2021
 * Time: 6:45
 */


namespace Core\Elements\View;


class View {

  public string $id;
  public string $title;
  public string $type_id;
  public string $package_id;
  public int $qty;
}
