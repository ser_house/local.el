<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.05.2021
 * Time: 8:54
 */


namespace Core\Elements\Units;


final class InvalidUnitsTypeException extends \DomainException {

}
