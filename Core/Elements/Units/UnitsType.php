<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 19.05.2021
 * Time: 8:44
 */


namespace Core\Elements\Units;


class UnitsType {
  private const RESISTANCE = 'resistance';
  private const CAPACITY = 'capacity';

  private string $value;

  /**
   * UnitsType constructor.
   *
   * @param string $value
   */
  public function __construct(string $value) {
    if (!self::isTypeDefined($value)) {
      throw new InvalidUnitsTypeException("Тип '$value' не определен.");
    }

    $this->value = $value;
  }

  public function value(): string {
    return $this->value;
  }

  public function equals(UnitsType $unitsType): bool {
    return $this->value === $unitsType->value();
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value();
  }

  /**
   *
   * @return array|string[]
   */
  public static function data(): array {
    return [
      self::RESISTANCE => [
        'title' => 'Единицы сопротивления',
        'units' => ['R', 'K', 'M'],
      ],
      self::CAPACITY => [
        'title' => 'Единицы ёмкости',
        'units' => ['pF', 'nF', 'uF'],
      ],
    ];
  }

  public static function baseTypeUnit(string $type): string {
    if (!self::isTypeDefined($type)) {
      throw new InvalidUnitsTypeException("Тип '$type' не определен.");
    }

    $data = self::data();
    return $data[$type]['units'][0];
  }

  public static function isBaseUnit(string $unit): bool {
    $type = self::getTypeByUnit($unit);
    if (null === $type) {
      throw new \InvalidArgumentException("Тип для ед. изм. '$unit' не найден.");
    }
    return self::baseTypeUnit($type) === $unit;
  }

  public static function getTypeByUnit(string $unit): ?string {
    $data = self::data();
    if (in_array($unit, $data[self::RESISTANCE]['units'])) {
      return self::RESISTANCE;
    }

    if (in_array($unit, $data[self::CAPACITY]['units'])) {
      return self::CAPACITY;
    }

    return null;
  }

  private static function isTypeDefined(string $type): bool {
    return $type === self::RESISTANCE || $type === self::CAPACITY;
  }
}
