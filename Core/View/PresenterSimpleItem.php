<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.12.2020
 * Time: 10:00
 */


namespace Core\View;


class PresenterSimpleItem {

  public function presentOne($model): array {
    return [
      'id' => trim($model->id),
      'title' => $model->title,
    ];
  }

  public function presentMulti(iterable $models): array {
    $items = [];
    foreach($models as $model) {
      $items[] = $this->presentOne($model);
    }
    return $items;
  }
}
