<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.10.2020
 * Time: 10:35
 */


namespace Core\View;


class IdentifiedItemView {

  public string $id;
  public string $title;

  /**
   * IdentifiedItemView constructor.
   *
   * @param string $id
   * @param string $title
   */
  public function __construct(string $id, string $title) {
    $this->id = $id;
    $this->title = $title;
  }
}
