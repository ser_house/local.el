<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.09.2020
 * Time: 8:25
 */


namespace Core;


use Ulid\Ulid;

/**
 * Class Id
 *
 * @package Core\Entity\Field
 */
class Id {
  protected string $value;

  /**
   * Id constructor.
   *
   * @param string $value
   */
  public function __construct(string $value = '') {
    $this->value = !empty($value) ? trim($value) : (string)Ulid::generate(true);
  }

  /**
   * @param Id $id
   *
   * @return bool
   */
  public function equals(Id $id): bool {
    return (string)$id === $this->value;
  }

  /**
   * @inheritDoc
   */
  public function __toString() {
    return $this->value;
  }

  /**
   *
   * @return bool
   */
  public function isEmpty(): bool {
    return empty($this->value);
  }
}
