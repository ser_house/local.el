<?php
/****************************************************************************
 * Software: Midi Class
 * Version:  1.7.3
 * Date:     2008/05/30
 * Author:   Valentin Schmidt
 * Contact:  fluxus@freenet.de
 * License:  Freeware
 *
 * You may use and modify this software as you wish.
 *
 * Last Changes:
 * + bug bix in newTrack(): -1 was missing to return
 * + bug fix parsing sysex messages
 ****************************************************************************/

namespace Core\Tools\Midi;

use Exception;

class Midi {

  protected array $tracks;          //array of tracks, where each track is array of message strings
  protected int $timebase;        //timebase = ticks per frame (quarter note)
  protected int $tempo;           //tempo as integer (0 for unknown)
  protected int $tempoMsgNum;     //position of tempo event in track 0
  protected int $type;

  /****************************************************************************
   *                                                                           *
   *                              Public methods                               *
   *                                                                           *
   ***************************************************************************
   *
   * @param int $timebase
   */

//---------------------------------------------------------------
// creates (or resets to) new empty MIDI song
//---------------------------------------------------------------
  function open(int $timebase = 480) {
    $this->tempo = 0;//125000 = 120 bpm
    $this->timebase = $timebase;
    $this->tracks = [];
  }

//---------------------------------------------------------------
// sets tempo by replacing set tempo msg in track 0 (or adding new track 0)
//---------------------------------------------------------------
  function setTempo($tempo) {
    $tempo = round($tempo);
    if (isset($this->tempoMsgNum)) {
      $this->tracks[0][$this->tempoMsgNum] = "0 Tempo $tempo";
    }
    else {
      $tempoTrack = ['0 TimeSig 4/4 24 8', "0 Tempo $tempo", '0 Meta TrkEnd'];
      array_unshift($this->tracks, $tempoTrack);
      $this->tempoMsgNum = 1;
    }
    $this->tempo = $tempo;
  }

//---------------------------------------------------------------
// returns tempo (0 if not set)
//---------------------------------------------------------------
  function getTempo() {
    return $this->tempo;
  }

//---------------------------------------------------------------
// returns bpm corresponding to tempo
//---------------------------------------------------------------
  function getBpm() {
    return ($this->tempo != 0) ? (int)(60000000 / $this->tempo) : 0;
  }

//---------------------------------------------------------------
// sets timebase
//---------------------------------------------------------------
  function setTimebase($tb) {
    $this->timebase = $tb;
  }

//---------------------------------------------------------------
// returns timebase
//---------------------------------------------------------------
  function getTimebase() {
    return $this->timebase;
  }

//---------------------------------------------------------------
// returns track $tn as array of msg strings
//---------------------------------------------------------------
  function getTrack($tn) {
    return $this->tracks[$tn];
  }

//---------------------------------------------------------------
// deletes all tracks except track $tn (and $track 0 which contains tempo info)
//---------------------------------------------------------------
  function soloTrack($tn) {
    if ($tn == 0) {
      $this->tracks = [$this->tracks[0]];
    }
    else {
      $this->tracks = [$this->tracks[0], $this->tracks[$tn]];
    }
  }

//---------------------------------------------------------------
// transposes song by $dn half tone steps
//---------------------------------------------------------------
  function transpose($dn) {
    $tc = count($this->tracks);
    for ($i = 0; $i < $tc; $i++) {
      $this->transposeTrack($i, $dn);
    }
  }

//---------------------------------------------------------------
// transposes track $tn by $dn half tone steps
//---------------------------------------------------------------
  function transposeTrack($tn, $dn) {
    $track = $this->tracks[$tn];
    $mc = count($track);
    $n = 0;
    for ($i = 0; $i < $mc; $i++) {
      $msg = explode(' ', $track[$i]);
      if ($msg[1] == 'On' || $msg[1] == 'Off') {
        eval('$' . $msg[3] . ';'); // $n
        $n = max(0, min(127, $n + $dn));
        $msg[3] = "n=$n";
        $track[$i] = implode(' ', $msg);
      }
    }
    $this->tracks[$tn] = $track;
  }

  //---------------------------------------------------------------
// imports Standard MIDI File (typ 0 or 1) (and RMID)
// (if optional parameter $tn set, only track $tn is imported)
//---------------------------------------------------------------
  function importMid($smf_path) {
    $SMF = fopen($smf_path, 'rb'); // Standard MIDI File, typ 0 or 1
    $song = fread($SMF, filesize($smf_path));
    fclose($SMF);
    if (strpos($song, 'MThd') > 0) {
      $song = substr($song, strpos($song, 'MThd'));
    }//get rid of RMID header
    $header = substr($song, 0, 14);
    if (substr($header, 0, 8) != "MThd\0\0\0\6") {
      throw new Exception('wrong MIDI-header');
    }
    $type = ord($header[9]);
    if ($type > 1) {
      throw new Exception('only SMF Typ 0 and 1 supported');
    }
    //$trackCnt = ord($header[10])*256 + ord($header[11]); //ignore
    $timebase = ord($header[12]) * 256 + ord($header[13]);

    $this->type = $type;
    $this->timebase = $timebase;
    $this->tempo = 0; // maybe (hopefully!) overwritten by _parseTrack
    $trackStrings = explode('MTrk', $song);
    array_shift($trackStrings);
    $tracks = [];
    $tsc = count($trackStrings);
    if (func_num_args() > 1) {
      $tn = func_get_arg(1);
      if ($tn >= $tsc) {
        throw new Exception('SMF has less tracks than $tn');
      }
      $tracks[] = $this->_parseTrack($trackStrings[$tn], $tn);
    }
    else {
      for ($i = 0; $i < $tsc; $i++) {
        $tracks[] = $this->_parseTrack($trackStrings[$i], $i);
      }
    }
    $this->tracks = $tracks;
  }

//---------------------------------------------------------------
// embeds Standard MIDI File
//---------------------------------------------------------------
  function playMidFile($file, $visible = 1, $auto = 1, $loop = 1, $plug = '') {

    switch ($plug) {
      case 'qt':
        ?>
				<!-- QT -->
				<OBJECT CLASSID="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" WIDTH="<?php echo ($visible == 0) ? 2 : 160 ?>" HEIGHT="<?php echo ($visible == 0) ? 2 : 16 ?>" CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab">
					<PARAM NAME="SRC" VALUE="<?php echo $file ?>">
					<PARAM NAME="AUTOPLAY" VALUE="<?php echo $auto ? 'true' : 'false' ?>">
					<PARAM NAME="LOOP" VALUE="<?php echo $loop ? 'true' : 'false' ?>">
					<PARAM NAME="CONTROLLER" VALUE="<?php echo ($visible == 0) ? 'false' : 'true' ?>">
          <?php echo ($visible == 0) ? '<PARAM NAME="HIDDEN" VALUE="true">' : '' ?>
					<EMBED TYPE="video/quicktime" SRC="<?php echo $file ?>" WIDTH="<?php echo ($visible == 0) ? 2 : 160 ?>" HEIGHT="<?php echo ($visible == 0) ? 2 : 16 ?>" AUTOPLAY="<?php echo $auto ? 'true' : 'false' ?>"
						LOOP="<?php echo $loop ? 'true' : 'false' ?>" CONTROLLER="<?php echo ($visible == 0) ? 'false' : 'true' ?>" <?php echo ($visible == 0) ? 'HIDDEN="true" ' : '' ?>PLUGINSPAGE="http://www.apple.com/quicktime/download/">
				</OBJECT>
        <?php
        break;
      case 'wm':
        ?>
				<!-- WMP -->
				<OBJECT CLASSID="CLSID:22D6f312-B0F6-11D0-94AB-0080C74C7E95" CODEBASE="http://www.microsoft.com/ntserver/netshow/download/en/nsmp2inf.cab#Version=5,1,51,415" type="application/x-oleobject"
					width=<?php echo ($visible == 0) ? 0 : 300 ?> height=<?php echo ($visible == 0) ? 0 : 44 ?>>
					<PARAM NAME="AutoStart" VALUE="<?php echo $auto ? 'true' : 'false' ?>">
					<PARAM NAME="FileName" VALUE="<?php echo $file ?>">
					<PARAM NAME="ControlType" VALUE="1">
					<PARAM NAME="Loop" VALUE="<?php echo $loop ? 'true' : 'false' ?>">
					<PARAM NAME="ShowControls" VALUE="<?php echo ($visible == 0) ? 'false' : 'true' ?>">
					<EMBED TYPE="video/x-ms-asf-plugin" PLUGINSPAGE="http://www.microsoft.com/windows/mediaplayer/download/default.asp" SRC="<?php echo $file ?>" AutoStart="<?php echo $auto ? 1 : 0 ?>"
						ShowControls="<?php echo ($visible == 0) ? 0 : 1 ?>" Loop="<?php echo $loop ?>" width=<?php echo ($visible == 0) ? 0 : 300 ?> height=<?php echo ($visible == 0) ? 0 : 44 ?>>
				</OBJECT>
        <?php
        break;
      case 'bk':
        ?>
				<OBJECT CLASSID="CLSID:B384F118-18EE-11D1-95C8-00A024330339" CODEBASE="http://dasdeck.de/beatnik/beatnik.cab" WIDTH=<?php echo ($visible == 0) ? 2 : 144 ?> HEIGHT=<?php echo ($visible == 0) ? 0 : 15 ?>>
					<PARAM NAME="SRC" VALUE="<?php echo $file ?>">
					<PARAM NAME="TYPE" VALUE="audio/midi">
					<PARAM NAME="WIDTH" VALUE="<?php echo ($visible == 0) ? 2 : 144 ?>">
					<PARAM NAME="HEIGHT" VALUE="<?php echo ($visible == 0) ? 2 : 15 ?>">
					<PARAM NAME="DISPLAY" VALUE="song">
					<PARAM NAME="AUTOSTART" VALUE="<?php echo $auto ? 'true' : 'false' ?>">
					<PARAM NAME="LOOP" VALUE="<?php echo $loop ? 'true' : 'false' ?>">
          <?php echo ($visible == 0) ? '<PARAM NAME="HIDDEN" VALUE="true">' : '' ?>
					<EMBED TYPE="audio/rmf" SRC="<?php echo $file ?>" WIDTH="<?php echo ($visible == 0) ? 2 : 144 ?>" HEIGHT="<?php echo ($visible == 0) ? 2 : 15 ?>" DISPLAY="SONG" AUTOSTART="<?php echo $auto ? 'true' : 'false' ?>"
						LOOP="<?php echo $loop ? 'true' : 'false' ?>" PLUGINSPAGE="http://www.beatnik.com/to/?player"<?php echo ($visible == 0) ? ' HIDDEN="true"' : '' ?>
					>
				</OBJECT>
        <?php
        break;
      case 'jv':
        ?>
				<OBJECT classid="clsid:CAFEEFAC-0014-0002-0000-ABCDEFFEDCBA" WIDTH="<?php echo ($visible == 0) ? 0 : 32 ?>" HEIGHT="<?php echo ($visible == 0) ? 0 : 32 ?>"
					CODEBASE="http://java.sun.com/products/plugin/autodl/jinstall-1_4_2-windows-i586.cab#Version=1,4,2,0">
					<PARAM NAME="code" VALUE="MidiApplet.class">
					<PARAM NAME="type" VALUE="application/x-java-applet;jpi-version=1.4.2">
					<PARAM NAME="src" VALUE="<?php echo $file ?>">
					<PARAM NAME="autostart" VALUE="<?php echo $auto ? 1 : 0 ?>">
					<PARAM NAME="loop" VALUE="<?php echo $loop ? 1 : 0 ?>">
					<COMMENT>
						<EMBED TYPE="application/x-java-applet;jpi-version=1.4.2" CODE="MidiApplet.class" WIDTH="<?php echo ($visible == 0) ? 0 : 32 ?>" HEIGHT="<?php echo ($visible == 0) ? 0 : 32 ?>"
							pluginspage="http://java.sun.com/products/plugin/index.html#download" autostart="<?php echo $auto ? 1 : 0 ?>" loop="<?php echo $loop ? 1 : 0 ?>"></EMBED>
					</COMMENT>
				</OBJECT>
        <?php
        break;
      default:
        ?>
				<EMBED SRC="<?php echo $file ?>" TYPE="audio/midi" AUTOSTART="<?php echo $auto ? 'TRUE' : 'FALSE' ?>" LOOP="<?php echo $loop ? 'TRUE' : 'FALSE' ?>"<?php echo ($visible == 0) ? ' HIDDEN="true"' : '' ?>>
      <?php
    }
  }

//---------------------------------------------------------------
// returns list of note names
//---------------------------------------------------------------
  function getNoteList() {
    //note 69 (A6) = A440
    //note 60 (C6) = Middle C
    return [
      //Do          Re           Mi    Fa           So           La           Ti
      'C0', 'Cs0', 'D0', 'Ds0', 'E0', 'F0', 'Fs0', 'G0', 'Gs0', 'A0', 'As0', 'B0',
      'C1', 'Cs1', 'D1', 'Ds1', 'E1', 'F1', 'Fs1', 'G1', 'Gs1', 'A1', 'As1', 'B1',
      'C2', 'Cs2', 'D2', 'Ds2', 'E2', 'F2', 'Fs2', 'G2', 'Gs2', 'A2', 'As2', 'B2',
      'C3', 'Cs3', 'D3', 'Ds3', 'E3', 'F3', 'Fs3', 'G3', 'Gs3', 'A3', 'As3', 'B3',
      'C4', 'Cs4', 'D4', 'Ds4', 'E4', 'F4', 'Fs4', 'G4', 'Gs4', 'A4', 'As4', 'B4',
      'C5', 'Cs5', 'D5', 'Ds5', 'E5', 'F5', 'Fs5', 'G5', 'Gs5', 'A5', 'As5', 'B5',
      'C6', 'Cs6', 'D6', 'Ds6', 'E6', 'F6', 'Fs6', 'G6', 'Gs6', 'A6', 'As6', 'B6',
      'C7', 'Cs7', 'D7', 'Ds7', 'E7', 'F7', 'Fs7', 'G7', 'Gs7', 'A7', 'As7', 'B7',
      'C8', 'Cs8', 'D8', 'Ds8', 'E8', 'F8', 'Fs8', 'G8', 'Gs8', 'A8', 'As8', 'B8',
      'C9', 'Cs9', 'D9', 'Ds9', 'E9', 'F9', 'Fs9', 'G9', 'Gs9', 'A9', 'As9', 'B9',
      'C10', 'Cs10', 'D10', 'Ds10', 'E10', 'F10', 'Fs10', 'G10'];
  }

//---------------------------------------------------------------
// converts binary track string to track (list of msg strings)
//---------------------------------------------------------------
  function _parseTrack($binStr, $tn) {
    //$trackLen2 =  ((( (( (ord($binStr[0]) << 8) | ord($binStr[1]))<<8) | ord($binStr[2]) ) << 8 ) |  ord($binStr[3]) );
    //$trackLen2 += 4;
    $trackLen = strlen($binStr);
// MM: ToDo: Warn if trackLen and trackLen2 are different!!!
// if ($trackLen != $trackLen2) { echo "Warning: TrackLength is corrupt ($trackLen != $trackLen2)! \n"; }
    $p = 4;
    $time = 0;
    $track = [];
    while ($p < $trackLen) {
      // timedelta
      $dt = $this->_readVarLen($binStr, $p);
      $time += $dt;

      $byte = ord($binStr[$p]);
      $high = $byte >> 4;
      $low = $byte - $high * 16;
      switch ($high) {
        case 0x0C: //PrCh = ProgramChange
          $chan = $low + 1;
          $prog = ord($binStr[$p + 1]);
          $last = 'PrCh';
          $track[] = "$time PrCh ch=$chan p=$prog";
          $p += 2;
          break;
        case 0x09: //On
          $chan = $low + 1;
          $note = ord($binStr[$p + 1]);
          $vel = ord($binStr[$p + 2]);
          $last = 'On';
          $track[] = "$time On ch=$chan n=$note v=$vel";
          $p += 3;
          break;
        case 0x08: //Off
          $chan = $low + 1;
          $note = ord($binStr[$p + 1]);
          $vel = ord($binStr[$p + 2]);
          $last = 'Off';
          $track[] = "$time Off ch=$chan n=$note v=$vel";
          $p += 3;
          break;
        case 0x0A: //PoPr = PolyPressure
          $chan = $low + 1;
          $note = ord($binStr[$p + 1]);
          $val = ord($binStr[$p + 2]);
          $last = 'PoPr';
          $track[] = "$time PoPr ch=$chan n=$note v=$val";
          $p += 3;
          break;
        case 0x0B: //Par = ControllerChange
          $chan = $low + 1;
          $c = ord($binStr[$p + 1]);
          $val = ord($binStr[$p + 2]);
          $last = 'Par';
          $track[] = "$time Par ch=$chan c=$c v=$val";
          $p += 3;
          break;
        case 0x0D: //ChPr = ChannelPressure
          $chan = $low + 1;
          $val = ord($binStr[$p + 1]);
          $last = 'ChPr';
          $track[] = "$time ChPr ch=$chan v=$val";
          $p += 2;
          break;
        case 0x0E: //Pb = PitchBend
          $chan = $low + 1;
          $val = (ord($binStr[$p + 1]) & 0x7F) | (((ord($binStr[$p + 2])) & 0x7F) << 7);
          $last = 'Pb';
          $track[] = "$time Pb ch=$chan v=$val";
          $p += 3;
          break;
        default:
          switch ($byte) {
            case 0xFF: // Meta
              $meta = ord($binStr[$p + 1]);
              switch ($meta) {
                case 0x00: // sequence_number
                  $tmp = ord($binStr[$p + 2]);
                  if ($tmp == 0x00) {
                    $num = $tn;
                    $p += 3;
                  }
                  else {
                    $num = 1;
                    $p += 5;
                  }
                  $track[] = "$time Seqnr $num";
                  break;

                case 0x01: // Meta Text
                case 0x02: // Meta Copyright
                case 0x03: // Meta TrackName ???sequence_name???
                case 0x04: // Meta InstrumentName
                case 0x05: // Meta Lyrics
                case 0x06: // Meta Marker
                case 0x07: // Meta Cue
                  $texttypes = ['Text', 'Copyright', 'TrkName', 'InstrName', 'Lyric', 'Marker', 'Cue'];
                  $type = $texttypes[$meta - 1];
                  $p += 2;
                  $len = $this->_readVarLen($binStr, $p);
                  if (($len + $p) > $trackLen) {
                    throw new Exception("Meta $type has corrupt variable length field ($len) [track: $tn dt: $dt]");
                  }
                  $txt = substr($binStr, $p, $len);
                  $track[] = "$time Meta $type \"$txt\"";
                  $p += $len;
                  break;
                case 0x20: // ChannelPrefix
                  $chan = ord($binStr[$p + 3]);
                  if ($chan < 10) {
                    $chan = '0' . $chan;
                  }//???
                  $track[] = "$time Meta 0x20 $chan";
                  $p += 4;
                  break;
                case 0x21: // ChannelPrefixOrPort
                  $chan = ord($binStr[$p + 3]);
                  if ($chan < 10) {
                    $chan = '0' . $chan;
                  }//???
                  $track[] = "$time Meta 0x21 $chan";
                  $p += 4;
                  break;
                case 0x2F: // Meta TrkEnd
                  $track[] = "$time Meta TrkEnd";

                  return $track;//ignore rest

                case 0x51: // Tempo
                  $tempo = ord($binStr[$p + 3]) * 256 * 256 + ord($binStr[$p + 4]) * 256 + ord($binStr[$p + 5]);
                  $track[] = "$time Tempo $tempo";
                  if ($tn == 0 && $time == 0) {
                    $this->tempo = $tempo;// ???
                    $this->tempoMsgNum = count($track) - 1;
                  }
                  $p += 6;
                  break;
                case 0x54: // SMPTE offset
                  $h = ord($binStr[$p + 3]);
                  $m = ord($binStr[$p + 4]);
                  $s = ord($binStr[$p + 5]);
                  $f = ord($binStr[$p + 6]);
                  $fh = ord($binStr[$p + 7]);
                  $track[] = "$time SMPTE $h $m $s $f $fh";
                  $p += 8;
                  break;
                case 0x58: // TimeSig
                  $z = ord($binStr[$p + 3]);
                  $t = pow(2, ord($binStr[$p + 4]));
                  $mc = ord($binStr[$p + 5]);
                  $c = ord($binStr[$p + 6]);
                  $track[] = "$time TimeSig $z/$t $mc $c";
                  $p += 7;
                  break;
                case 0x59: // KeySig
                  $vz = ord($binStr[$p + 3]);
                  $g = ord($binStr[$p + 4]) == 0 ? 'major' : 'minor';
                  $track[] = "$time KeySig $vz $g";
                  $p += 5;
                  break;
                case 0x7F: // Sequencer specific data (string or hexString???)
                  $p += 2;
                  $len = $this->_readVarLen($binStr, $p);
                  if (($len + $p) > $trackLen) {
                    throw new Exception("SeqSpec has corrupt variable length field ($len) [track: $tn dt: $dt]");
                  }
                  $p -= 3;
                  $data = '';
                  for ($i = 0; $i < $len; $i++) {
                    $data .= ' ' . sprintf('%02x', ord($binStr[$p + 3 + $i]));
                  }
                  $track[] = "$time SeqSpec$data";
                  $p += $len + 3;
                  break;

                default:
// MM added: accept "unknown" Meta-Events
                  $metacode = sprintf('%02x', ord($binStr[$p + 1]));
                  $p += 2;
                  $len = $this->_readVarLen($binStr, $p);
                  if (($len + $p) > $trackLen) {
                    throw new Exception("Meta $metacode has corrupt variable length field ($len) [track: $tn dt: $dt]");
                  }
                  $p -= 3;
                  $data = '';
                  for ($i = 0; $i < $len; $i++) {
                    $data .= ' ' . sprintf('%02x', ord($binStr[$p + 3 + $i]));
                  }
                  $track[] = "$time Meta 0x$metacode $data";
                  $p += $len + 3;
                  break;
              } // switch ($meta)
              break; // Ende Meta

            case 0xF0: // SysEx
              $p += 1;
              $len = $this->_readVarLen($binStr, $p);
              if (($len + $p) > $trackLen) {
                throw new Exception("SysEx has corrupt variable length field ($len) [track: $tn dt: $dt p: $p]");
              }
              $str = 'f0';
              for ($i = 0; $i < $len; $i++) {
                $str .= ' ' . sprintf('%02x', ord($binStr[$p + $i]));
              } # FIXED
              $track[] = "$time SysEx $str";
              $p += $len;
              break;
            default: // Repetition of last event?
              switch ($last) {
                case 'On':
                case 'Off':
                  $note = ord($binStr[$p]);
                  $vel = ord($binStr[$p + 1]);
                  $track[] = "$time $last ch=$chan n=$note v=$vel";
                  $p += 2;
                  break;
                case 'PrCh':
                  $prog = ord($binStr[$p]);
                  $track[] = "$time PrCh ch=$chan p=$prog";
                  $p += 1;
                  break;
                case 'PoPr':
                  $note = ord($binStr[$p + 1]);
                  $val = ord($binStr[$p + 2]);
                  $track[] = "$time PoPr ch=$chan n=$note v=$val";
                  $p += 2;
                  break;
                case 'ChPr':
                  $val = ord($binStr[$p]);
                  $track[] = "$time ChPr ch=$chan v=$val";
                  $p += 1;
                  break;
                case 'Par':
                  $c = ord($binStr[$p]);
                  $val = ord($binStr[$p + 1]);
                  $track[] = "$time Par ch=$chan c=$c v=$val";
                  $p += 2;
                  break;
                case 'Pb':
                  $val = (ord($binStr[$p]) & 0x7F) | ((ord($binStr[$p + 1]) & 0x7F) << 7);
                  $track[] = "$time Pb ch=$chan v=$val";
                  $p += 2;
                  break;
                default:
// MM: ToDo: Repetition of SysEx and META-events? with <last>?? \n";
                  throw new Exception("unknown repetition: $last");
              }  // switch ($last)
          } // switch ($byte)
      } // switch ($high)
    } // while

    return $track;
  }


//---------------------------------------------------------------
// variable length string to int (+repositioning)
//---------------------------------------------------------------
  function _readVarLen($str, &$pos) {
    if (($value = ord($str[$pos++])) & 0x80) {
      $value &= 0x7F;
      do {
        $value = ($value << 7) + (($c = ord($str[$pos++])) & 0x7F);
      } while ($c & 0x80);
    }

    return ($value);
  }

}
