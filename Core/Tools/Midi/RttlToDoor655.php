<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.03.2021
 * Time: 19:32
 */


namespace Core\Tools\Midi;


class RttlToDoor655 {
  private string $filename;
  private int $default_duration;
  private int $default_octave;
  private int $default_tempo;
  private array $notes;

  private const NOTE_INDEXES = [
    'p' => 0,
    'c' => 1,
    'c#' => 2,
    'd' => 3,
    'd#' => 4,
    'e' => 5,
    'f' => 6,
    'f#' => 7,
    'g' => 8,
    'g#' => 9,
    'a' => 10,
    'a#' => 11,
    'b' => 12,
  ];

  /**
   * RttlToDoor655 constructor.
   *
   * @param string $rttl
   */
  public function __construct(string $rttl) {
    $data = explode(':', $rttl);

    $this->filename = $data[0];

    $defaults = [];

    preg_match('/d=(\d+)/', $data[1], $defaults);
    $this->default_duration = $defaults[1];

    preg_match('/o=(\d+)/', $data[1], $defaults);
    $this->default_octave = $defaults[1];

    preg_match('/b=(\d+)/', $data[1], $defaults);
    $this->default_tempo = $defaults[1];

    $this->notes = explode(',', $data[2]);
  }

  /**
   * @return string
   */
  public function getDefaults(): string {
    return "d={$this->default_duration},o={$this->default_octave},b={$this->default_tempo}";
  }

  /**
   * @return string
   */
  public function getNotes(): string {
    $tempo = (int)($this->default_tempo / 4);
    $notes = [];
    foreach ($this->notes as $note_data) {
      $notes[] = $this->parseNoteData($note_data);
    }

    $notes_strings = [];
    foreach($notes as $item) {
      $duration = $item['duration'];
      $freq = $item['frequency'];
      $notes_strings[] = "{$duration},{$freq}";
    }
    $notes_str = implode(',', $notes_strings);
    return "const unsigned char {$this->filename}[] PROGMEM = {{$tempo},$notes_str,0};";
  }

  private function parseNoteData(string $note_data): array {
    $data = [];
    preg_match_all('/(1|2|4|8|16|32|64)?((?:[a-g]|h|p)#?){1}(\.?)(4|5|6|7)?/', $note_data, $data);
    $data = array_column($data, 0);
    $duration = $data[1] ?: $this->default_duration;
    $note = $data[2] === 'h' ? 'b' : $data[2];
    $is_dotted = $data[3] === '.';
    $octave = $data[4] ?: $this->default_octave;
    $add = 32 * $octave;
    // код ноты: индекс ноты + 160
    return [
      'note' => $note,
      'duration' => $duration + ($is_dotted ? ($duration / 2) : 0),
      'frequency' => self::NOTE_INDEXES[$note] + $add,
    ];
  }
}
