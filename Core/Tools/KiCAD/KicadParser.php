<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 27.03.2021
 * Time: 9:46
 */


namespace Core\Tools\KiCAD;


class KicadParser {

  public function getComponentsFromNetFile(string $file_content): array {
    $rows = explode(PHP_EOL, $file_content);

    $component_rows = [];
    $is_component_rows_begins = false;
    $is_component_rows_ended = false;
    foreach($rows as $row) {
      if (false !== strpos($row, '(components')) {
        $is_component_rows_begins = true;
      }
      elseif ($is_component_rows_begins) {
        $component_rows[] = $row;
        if (false !== strpos($row, ')))')) {
          $is_component_rows_ended = true;
          $is_component_rows_begins = false;
        }
      }
      if ($is_component_rows_ended) {
        break;
      }
    }

    $components = [];
    $component = [
      'ref' => '',
      'value' => '',
      'footprint' => '',
      'qty' => 1,
    ];
    $match = [];
    foreach($component_rows as $component_row) {
      $component_row = trim($component_row);

      if (false !== strpos($component_row, '(comp (ref')) {
        preg_match('/\(comp \(ref (\w+)\).*/', $component_row, $match);
        $component['ref'] = $match[1];
      }
      elseif (false !== strpos($component_row, '(value')) {
        preg_match('/\(value (.+)\).*/', $component_row, $match);
        $component['value'] = $match[1];

      }
      elseif (false !== strpos($component_row, '(footprint')) {
        preg_match('/\(footprint (\S+)\).*/', $component_row, $match);
        $component['footprint'] = explode(':', $match[1])[1];
        $key = $component['ref'];
        $components[$key] = $component;
      }
    }

    return $this->afterParseActions($components);
  }

  public function getComponentsFromSchFile(string $file_content): array {
    $components = [];

    $match = [];
    preg_match_all('/\$Comp(.*?)\$EndComp/s', $file_content, $match);

    $component = [
      'ref' => '',
      'value' => '',
      'footprint' => '',
      'qty' => 1,
    ];
    foreach($match[1] as $data) {

      $items = explode(PHP_EOL, $data);

      foreach($items as $item) {
        $item = trim($item);
        if (empty($item[0])) {
          continue;
        }

        switch($item[0]) {
          case 'L':
            $values = explode(' ', $item);
            $component['ref'] = $values[2];
            break;

          case 'F':
            $item_data = [];
            preg_match('/F (.*) \"(.*)\"/', $item, $item_data);
            if (1 == $item_data[1]) {
              $component['value'] = $item_data[2];
            }
            elseif (2 == $item_data[1]) {
              $footprint = $item_data[2];

              if (!empty($footprint)) {
                $component['footprint'] = explode(':', $footprint)[1];

                $key = $component['ref'];
                $components[$key] = $component;
              }
            }
            break;

          default:
            break;
        }
      }
    }

    return $this->afterParseActions($components);
  }

  private function afterParseActions(array $components): array {
    uasort($components, [__CLASS__, 'cmpComponents']);

    return $this->sumSameValueAndFootprint($components);
  }

  private function sumSameValueAndFootprint(array $components): array {
    $summarized = [];
    foreach($components as $component) {
      $component['refs'] = [];
      $key = "{$component['value']}_{$component['footprint']}";
      if (isset($summarized[$key])) {
        $summarized[$key]['qty']++;
      }
      else {
        $summarized[$key] = $component;
      }
      $summarized[$key]['refs'][] = $component['ref'];
    }

    foreach($summarized as &$component) {
      $component['ref'] = implode(',', $component['refs']);
      unset($component['refs']);
    }
    unset($component);

    return $summarized;
  }

  private static function cmpComponents($component1, $component2) {
    return $component1['ref'] <=> $component2['ref'];
  }
}
