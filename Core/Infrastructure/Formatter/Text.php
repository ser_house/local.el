<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 13.06.2020
 * Time: 10:31
 */


namespace Core\Infrastructure\Formatter;


class Text {

  /**
   * Склоняем словоформу
   * @ author runcore
   *
   * @param $n
   * @param $f1
   * @param $f2
   * @param $f5
   *
   * @return mixed
   */
  public function morph($n, $f1, $f2, $f5) {
    $n = abs((int)$n) % 100;
    if ($n > 10 && $n < 20) {
      return $f5;
    }
    $n %= 10;
    if ($n > 1 && $n < 5) {
      return $f2;
    }
    if ($n == 1) {
      return $f1;
    }

    return $f5;
  }

  public function mbUcfirst($string, $encoding = 'UTF-8'): string {
    $strlen = mb_strlen($string, $encoding);
    $firstChar = mb_substr($string, 0, 1, $encoding);
    $then = mb_substr($string, 1, $strlen - 1, $encoding);

    return mb_strtoupper($firstChar, $encoding) . $then;
  }
}
