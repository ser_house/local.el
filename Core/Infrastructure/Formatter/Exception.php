<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 10.05.2020
 * Time: 12:43
 */


namespace Core\Infrastructure\Formatter;


use Throwable;

class Exception {
  public function format(Throwable $e): string {
    $output = $e->getMessage() . '<br>';

    $trace = $e->getTrace();

    $file = 'file';

    $i = 0;
    $max_trace_lines = 5;
    foreach ($trace as $trace_item) {
      if ($i === $max_trace_lines) {
        break;
      }
      $i++;
      if (isset($trace_item['file'])) {
        $file = basename($trace_item['file']);
      }
      elseif (isset($trace_item['class'])) {
        $file = basename($trace_item['class']);
      }

      $line = $trace_item['line'] ?? '';

      $output .= "$file: $line" . '<br>';
    }

    return $output;
  }
}
