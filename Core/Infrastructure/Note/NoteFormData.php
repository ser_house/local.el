<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2021
 * Time: 17:47
 */


namespace Core\Infrastructure\Note;


use JsonSerializable;

class NoteFormData implements JsonSerializable {

  private NoteData $noteData;
  private array $files;
  private array $file_rules;
  private string $pdf_thumbnail_url;

  /**
   * NoteFormData constructor.
   *
   * @param NoteData $noteData
   * @param array $file_rules
   * @param string $pdf_thumbnail_url
   */
  public function __construct(NoteData $noteData, array $file_rules, string $pdf_thumbnail_url) {
    $this->noteData = $noteData;
    $this->files = [];
    $this->file_rules = $file_rules;
    $this->pdf_thumbnail_url = $pdf_thumbnail_url;
  }

  /**
   * @param NoteFileData $noteFileData
   *
   * @return $this
   */
  public function addFileData(NoteFileData $noteFileData): self {
    $this->files[] = $noteFileData;
    return $this;
  }

  /**
   *
   * @return string
   */
  public function getId(): string {
    return $this->noteData->id();
  }

  /**
   *
   * @return string
   */
  public function getTitle(): string {
    return $this->noteData->title();
  }

  /**
   *
   * @return string
   */
  public function getText(): string {
    return $this->noteData->text();
  }

  /**
   *
   * @return string
   */
  public function getPdfThumbUrl(): string {
    return $this->pdf_thumbnail_url;
  }

  /**
   *
   * @return array
   */
  public function getFileRules(): array {
    return $this->file_rules;
  }

  /**
   *
   * @return array
   */
  public function getFiles(): array {
    return $this->files;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'note' => $this->noteData,
      'note_files' => $this->files,
      'note_file_rules' => $this->file_rules,
      'pdf_thumbnail_url' => $this->pdf_thumbnail_url,
    ];
  }
}
