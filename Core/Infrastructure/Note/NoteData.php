<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2021
 * Time: 17:28
 */


namespace Core\Infrastructure\Note;


use Core\Note\Note;
use JsonSerializable;

class NoteData implements JsonSerializable {

  private string $id;
  private string $title;
  private string $text;
  private string $added_at;

  /**
   * NoteData constructor.
   *
   * @param Note|null $note
   */
  public function __construct(Note $note = null) {
    $this->id = $note ? (string)$note->getId() : '';
    $this->title = $note ? $note->getTitle() : '';
    $this->text = $note ? $note->getText() : '';
    $this->added_at = '';
    if ($note) {
      $addedAt = $note->getAddedAt();
      if ($addedAt) {
        $this->added_at = $addedAt->format('d.m.Y H:s');
      }
    }
  }

  /**
   * @return string
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * @return string
   */
  public function title(): string {
    return $this->title;
  }

  /**
   * @return string
   */
  public function text(): string {
    return $this->text;
  }

  /**
   * @return string
   */
  public function addedAt(): string {
    return $this->added_at;
  }


  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'id' => $this->id,
      'title' => $this->title,
      'text' => $this->text,
      'added_at' => $this->added_at,
    ];
  }
}
