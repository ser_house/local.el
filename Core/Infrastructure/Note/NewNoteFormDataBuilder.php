<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2021
 * Time: 17:44
 */


namespace Core\Infrastructure\Note;


use App\NoteFile\FileValidator;
use Core\File\IFileUrlResolver;

class NewNoteFormDataBuilder {
  private FileValidator $fileValidator;
  private IFileUrlResolver $fileUrlResolver;

  /**
   * NewNoteFormDataBuilder constructor.
   *
   * @param FileValidator $fileValidator
   * @param IFileUrlResolver $fileUrlResolver
   */
  public function __construct(FileValidator $fileValidator, IFileUrlResolver $fileUrlResolver) {
    $this->fileValidator = $fileValidator;
    $this->fileUrlResolver = $fileUrlResolver;
  }

  /**
   *
   * @return NoteFormData
   */
  public function build(): NoteFormData {
    $note_file_rules = [
      'types' => $this->fileValidator->getTypes(),
      'max_filesize' => $this->fileValidator->getMaxFileSize(),
      'help' => $this->fileValidator->getHelp(),
    ];

    $pdf_thumbnail_url = $this->fileUrlResolver->getPdfThumbUrl();

    return new NoteFormData(new NoteData(), $note_file_rules, $pdf_thumbnail_url);
  }
}
