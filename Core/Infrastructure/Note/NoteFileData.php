<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2021
 * Time: 17:33
 */


namespace Core\Infrastructure\Note;


use Core\File\File;
use Core\File\IFileUrlResolver;
use JsonSerializable;

class NoteFileData implements JsonSerializable {

  private string $id;
  private string $name;
  private string $human_name;
  private string $url;
  private string $thumb_url;
  private string $delete_url;

  /**
   * NoteFileData constructor.
   *
   * @param IFileUrlResolver $fileUrlResolver
   * @param File $file
   */
  public function __construct(IFileUrlResolver $fileUrlResolver, File $file) {
    $this->id = (string)$file->getId();
    $this->name = $file->getFilename();
    $this->human_name = $file->getHumanName();
    $this->url = $fileUrlResolver->resolveFileUrl($file);
    $this->thumb_url = $fileUrlResolver->resolveFileThumbUrl($file);
    $this->delete_url = route('api.files.delete', ['noteFile' => (string)$file->getId()]);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return [
      'id' => $this->id,
      'name' => $this->name,
      'human_name' => $this->human_name,
      'url' => $this->url,
      'thumb_url' => $this->thumb_url,
      'delete_url' => $this->delete_url,
    ];
  }
}
