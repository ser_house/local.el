<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2021
 * Time: 17:44
 */


namespace Core\Infrastructure\Note;


use App\NoteFile\FileValidator;
use Core\File\IFileUrlResolver;
use Core\Note\INoteFileGateway;
use Core\Note\Note;

class NoteFormDataBuilder {
  private FileValidator $fileValidator;
  private IFileUrlResolver $fileUrlResolver;
  private INoteFileGateway $fileGateway;

  /**
   * NoteFormDataBuilder constructor.
   *
   * @param FileValidator $fileValidator
   * @param IFileUrlResolver $fileUrlResolver
   * @param INoteFileGateway $fileGateway
   */
  public function __construct(FileValidator $fileValidator, IFileUrlResolver $fileUrlResolver, INoteFileGateway $fileGateway) {
    $this->fileValidator = $fileValidator;
    $this->fileUrlResolver = $fileUrlResolver;
    $this->fileGateway = $fileGateway;
  }

  public function build(Note $note): NoteFormData {
    $note_file_rules = [
      'types' => $this->fileValidator->getTypes(),
      'max_filesize' => $this->fileValidator->getMaxFileSize(),
      'help' => $this->fileValidator->getHelp(),
    ];

    $pdf_thumbnail_url = $this->fileUrlResolver->getPdfThumbUrl();


    $formData = new NoteFormData(new NoteData($note), $note_file_rules, $pdf_thumbnail_url);
    $noteFiles = $this->fileGateway->get($note->getId());
    foreach ($noteFiles as $noteFile) {
      $formData->addFileData(new NoteFileData($this->fileUrlResolver, $noteFile));
    }

    return $formData;
  }
}
