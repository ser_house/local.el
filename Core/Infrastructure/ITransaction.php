<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 04.04.2021
 * Time: 17:17
 */


namespace Core\Infrastructure;


/**
 * Interface ITransaction
 *
 * @package Core\Infrastructure
 */
interface ITransaction {

  /**
   *
   */
  public function begin(): void;

  /**
   *
   */
  public function commit(): void;

  /**
   *
   */
  public function rollback(): void;
}
