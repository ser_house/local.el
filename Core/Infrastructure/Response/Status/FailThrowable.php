<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 13:03
 */


namespace Core\Infrastructure\Response\Status;


use Throwable;

/**
 * Class FailThrowable
 *
 * @package Core\Infrastructure\Response\Status
 */
class FailThrowable extends Fail {

  /**
   * FailThrowable constructor.
   *
   * @param Throwable $e
   * @param array $vars
   */
  public function __construct(Throwable $e, array $vars = []) {
    parent::__construct($e->getMessage(), $vars);
  }
}
