<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2020
 * Time: 10:03
 */


namespace Core\Infrastructure\Response\Status;


/**
 * Class Success
 *
 * @package Core\Infrastructure\Response\Status
 */
class Success extends Base {

  protected const STATUS = 'success';
}
