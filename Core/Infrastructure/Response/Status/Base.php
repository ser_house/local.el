<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2020
 * Time: 10:04
 */


namespace Core\Infrastructure\Response\Status;

use JsonSerializable;

/**
 * Class Base
 *
 * @package Core\Infrastructure\Response\Status
 */
class Base implements JsonSerializable {
  protected const STATUS = '';

  protected string $msg;
  protected array $vars;

  /**
   * Base constructor.
   *
   * @param string $msg
   * @param array $vars
   */
  public function __construct(string $msg = '', array $vars = []) {
    $this->msg = $msg;
    $this->vars = $vars;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    $json = [
      'status' => static::STATUS,
      'msg' => $this->msg,
    ];
    if ($this->vars) {
      $json = array_merge($json, $this->vars);
    }

    return $json;
  }
}
