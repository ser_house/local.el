<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 23.04.2020
 * Time: 10:03
 */


namespace Core\Infrastructure\Response\Status;

/**
 * Class Fail
 *
 * @package Core\Infrastructure\Response\Status
 */
class Fail extends Base {

  protected const STATUS = 'fail';

}
