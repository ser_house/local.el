<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.10.2020
 * Time: 9:57
 */


namespace Core\Note\View;


use Core\File\File;
use Core\File\IFileUrlResolver;
use Core\Note\INoteFileGateway;
use Core\Note\Note;
use Illuminate\Support\Facades\Storage;

class NotePresenter {
  private INoteFileGateway $noteFileGateway;
  private IFileUrlResolver $fileUrlResolver;

  /**
   * NotePresenter constructor.
   *
   * @param INoteFileGateway $noteFileGateway
   * @param IFileUrlResolver $fileUrlResolver
   */
  public function __construct(INoteFileGateway $noteFileGateway, IFileUrlResolver $fileUrlResolver) {
    $this->noteFileGateway = $noteFileGateway;
    $this->fileUrlResolver = $fileUrlResolver;
  }


  public function short(Note $note): array {

    return [
      'id' => (string)$note->getId(),
      'added_at' => $note->getAddedAt()->format('d.m.Y H:s'),
      'title' => htmlspecialchars($note->getTitle()),
    ];
  }

  public function shortWithFiles(Note $note): array {
    $view = $this->short($note);
    $noteFiles = $this->noteFileGateway->get($note->getId());
    $view['files'] = [];
    foreach ($noteFiles as $noteFile) {
      $view['files'][] = $this->noteFile($noteFile);
    }

    return $view;
  }

  public function full(Note $note): array {
    $text = $note->getText();
    $view = $this->shortWithFiles($note);
    $view['text'] = $text ?: nl2br(e($text), false);

    return $view;
  }

  public function formatFileSize(int $bytes): string {
    if ($bytes >= 1000000000) {
      return number_format($bytes / 1000000000, 0, ',', ' ') . ' Gb';
    }
    if ($bytes >= 1000000) {
      return number_format($bytes / 1000000, 0, ',', ' ') . ' Mb';
    }

    return number_format($bytes / 1000, 0, ',', ' ') . ' Kb';
  }

  private function noteFile(File $file): NoteFileView {
    $url = $this->fileUrlResolver->resolveFileUrl($file);
    $path = $file->getPath();
    $name = $file->getFilename();
    $size = Storage::size("$path/$name");

    $view = new NoteFileView();
    $view->id = (string)$file->getId();
    $view->name = $name;
    $view->human_name = $file->getHumanName();
    $view->url = $url;
    $view->thumb_url = $this->fileUrlResolver->resolveFileThumbUrl($file);
    $view->size = $this->formatFileSize($size);

    return $view;
  }
}
