<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 9:21
 */


namespace Core\Note\View;


class NoteFileView {
  public string $id;
  public string $name;
  public string $human_name;
  public string $url;
  public string $thumb_url;
  public string $size;
}
