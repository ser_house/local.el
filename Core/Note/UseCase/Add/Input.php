<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.11.2020
 * Time: 13:20
 */


namespace Core\Note\UseCase\Add;


use Exception;

class Input {

  private string $title;
  private string $text;

  /**
   * Input constructor.
   *
   * @param array $input
   *
   * @throws Exception
   */
  public function __construct(array $input) {
    if (empty($input['title'])) {
      throw new Exception("Поле 'title' не может быть пустым.");
    }

    $this->title = $input['title'];
    $this->text = $input['text'] ?? '';
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getText(): string {
    return $this->text;
  }
}
