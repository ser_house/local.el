<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.11.2020
 * Time: 13:20
 */


namespace Core\Note\UseCase\Add;


use Core\Note\INoteGateway;
use Core\Note\Note;
use Core\Note\NoteBuilder;

class Action {
  private INoteGateway $noteGateway;

  /**
   * Action constructor.
   *
   * @param INoteGateway $noteGateway
   */
  public function __construct(INoteGateway $noteGateway) {
    $this->noteGateway = $noteGateway;
  }


  public function run(Input $input): Note {
    $note = NoteBuilder::create($input->getTitle(), $input->getText());
    $this->noteGateway->add($note);

    return $note;
  }
}
