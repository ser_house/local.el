<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.11.2020
 * Time: 13:20
 */


namespace Core\Note\UseCase\Update;


use Core\Note\NoteId;
use Exception;

class Input {

  private NoteId $id;
  private string $title;
  private string $text;

  /**
   * Input constructor.
   *
   * @param string $id
   * @param array $input
   *
   * @throws Exception
   */
  public function __construct(string $id, array $input) {
    $this->id = new NoteId($id);

    if (empty($input['title'])) {
      throw new Exception("Поле 'title' не может быть пустым.");
    }

    $this->title = $input['title'];
    $this->text = $input['text'] ?? '';
  }

  /**
   * @return NoteId
   */
  public function getId(): NoteId {
    return $this->id;
  }

  /**
   * @return mixed|string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return mixed|string
   */
  public function getText(): string {
    return $this->text;
  }
}
