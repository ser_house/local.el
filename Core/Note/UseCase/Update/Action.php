<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.11.2020
 * Time: 13:20
 */


namespace Core\Note\UseCase\Update;


use Core\Infrastructure\ITransaction;
use Core\Note\INoteGateway;
use Core\Note\Note;
use Core\Note\NoteBuilder;
use DomainException;
use Exception;

class Action {

  private INoteGateway $noteGateway;
  private ITransaction $transaction;

  /**
   * Action constructor.
   *
   * @param INoteGateway $noteGateway
   * @param ITransaction $transaction
   */
  public function __construct(INoteGateway $noteGateway, ITransaction $transaction) {
    $this->noteGateway = $noteGateway;
    $this->transaction = $transaction;
  }


  public function run(Input $input): Note {
    $noteId = $input->getId();
    $note = $this->noteGateway->getById($noteId);
    if (null === $note) {
      throw new DomainException("Заметка с id '$noteId' не найдена.");
    }

    $note = NoteBuilder::changedNote($note, $input->getTitle(), $input->getText());

    $this->transaction->begin();
    try {
      $this->noteGateway->update($note);
      $this->transaction->commit();
    }
    catch (Exception $e) {
      $this->transaction->rollBack();
      throw $e;
    }

    return $note;
  }
}
