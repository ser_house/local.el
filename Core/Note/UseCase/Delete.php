<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.11.2020
 * Time: 21:15
 */


namespace Core\Note\UseCase;


use Core\Infrastructure\ITransaction;
use Core\Note\INoteFileGateway;
use Core\Note\INoteGateway;
use Core\Note\Note;
use Core\Note\NoteId;
use DomainException;
use Throwable;

class Delete {
  private INoteGateway $noteGateway;
  private INoteFileGateway $noteFileGateway;
  private ITransaction $transaction;

  /**
   * Delete constructor.
   *
   * @param INoteGateway $noteGateway
   * @param INoteFileGateway $noteFileGateway
   * @param ITransaction $transaction
   */
  public function __construct(INoteGateway $noteGateway, INoteFileGateway $noteFileGateway, ITransaction $transaction) {
    $this->noteGateway = $noteGateway;
    $this->noteFileGateway = $noteFileGateway;
    $this->transaction = $transaction;
  }


  public function run(NoteId $noteId): Note {
    $note = $this->noteGateway->getById($noteId);
    if (null === $note) {
      throw new DomainException("Заметка с id '$noteId' не найдена.");
    }

    $this->transaction->begin();
    try {
      $this->noteGateway->remove($noteId);
      $this->noteFileGateway->remove($noteId);
      $this->transaction->commit();
      return $note;
    }
    catch (Throwable $e) {
      $this->transaction->rollBack();
      throw $e;
    }
  }
}
