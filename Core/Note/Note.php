<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.09.2020
 * Time: 19:51
 */


namespace Core\Note;

use DateTimeImmutable;

/**
 * Class Note
 *
 * @package Core\Note
 */
class Note {
  protected DateTimeImmutable $addedAt;
  protected NoteId $id;
  protected string $title;
  protected string $text;

  /**
   * Note constructor.
   *
   * @param string $title
   * @param string $text
   */
  public function __construct(string $title, string $text) {
    $this->title = $title;
    $this->text = $text;
  }

  /**
   * @return string
   */
  public function getTitle(): string {
    return $this->title;
  }

  /**
   * @return NoteId
   */
  public function getId(): NoteId {
    return $this->id;
  }

  /**
   * @return string
   */
  public function getText(): string {
    return $this->text;
  }

  /**
   * @return DateTimeImmutable
   */
  public function getAddedAt(): DateTimeImmutable {
    return $this->addedAt;
  }
}
