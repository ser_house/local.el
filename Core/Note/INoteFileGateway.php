<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.04.2021
 * Time: 9:01
 */


namespace Core\Note;


interface INoteFileGateway {
  /**
   * @param NoteId $noteId
   */
  public function remove(NoteId $noteId): void;

  /**
   * @param NoteId $noteId
   *
   * @return array
   */
  public function get(NoteId $noteId): array;
}
