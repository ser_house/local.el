<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.09.2020
 * Time: 10:44
 */


namespace Core\Note;


use DateTimeImmutable;
use Exception;
use stdClass;

class NoteBuilder extends Note {

  /**
   * @param string $title
   * @param string $text
   *
   * @return Note
   */
  public static function create(string $title, string $text): Note {
    $note = new Note($title, $text);
    $note->id = new NoteId();

    return $note;
  }

  /**
   * @param NoteId $id
   * @param string $title
   * @param string $text
   * @param DateTimeImmutable $addedAt
   *
   * @return Note
   */
  public static function build(NoteId $id, string $title, string $text, DateTimeImmutable $addedAt): Note {
    $note = new Note($title, $text);
    $note->id = $id;
    $note->addedAt = $addedAt;

    return $note;
  }

  /**
   * Для восстановления из базы.
   *
   * @param stdClass $obj
   *
   * @return Note
   * @throws Exception
   */
  public static function buildFromStdObject(stdClass $obj): Note {
    return self::build(
      new NoteId($obj->id),
      $obj->title,
      $obj->value,
      new DateTimeImmutable($obj->added_at),
    );
  }

  public static function changedNote(Note $originalNote, string $new_title, string $new_text): Note {
    $clone = new Note($new_title, $new_text);
    $clone->id = $originalNote->getId();
    $clone->addedAt = DateTimeImmutable::createFromFormat('Y-m-d H:i:s.v', $originalNote->getAddedAt()->format('Y-m-d H:i:s.v'));
    return $clone;
  }
}
