<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 8:42
 */


namespace Core\Note;


interface INoteGateway {
  /**
   * @param NoteId $noteId
   *
   * @return Note|null
   */
  public function getById(NoteId $noteId): ?Note;

  /**
   * @param string $title
   *
   * @return Note|null
   */
  public function getByTitle(string $title): ?Note;

  /**
   *
   * @return array
   */
  public function all(): array;

  /**
   * @param Note $note
   */
  public function add(Note $note): void;

  /**
   * @param Note $note
   */
  public function update(Note $note): void;

  /**
   * @param NoteId $noteId
   */
  public function remove(NoteId $noteId): void;
}
