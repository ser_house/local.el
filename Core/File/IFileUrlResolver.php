<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2021
 * Time: 18:34
 */


namespace Core\File;


interface IFileUrlResolver {
  public const PDF_THUMBNAIL_FILE = 'pdf_thumbnail.png';

  /**
   *
   * @return string
   */
  public function getTargetUploads(): string;

  /**
   * @param File $file
   *
   * @return string
   */
  public function resolveFileUrl(File $file): string;

  /**
   * @param File $file
   *
   * @return string
   */
  public function resolveFileThumbUrl(File $file): string;

  /**
   * @param File $file
   *
   * @return string
   */
  public function getFileThumbFileName(File $file): string;

  /**
   *
   * @return string
   */
  public function getPdfThumbUrl(): string;
}
