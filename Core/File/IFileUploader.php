<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 12:13
 */


namespace Core\File;


use Core\Id;


interface IFileUploader {
  public function upload(IUploadedFileInfo $uploadedFileInfo, IUploadedFileResizer $uploadedFileResizer, IUploadedFileSaver $uploadedFileSaver, ?string $human_name, Id $entityId): File;
}
