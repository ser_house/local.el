<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 13:36
 */


namespace Core\File;


interface IUploadedFileSaver {

  /**
   * @param string $destination
   * @param string $filename
   */
  public function save(string $destination, string $filename): void;
}
