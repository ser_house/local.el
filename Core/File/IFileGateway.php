<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 12:20
 */


namespace Core\File;


interface IFileGateway {
  /**
   * @param File $file
   */
  public function add(File $file): void;
}
