<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 12:17
 */


namespace Core\File;


use Core\Id;

class File {

  private FileId $id;
  private Id $entityId;
  private string $filename;
  private string $path;
  private string $human_name;
  private string $mime;

  /**
   * File constructor.
   *
   * @param FileId $id
   * @param Id $entityId
   * @param string $filename
   * @param string $path
   * @param string $human_name
   * @param string $mime
   */
  public function __construct(FileId $id, Id $entityId, string $filename, string $path, string $human_name, string $mime) {
    $this->id = $id;
    $this->entityId = $entityId;
    $this->filename = $filename;
    $this->path = $path;
    $this->human_name = $human_name;
    $this->mime = $mime;
  }

  /**
   * @return FileId
   */
  public function getId(): FileId {
    return $this->id;
  }

  /**
   * @return Id
   */
  public function getEntityId(): Id {
    return $this->entityId;
  }

  /**
   * @return string
   */
  public function getFilename(): string {
    return $this->filename;
  }

  /**
   * @return string
   */
  public function getPath(): string {
    return $this->path;
  }

  /**
   * @return string
   */
  public function getHumanName(): string {
    return $this->human_name;
  }

  /**
   * @return string
   */
  public function getMime(): string {
    return $this->mime;
  }
}
