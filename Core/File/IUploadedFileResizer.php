<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 11:57
 */


namespace Core\File;


/**
 * Interface IUploadedFileResizer
 *
 * @package Core
 */
interface IUploadedFileResizer {

  /**
   * @param int|null $width
   * @param int|null $height
   *
   * @return string
   */
  public function resize(int $width = null, int $height = null): string;
}
