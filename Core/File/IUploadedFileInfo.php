<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 11:52
 */


namespace Core\File;


/**
 * Interface IUploadedFileInfo
 *
 * @package Core
 */
interface IUploadedFileInfo {

  /**
   *
   * @return string
   */
  public function getFilename(): string;

  /**
   *
   * @return string
   */
  public function getMime(): string;

  /**
   *
   * @return string
   */
  public function getExtension(): string;
}
