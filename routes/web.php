<?php

use App\Http\Controllers\Elements;
use App\Http\Controllers\Notes;
use App\Http\Controllers\Tools;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Elements\Home::class)->name('home');


Route::prefix('packages')->group(function () {
  Route::get('/', Elements\Packages\Manage::class)->name('packages.manage');
});

Route::prefix('notes')->group(function () {
  Route::get('/', Notes\Home::class)->name('notes');
  Route::get('new', Notes\NewNote::class)->name('notes.new');
  Route::get('edit/{note_id}', Notes\EditNote::class)->name('notes.edit');
  Route::get('view/{note_id}', Notes\ViewNote::class)->name('notes.view');
  Route::delete('delete/{note_id}', Notes\DeleteNote::class)->name('notes.delete');
});

Route::prefix('tools')->group(function () {
  Route::get('midi-rttl', [Tools\MidiRttl::class, 'page'])->name('tools.midi_rttl_page');
  Route::post('midi-rttl', [Tools\MidiRttl::class, 'convert'])->name('tools.midi_rttl_convert');
  Route::get('kicad-bom', [Tools\KicadBom::class, 'page'])->name('tools.kicad_bom_page');
  Route::post('kicad-bom', [Tools\KicadBom::class, 'generate'])->name('tools.kicad_bom_generate');
  Route::get('calculators', Tools\Calculators::class)->name('tools.calculators');
});
