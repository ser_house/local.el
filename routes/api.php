<?php

use App\Http\Controllers\Elements\Element;
use App\Http\Controllers\Elements\Packages;
use App\Http\Controllers\Elements\Type;
use App\Http\Controllers\FileController;
use App\Http\Controllers\Notes;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('json_unicode')->group(function () {
  Route::prefix('element')->group(function () {
    Route::post('add', Element\Add::class)->name('api.element.add');
    Route::put('rename', Element\Rename::class)->name('api.element.rename');
    Route::put('move-to-type', Element\MoveToType::class)->name('api.element.move_to_type');
    Route::put('replace-package', Element\ReplacePackage::class)->name('api.element.replace_package');
    Route::post('add-package-qty', Element\AddPackageQty::class)->name('api.element.add_package_qty');
    Route::post('increase-qty', Element\IncreaseQty::class)->name('api.element.increase_qty');
    Route::post('decrease-qty', Element\DecreaseQty::class)->name('api.element.decrease_qty');
    Route::get('list-by-type/{type}', Element\ListByType::class)->name('api.elements.list_by_type');
    Route::get('list-with-package/{package}', Element\ListWithPackage::class)->name('api.elements.list_with_package');
    Route::post('generate-by-series/{package}/{type}', Element\GenerateBySeries::class)->name('api.elements.generate_by_series');
    Route::post('merge', Element\Merge::class)->name('api.element.merge');
  });

  Route::prefix('packages')->group(function () {
    Route::post('add', Packages\Add::class)->name('api.package.add');
    Route::put('rename', Packages\Rename::class)->name('api.package.rename');
    Route::delete('delete/{package}', Packages\Delete::class)->name('api.package.delete');
  });

  Route::prefix('types')->group(function () {
    Route::post('add', Type\Add::class)->name('api.type.add');
    Route::put('change', Type\Change::class)->name('api.type.change');
    Route::put('moveToType', Type\MoveToType::class)->name('api.type.move_to_type');
    Route::delete('delete/{type}', Type\Delete::class)->name('api.type.delete');
  });

  Route::prefix('notes')->group(function () {
    Route::post('add', Notes\AddNote::class)->name('api.notes.add');
    Route::post('update/{note_id}', Notes\UpdateNote::class)->name('api.notes.update');
  });

  Route::prefix('files')->group(function() {
    Route::delete('delete/{noteFile}', [FileController::class, 'delete'])->name('api.files.delete');
    Route::put('rename/{noteFile}', [FileController::class, 'rename'])->name('api.files.rename');
  });
});
