<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.04.2021
 * Time: 9:41
 */

namespace Notes\UseCase;

use Core\Note\INoteGateway;
use Core\Note\NoteBuilder;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class ChangeNoteTest extends TestCaseDb {
  private INoteGateway $noteGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->noteGateway = $this->app->make(INoteGateway::class);
  }


  public function testHttpApi() {
    $title = 'Note title';
    $text = 'Note text';
    $note = NoteBuilder::create($title, $text);
    $this->noteGateway->add($note);
    $noteId = $note->getId();

    $title_changed = 'Note title changed';
    $text_changed = 'Note text changed';
    $data = [
      'title' => $title_changed,
      'text' => $text_changed,
    ];

    $response = $this->postJson(route('api.notes.update', ['note_id' => (string)$noteId]), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);

    $note = $this->noteGateway->getById($noteId);
    self::assertNotNull($note);
    self::assertEquals($title_changed, $note->getTitle());
    self::assertEquals($text_changed, $note->getText());
  }
}
