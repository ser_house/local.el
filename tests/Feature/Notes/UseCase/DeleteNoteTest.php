<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.04.2021
 * Time: 9:41
 */

namespace Notes\UseCase;

use Core\Note\INoteGateway;
use Core\Note\NoteBuilder;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class DeleteNoteTest extends TestCaseDb {
  private INoteGateway $noteGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->noteGateway = $this->app->make(INoteGateway::class);
  }


  public function testHttpApi() {
    $title = 'Note title';
    $text = 'Note text';
    $note = NoteBuilder::create($title, $text);
    $this->noteGateway->add($note);
    $noteId = $note->getId();
    // Доказываем, что заметка есть.
    $note = $this->noteGateway->getById($noteId);
    self::assertNotNull($note);

    $this->followingRedirects();
    $response = $this->delete(route('notes.delete', ['note_id' => (string)$noteId]));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertSee('alert-success');

    $note = $this->noteGateway->getById($noteId);
    self::assertNull($note);
  }
}
