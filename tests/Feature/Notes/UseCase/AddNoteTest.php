<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.04.2021
 * Time: 9:41
 */

namespace Notes\UseCase;

use Core\Note\INoteGateway;
use Tests\TestCaseDb;
use Illuminate\Http\Response;

class AddNoteTest extends TestCaseDb {
  private INoteGateway $noteGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->noteGateway = $this->app->make(INoteGateway::class);
  }


  public function testHttpApi() {
    $title = 'Note title';
    $text = 'Note text';

    $data = [
      'title' => $title,
      'text' => $text,
    ];
    $response = $this->postJson(route('api.notes.add'), $data);
    $response->assertStatus(Response::HTTP_CREATED);
    $response->assertJson(['status' => 'success']);

    $note = $this->noteGateway->getByTitle($title);
    self::assertNotNull($note);
  }
}
