<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\Type;

use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\TypeId;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class ChangeTypeTest extends TestCaseDb {

  private ITypeGateway $typeGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->typeGateway = $this->app->make(ITypeGateway::class);
  }


  public function testHttpApi() {
    $id = 'type_id_1';
    $title = 'Type';

    $typeId = new TypeId($id);

    $type = $this->typeGateway->getById($typeId);
    self::assertNotNull($type);
    self::assertEquals('Тестовый тип', $type->title());
    self::assertFalse($type->hasSeries());

    $data = [
      'id' => $id,
      'title' => $title,
      'has_series' => true,
    ];
    $response = $this->putJson(route('api.type.change'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);

    $type = $this->typeGateway->getById($typeId);
    self::assertEquals($title, $type->title());
    self::assertTrue($type->hasSeries());
  }

  public function testHttpApiDuplicateFail() {
    $id = 'type_id_1';
    $title = 'Тестовый тип 2';

    $data = [
      'id' => $id,
      'title' => $title,
      'has_series' => true,
    ];
    $response = $this->putJson(route('api.type.change'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'fail']);
  }
}
