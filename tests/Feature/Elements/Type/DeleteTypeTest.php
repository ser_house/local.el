<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\Type;

use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\TypeId;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class DeleteTypeTest extends TestCaseDb {

  private ITypeGateway $typeGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->typeGateway = $this->app->make(ITypeGateway::class);
  }

  public function testHttpApi() {
    $id = 'type_id_e24';
    $typeId = new TypeId($id);

    $type = $this->typeGateway->getById($typeId);
    self::assertNotNull($type);

    $data = [
      'type' => $id,
    ];
    $response = $this->deleteJson(route('api.type.delete', $data));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);

    $type = $this->typeGateway->getById($typeId);
    self::assertNull($type);
  }

  public function testHttpApiFailWithElements() {
    $id = 'type_id_1';
    $typeId = new TypeId($id);

    $type = $this->typeGateway->getById($typeId);
    self::assertNotNull($type);

    $data = [
      'type' => $id,
    ];
    $response = $this->deleteJson(route('api.type.delete', $data));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'fail']);

    $type = $this->typeGateway->getById($typeId);
    self::assertNotNull($type);
  }
}
