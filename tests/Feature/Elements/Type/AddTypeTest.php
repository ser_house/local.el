<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\Type;

use Core\Elements\Type\ITypeGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class AddTypeTest extends TestCaseDb {

  private ITypeGateway $typeGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->typeGateway = $this->app->make(ITypeGateway::class);
  }


  public function testHttpApi() {
    $title = 'Type';

    $data = [
      'title' => $title,
      'has_series' => false,
    ];
    $response = $this->postJson(route('api.type.add'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
    $response->assertJson([
      'item' => [
        'title' => $title,
      ],
    ]);

    $type = $this->typeGateway->getByTitle($title);
    self::assertNotNull($type);
  }

  public function testHttpApiDuplicateFail() {
    $title = 'Тестовый тип';

    $type = $this->typeGateway->getByTitle($title);
    self::assertNotNull($type);

    $data = [
      'title' => $title,
      'has_series' => false,
    ];
    $response = $this->postJson(route('api.type.add'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'fail']);
  }
}
