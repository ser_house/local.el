<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\Type;

use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\TypeId;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class MoveToTypeTest extends TestCaseDb {

  private ITypeGateway $typeGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->typeGateway = $this->app->make(ITypeGateway::class);
  }


  public function testHttpApi() {
    $id = 'type_id_1';
    $parent_id = 'type_id_2';

    $typeId = new TypeId($id);

    $type = $this->typeGateway->getById($typeId);
    self::assertNotNull($type);
    self::assertNotEquals($parent_id, (string)$type->parentId());

    $data = [
      'id' => $id,
      'parent_id' => $parent_id,
    ];
    $response = $this->putJson(route('api.type.move_to_type'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);

    $type = $this->typeGateway->getById($typeId);
    self::assertEquals($parent_id, (string)$type->parentId());
  }
}
