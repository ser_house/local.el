<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\Package;

use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\PackageId;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class DeletePackageTest extends TestCaseDb {

  private IPackageGateway $packageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->packageGateway = $this->app->make(IPackageGateway::class);
  }


  public function testHttpApi() {
    $id = 'package_id_e24';

    $data = [
      'package' => $id,
    ];
    $response = $this->deleteJson(route('api.package.delete', $data));
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);

    $package = $this->packageGateway->getById(new PackageId($id));
    self::assertNull($package);
  }

  public function testHttpApiHasElementsFail() {
    $id = 'package_id_1';

    $data = [
      'package' => $id,
    ];
    $response = $this->deleteJson(route('api.package.delete', $data));
    $response->assertStatus(Response::HTTP_CONFLICT);
    $response->assertJson(['status' => 'fail']);
  }
}
