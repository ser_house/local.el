<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\Package;

use Core\Elements\Package\IPackageGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class ManagePackagesTest extends TestCaseDb {

  private IPackageGateway $packageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->packageGateway = $this->app->make(IPackageGateway::class);
  }


  public function testPackagesWithElementsCount() {
    $items = $this->packageGateway->getPackagesWithElementsCount();
    self::assertCount(3, $items);

    $response = $this->get(route('packages.manage'));
    $response->assertStatus(Response::HTTP_OK);
  }
}
