<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\Package;

use Core\Elements\Package\IPackageGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class AddPackageTest extends TestCaseDb {

  private IPackageGateway $packageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->packageGateway = $this->app->make(IPackageGateway::class);
  }


  public function testHttpApi() {
    $title = 'Package';

    $data = [
      'title' => $title,
    ];
    $response = $this->postJson(route('api.package.add'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
    $response->assertJson([
      'item' => [
        'title' => $title,
      ],
    ]);

    $package = $this->packageGateway->getByTitle($title);
    self::assertNotNull($package);
  }

  public function testHttpApiDuplicateFail() {
    $title = 'Тестовый корпус 1';

    $package = $this->packageGateway->getByTitle($title);
    self::assertNotNull($package);

    $data = [
      'title' => $title,
    ];
    $response = $this->postJson(route('api.package.add'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'fail']);
  }
}
