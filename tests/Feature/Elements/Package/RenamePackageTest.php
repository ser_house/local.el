<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\Package;

use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\PackageId;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class RenamePackageTest extends TestCaseDb {

  private IPackageGateway $packageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->packageGateway = $this->app->make(IPackageGateway::class);
  }


  public function testHttpApi() {
    $id = 'package_id_1';
    $title = 'Renamed package';

    $data = [
      'id' => $id,
      'title' => $title,
    ];
    $response = $this->putJson(route('api.package.rename'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
    $response->assertJson([
      'item' => [
        'id' => $id,
        'title' => $title,
      ],
    ]);

    $package = $this->packageGateway->getById(new PackageId($id));
    self::assertEquals($title, $package->getTitle());
  }
}
