<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 22.03.2021
 * Time: 6:06
 */

namespace Tests\Feature\Elements;

use Core\Elements\Package\IPackageGateway;
use Core\Elements\Type\ITypeGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;


class GenerateElementsTest extends TestCaseDb {

  private ITypeGateway $typeGateway;
  private IPackageGateway $packageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->typeGateway = $this->app->make(ITypeGateway::class);
    $this->packageGateway = $this->app->make(IPackageGateway::class);
  }

  public function resistanceDataProvider() {
    return [
      ['R', 1],
      ['R', 10],
      ['R', 100],
      ['K', 1],
      ['K', 10],
      ['K', 100],
      ['M', 1],
      ['M', 10],
      ['M', 100],
    ];
  }

  /**
   * @dataProvider resistanceDataProvider
   */
  public function testGenerateE24Resistors($unit, $multiplier) {
    $type = $this->typeGateway->getByTitle('Тип для E24');
    $package = $this->packageGateway->getByTitle('Корпус для E24');

    $data = [
      'series' => 'E24',
      'unit' => $unit,
      'multiplier' => $multiplier,
      'qty' => 25,
    ];
    $response = $this->postJson(route('api.elements.generate_by_series', ['package' => $package->getId(), 'type' => $type->id()]), $data);

    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
  }

  public function capacityDataProvider() {
    return [
      ['pF', 1],
      ['pF', 10],
      ['pF', 100],
      ['nF', 1],
      ['nF', 10],
      ['nF', 100],
      ['uF', 1],
      ['uF', 10],
      ['uF', 100],
    ];
  }

  /**
   * @dataProvider capacityDataProvider
   */
  public function testGenerateE24Capacitors($unit, $multiplier) {
    $type = $this->typeGateway->getByTitle('Тип для E24');
    $package = $this->packageGateway->getByTitle('Корпус для E24');

    $data = [
      'series' => 'E24',
      'unit' => $unit,
      'multiplier' => $multiplier,
      'qty' => 25,
    ];
    $response = $this->postJson(route('api.elements.generate_by_series', ['package' => $package->getId(), 'type' => $type->id()]), $data);

    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
  }
}
