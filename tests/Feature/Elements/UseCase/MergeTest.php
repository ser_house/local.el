<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\UseCase;

use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Storage\IStorageGateway;
use Core\Elements\Type\ITypeGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class MergeTest extends TestCaseDb {

  private IElementGateway $elementGateway;
  private IStorageGateway $storageGateway;
  private ITypeGateway $typeGateway;
  private IPackageGateway $packageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->elementGateway = $this->app->make(IElementGateway::class);
    $this->storageGateway = $this->app->make(IStorageGateway::class);
    $this->typeGateway = $this->app->make(ITypeGateway::class);
    $this->packageGateway = $this->app->make(IPackageGateway::class);
  }


  public function testHttpApi() {

    $type = $this->typeGateway->getByTitle('Тестовый тип');

    $element1 = $this->elementGateway->getByTitleAndType('Тестовый компонент 1', $type->id());
    $package1 = $this->packageGateway->getByTitle('Тестовый корпус 1');

    $qty1 = $this->storageGateway->getQty($element1->getId(), $package1->getId());
    self::assertEquals(5, $qty1->asInt());

    $element2 = $this->elementGateway->getByTitleAndType('Тестовый компонент 2', $type->id());
    $package2 = $this->packageGateway->getByTitle('Тестовый корпус 2');

    $qty2 = $this->storageGateway->getQty($element2->getId(), $package2->getId());
    self::assertEquals(6, $qty2->asInt());

    $data = [
      'src_element_id' => (string)$element2->getId(),
      'src_package_id' => (string)$package2->getId(),
      'target_element_id' => (string)$element1->getId(),
      'target_package_id' => (string)$package1->getId(),
    ];
    $response = $this->postJson(route('api.element.merge'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
    $response->assertJson([
      'qty' => 11,
    ]);

    $qty1 = $this->storageGateway->getQty($element1->getId(), $package1->getId());
    self::assertEquals(11, $qty1->asInt());
    $qty2 = $this->storageGateway->getQty($element2->getId(), $package2->getId());
    self::assertNull($qty2);
  }
}
