<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\UseCase;

use Core\Elements\ElementId;
use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\PackageId;
use Core\Elements\Storage\IStorageGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class DecreaseQtyTest extends TestCaseDb {

  private IElementGateway $elementGateway;
  private IPackageGateway $packageGateway;
  private IStorageGateway $storageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->elementGateway = $this->app->make(IElementGateway::class);
    $this->packageGateway = $this->app->make(IPackageGateway::class);
    $this->storageGateway = $this->app->make(IStorageGateway::class);
  }


  public function testHttpApi() {
    $element_id = 'element_id_1';
    $package_id = 'package_id_1';
    $qty = 2;

    $elementId = new ElementId($element_id);
    $element = $this->elementGateway->getById($elementId);
    self::assertNotNull($element);

    $packageId = new PackageId($package_id);
    $package = $this->packageGateway->getById($packageId);
    self::assertNotNull($package);

    $data = [
      'element_id' => $element_id,
      'package_id' => $package_id,
      'qty' => $qty,
    ];
    $response = $this->postJson(route('api.element.decrease_qty'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
    $response->assertJson([
      'qty' => 5 - $qty,
    ]);

    // Теперь есть в таком-то корпусе столько-то.
    $packageQty = $this->storageGateway->getQty($elementId, $packageId);
    self::assertNotNull($packageQty);
    self::assertEquals(5 - $qty, $packageQty->asInt());
  }
}
