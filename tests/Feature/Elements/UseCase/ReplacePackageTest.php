<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\UseCase;

use Core\Elements\ElementId;
use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\PackageId;
use Core\Elements\Storage\IStorageGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class ReplacePackageTest extends TestCaseDb {

  private IElementGateway $elementGateway;
  private IPackageGateway $packageGateway;
  private IStorageGateway $storageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->elementGateway = $this->app->make(IElementGateway::class);
    $this->packageGateway = $this->app->make(IPackageGateway::class);
    $this->storageGateway = $this->app->make(IStorageGateway::class);
  }


  public function testHttpApi() {
    $element_id = 'element_id_1';
    $current_package_id = 'package_id_1';
    $new_package_id = 'package_id_2';

    $elementId = new ElementId($element_id);
    $currentPackageId = new PackageId($current_package_id);
    $newPackageId = new PackageId($new_package_id);
    $newPackage = $this->packageGateway->getById($newPackageId);


    // Проверяем, что компонент в указанном как текущий корпусе есть,
    // чтобы показать, что после замены уже не будет.
    $currentPackageQty = $this->storageGateway->getQty($elementId, $currentPackageId);
    self::assertNotNull($currentPackageQty);

    $data = [
      'element_id' => $element_id,
      'current_package_id' => $current_package_id,
      'new_package_id' => $new_package_id,
    ];
    $response = $this->putJson(route('api.element.replace_package'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
    $response->assertJson([
      'item' => ['id' => (string)$newPackageId, 'title' => $newPackage->getTitle()],
    ]);

    // В корпусе, указанном как текущий, больше нет.
    $currentPackageQty = $this->storageGateway->getQty($elementId, $currentPackageId);
    self::assertNull($currentPackageQty);

    // А в новом корпусе теперь - есть.
    $newPackageQty = $this->storageGateway->getQty($elementId, $newPackageId);
    self::assertNotNull($newPackageQty);
  }
}
