<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\UseCase;

use Core\Elements\ElementId;
use Core\Elements\IElementGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class MoveToTypeTest extends TestCaseDb {

  private IElementGateway $elementGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->elementGateway = $this->app->make(IElementGateway::class);
  }


  public function testHttpApi() {
    $element_id = 'element_id_1';
    $target_type_id = 'type_id_2';

    $elementId = new ElementId($element_id);
    $element = $this->elementGateway->getById($elementId);
    // Убеждаемся, что компонент ещё не относится к типу,
    // в который мы его будем перемещать.
    self::assertNotEquals($target_type_id, (string)$element->getTypeId());

    $data = [
      'id' => $element_id,
      'type_id' => $target_type_id,
    ];
    $response = $this->putJson(route('api.element.move_to_type'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);

    $element = $this->elementGateway->getById($elementId);
    self::assertEquals($target_type_id, (string)$element->getTypeId());
  }
}
