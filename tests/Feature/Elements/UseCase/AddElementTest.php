<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\UseCase;

use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Storage\IStorageGateway;
use Core\Elements\Type\ITypeGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class AddElementTest extends TestCaseDb {

  private IElementGateway $elementGateway;
  private IStorageGateway $storageGateway;
  private ITypeGateway $typeGateway;
  private IPackageGateway $packageGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->elementGateway = $this->app->make(IElementGateway::class);
    $this->storageGateway = $this->app->make(IStorageGateway::class);
    $this->typeGateway = $this->app->make(ITypeGateway::class);
    $this->packageGateway = $this->app->make(IPackageGateway::class);
  }


  public function testHttpApi() {
    $title = 'Компонент';
    $qty = 5;

    $type = $this->typeGateway->getByTitle('Тестовый тип');
    $typeId = $type->id();

    $package = $this->packageGateway->getByTitle('Тестовый корпус 1');
    $packageId = $package->getId();

    $data = [
      'title' => $title,
      'type_id' => (string)$typeId,
      'package_id' => (string)$packageId,
      'qty' => $qty,
    ];
    $response = $this->postJson(route('api.element.add'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
    $response->assertJson([
      'data' => [
        'element' => ['title' => $title],
        'type' => ['id' => (string)$typeId, 'title' => $type->title()],
        'package' => ['id' => (string)$packageId, 'title' => $package->getTitle()],
        'qty' => $qty,
      ],
    ]);

    $element = $this->elementGateway->getByTitleAndType('Компонент', $typeId);
    self::assertNotNull($element);

    $resultQty = $this->storageGateway->getQty($element->getId(), $packageId);
    self::assertEquals($qty, $resultQty->asInt());
  }
}
