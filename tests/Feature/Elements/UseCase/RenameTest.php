<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 12:00
 */

namespace Tests\Feature\Elements\UseCase;

use Core\Elements\ElementId;
use Core\Elements\IElementGateway;
use Illuminate\Http\Response;
use Tests\TestCaseDb;

class RenameTest extends TestCaseDb {

  private IElementGateway $elementGateway;

  protected function setUp(): void {
    parent::setUp();

    $this->elementGateway = $this->app->make(IElementGateway::class);
  }


  public function testHttpApi() {
    $element_id = 'element_id_1';
    $title = 'Новое название';

    $data = [
      'id' => $element_id,
      'title' => $title,
    ];
    $response = $this->putJson(route('api.element.rename'), $data);
    $response->assertStatus(Response::HTTP_OK);
    $response->assertJson(['status' => 'success']);
    $response->assertJson([
      'item' => ['id' => $element_id, 'title' => $title],
    ]);

    $element = $this->elementGateway->getById(new ElementId($element_id));
    self::assertEquals($title, $element->getTitle());
  }
}
