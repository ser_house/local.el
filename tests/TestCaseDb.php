<?php

namespace Tests;


use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;


class TestCaseDb extends TestCase {
	use CreatesApplication;
	use DatabaseMigrations;
	use RefreshDatabase;

	protected function setUp(): void {
		parent::setUp();

		$this->artisan('db:seed');
	}

	// Для случая, когда мы хотим посмотреть тестовую базу после тестов.
//	use RefreshDatabase; не использовать
//	/**
//	 * Define hooks to migrate the database before and after each test.
//	 *
//	 * @return void
//	 */
//	public function runDatabaseMigrations() {
//
//		$this->artisan('migrate:fresh');
//	}
}
