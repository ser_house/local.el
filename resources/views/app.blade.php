<?php
$active = $active ?? '';
?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('page_title')</title>

  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  @stack('styles')
</head>
<body>
<section id="app" class="container">
  @component('main-menu', ['active' => $active])@endcomponent
  @if(!empty($sidebar))
    <section class="content with-sidebar">
      @component('breadcrumbs')@endcomponent
      @component('flash')@endcomponent
      @yield('content')
    </section>
    <aside id="sidebar" class="sidebar">
      @yield('sidebar')
    </aside>
  @else
    <section class="content @yield('content_class', '')">
      @component('breadcrumbs')@endcomponent
      @component('flash')@endcomponent
      @yield('content')
    </section>
  @endif
</section>
@routes
<script src="{{ asset('js/app.js') }}"></script>
@stack('scripts')
<script>
  const app = new Vue({
    el: "#app",
  });
</script>
</body>
</html>
