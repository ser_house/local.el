<nav class="main-menu-bar">
  <a class="home-link @if('elements' === $active)active @endif" href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
  <a href="{{ route('notes') }}" @if('notes' === $active)class="active" @endif>Заметки</a>
  <a href="{{ route('packages.manage') }}" @if('packages.manage' === $active)class="active" @endif>Корпуса</a>
  <a href="{{ route('tools.midi_rttl_page') }}" @if('tools.midi_rttl_page' === $active || 'tools.midi_rttl_convert' === $active)class="active" @endif>Midi to Rttl</a>
  <a href="{{ route('tools.kicad_bom_page') }}" @if('tools.kicad_bom_page' === $active || 'tools.kicad_bom_generate' === $active)class="active" @endif>KiCAD BOM</a>
  <a href="{{ route('tools.calculators') }}" @if('tools.calculators' === $active)class="active" @endif>Калькуляторы</a>
</nav>

