@if($breadcrumbs = Request::get('breadcrumbs'))
  <ul class="breadcrumbs">
    @foreach($breadcrumbs as $breadcrumb)
      <li class="segment">
        @if($breadcrumb['href'])
          <a href="{{ $breadcrumb['href'] }}">{!! $breadcrumb['title'] !!}</a>
        @else
          {!! $breadcrumb['title'] !!}
        @endif
      </li>
      @if (!$loop->last)
        <li class="delimiter">/</li>
      @endif
    @endforeach
  </ul>
@endif
