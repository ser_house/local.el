<?php
/**
 * @var $items array
 */
?>
@extends('app')
@section('page_title', 'Управление корпусами')

@push('scripts')
  <script src="{{ asset('js/pages/manage_packages.js') }}"></script>
@endpush

@push('styles')
  <link href="{{ asset('css/manage_packages.css') }}" rel="stylesheet">
@endpush

@section('content')
  <manage-packages :init-items='@json($items)'></manage-packages>
@endsection
