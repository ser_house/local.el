@extends('app')
@section('page_title', 'Заметки')

@section('content_class', 'notes')
@section('content')
  <h1>Заметки <a href="{{ route('notes.new') }}" class="action">Добавить заметку</a></h1>
  @if($notes)
    @foreach($notes as $note)
      <section class="note note-short">
        <div class="note-info">
          <div class="note-header">
            <a class="note-title" href="{!! route("notes.view", ['note_id' => $note['id']]) !!}">{!! $note['title'] !!}</a>
            <div class="note-actions">
              <a class="note-edit-link text-with-icon" href="{!! route("notes.edit", ['note_id' => $note['id']]) !!}"><span class="icon icon-edit"></span>изменить</a>
              <form class="phantom-form" action="{{ route('notes.delete', ['note_id' => $note['id']]) }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="btn btn-link note-delete-link text-with-icon"><span class="icon icon-delete"></span>удалить</button>
              </form>
            </div>
          </div>
          <div class="note-added-at">{{ $note['added_at'] }}</div>
        </div>
        @if($note['files'])
          <ul class="note-files">
            @foreach($note['files'] as $file)
              <li class="file-item">
                <div class="file-preview"><a href="{{ $file->url }}" title="{{ $file->name }}"><img src="{{$file->thumb_url}}"/></a></div>
                <div class="file-data">
                  <div class="file-info">
                    {{ $file->human_name }} ({{ $file->size }})
                  </div>
                </div>
              </li>
            @endforeach
          </ul>
        @endif
      </section>
    @endforeach
  @else
    Нет заметок.
  @endif
@endsection
