<?php
/**
 * @var $note array
 */
?>
@extends('app')
@section('page_title', $note['title'])

@section('content')
  <h1>{{ html_entity_decode($note['title']) }}</h1>
  <section class="note note-full">
    <header>
      <div class="note-subtitle">
        <div class="note-added-at">{!! $note['added_at'] !!}</div>
        <div class="note-actions">
          <a class="note-edit-link text-with-icon" href="{!! route("notes.edit", ['note_id' => $note['id']]) !!}"><span class="icon icon-edit"></span>изменить</a>
          <form class="phantom-form" action="{{ route('notes.delete', ['note_id' => $note['id']]) }}" method="POST">
            @method('DELETE')
            @csrf
            <button class="btn btn-link note-delete-link text-with-icon"><span class="icon icon-delete"></span>удалить</button>
          </form>
        </div>
      </div>
    </header>
    <main>
      <div class="note-text">{!! html_entity_decode($note['text']) !!}</div>
      @if($note['files'])
        <ul class="file-listing">
          @foreach($note['files'] as $file)
            <li class="file-item">
              <div class="file-preview"><a href="{{ $file->url }}" title="{{ $file->name }}"><img src="{{$file->thumb_url}}"/></a></div>
              <div class="file-data">
                <div class="file-info">
                  {{ $file->human_name }} ({{ $file->size }})
                </div>
              </div>
            </li>
          @endforeach
        </ul>
      @endif
    </main>
  </section>
@endsection
