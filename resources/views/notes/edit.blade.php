<?php
/**
 * @var $formData \Core\Infrastructure\Note\NoteFormData
 */
?>
@extends('app')
@section('page_title', '* ' . $formData->getTitle())

@push('scripts')
  <script src="{{ asset('js/pages/note_form.js') }}"></script>
@endpush

@section('content_class', 'note-edit')
@section('content')
  <h1>Изменение заметки<a class="action" href="{!! route("notes.view", ['note_id' => $formData->getId()]) !!}">на страницу заметки</a></h1>
  <note-form
    init-note-id="{!! $formData->getId() !!}"
    :init-title='@json(old('title', $formData->getTitle()))'
    :init-text='@json(old('text', $formData->getText()))'
    :note-file-rules='@json($formData->getFileRules())'
    :init-note-files='@json($formData->getFiles())'
    pdf-thumbnail="{!! $formData->getPdfThumbUrl() !!}"
  ></note-form>
@endsection
