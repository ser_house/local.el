<?php
/**
 * @var $formData \Core\Infrastructure\Note\NoteFormData
 */
?>
@extends('app')
@section('page_title', 'Новая заметка')

@push('scripts')
  <script src="{{ asset('js/pages/note_form.js') }}"></script>
@endpush

@section('content_class', 'note-new')
@section('content')
  <h1>Новая заметка</h1>
  <note-form
    init-note-id=""
    init-title="{!! old('title', $formData->getTitle()) !!}"
    init-text="{!! old('text', $formData->getText()) !!}"
    :note-file-rules='@json($formData->getFileRules())'
    :init-note-files='@json($formData->getFiles())'
    pdf-thumbnail="{!! $formData->getPdfThumbUrl() !!}"
  ></note-form>
@endsection
