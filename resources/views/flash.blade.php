@foreach(['success', 'fail', 'warning'] as $type)
  @php($css_class = ('fail' === $type) ? 'danger' : $type)
  @if (session($type))
    <div class="alert alert-{!! $css_class !!}"><p>{!! session($type) !!}</p></div>
  @endif
@endforeach
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

