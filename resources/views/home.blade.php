<?php
/**
 * @var $packages array
 * @var $types_tree array
 * @var $units array
 */
?>
@extends('app')
@section('page_title', 'Компоненты')

@push('scripts')
  <script src="{{ asset('js/pages/home.js') }}"></script>
@endpush
@section('content_class', 'elements-wrapper')
@section('content')
  <types :init-types='@json($types_tree)' :units='@json($units)'></types>
  <elements :packages='@json($packages)' :units='@json($units)'></elements>
@endsection
