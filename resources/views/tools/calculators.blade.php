<?php
/**
 * @var $rttl string
 * @var $data string
 */
?>
@extends('app')
@section('page_title', 'Калькуляторы')

@push('scripts')
  <script src="{{ asset('js/pages/calculators.js') }}"></script>
@endpush
@section('content_class', 'calculators')
@section('content')
  <h1>Калькуляторы</h1>
  <h2>Конденсаторы</h2>
  <div class="calc-cap flexed">
    <calc-cap-marking-by-value></calc-cap-marking-by-value>
    <calc-cap-value-by-marking></calc-cap-value-by-marking>
  </div>
  <h2>Резисторы</h2>
  <div class="calc-res flexed">
    <calc-res-marking-by-value></calc-res-marking-by-value>
    <calc-res-value-by-marking></calc-res-value-by-marking>
  </div>
@endsection
