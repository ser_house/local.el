<?php
/**
 * @var $rttl string
 * @var $data string
 */
?>
@extends('app')
@section('page_title', 'Midi to Rttl')

@push('styles')
  <link href="{{ asset('css/midi_to_rttl.css') }}" rel="stylesheet">
@endpush

@section('content')
  <form method="post" enctype="multipart/form-data" action="{{route('tools.midi_rttl_convert')}}" class="midi-to-rttl-form">
    @csrf
    <div class="form-group">
      <label for="midi">Midi file</label>
      <input id="midi" name="midi" type="file" accept=".midi"/>
    </div>

    <div class="actions">
      <button type="submit" class="btn btn-primary">Конвертировать</button>
    </div>
    @if($rttl)
      <div class="form-group">
        <label for="rttl">Rttl</label>
        <textarea id="rttl" name="rttl">{!! $rttl !!}</textarea>
      </div>
      @endif
    @if($data)
      <div class="form-group">
        <label for="data">Data for door 655</label>
        <textarea id="data" name="data" rows="5">{!! $data !!}</textarea>
      </div>
    @endif
  </form>
@endsection
