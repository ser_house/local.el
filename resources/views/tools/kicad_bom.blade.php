<?php
/**
 * @var $components array
 */
?>
@extends('app')
@section('page_title', 'KiCAD net to BOM')

@section('content')
  <form method="post" enctype="multipart/form-data" action="{{route('tools.kicad_bom_generate')}}" class="kicad-bom-form">
    @csrf
    <div class="form-group">
      <label for="kicad-file">KiCAD file</label>
      <input id="kicad-file" name="kicad_file" type="file" accept=".net,.sch"/>
      <span class="help">Расширения .net и .sch</span>
    </div>

    <div class="actions">
      <button type="submit" class="btn btn-primary">Генерировать</button>
    </div>

    @if($components)
      <div class="form-group">
        <label>Result</label>
        <table>
          <thead>
          <tr>
            <th>Ref</th>
            <th>Value</th>
            <th>Footprint</th>
            <th>Quantity</th>
          </tr>
          </thead>
          <tbody>
          @foreach($components as $component)
            <tr>
              <td>{!! $component['ref'] !!}</td>
              <td>{!! $component['value'] !!}</td>
              <td>{!! $component['footprint'] !!}</td>
              <td>{!! $component['qty'] !!}</td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    @endif
  </form>
@endsection
