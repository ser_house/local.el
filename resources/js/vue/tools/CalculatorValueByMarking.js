export class CalculatorValueByMarking {

  constructor(units) {
    this.units = units;
  }

  get(marking) {
    let values = [];
    switch (marking.length) {

      case 2:
        values.push(`${marking}${this.units[0]}`);
        break;

      // Если в маркировке присутствуют три цифры,
      // то первые две обозначают величину емкости, последняя – множитель нуля.
      // Если последняя цифра находится в диапазоне 0-6,
      // то к числу, состоящему из первых двух цифр, добавляют нули в указанном количестве.
      // Если последняя цифра – 8, то число из первых двух цифр умножают на 0,01, если 9, то – на 0,1.
      case 3: {
        let value = parseInt(marking[0] + marking[1], 10);
        if (this._processFractional(value, marking[2], values)) {
          break;
        }
        value = this._getMultiplied(value, marking[2]);

        this._fillValues(value, values);
        break;
      }
      // Значащая часть содержит три цифры, а четвертая – это показатель степени для 10.
      // Единица измерения – обычно пикофарады.
      case 4: {
        let value = parseInt(marking[0] + marking[1] + marking[2], 10);
        if (this._processFractional(value, marking[3], values)) {
          break;
        }

        value = this._getMultiplied(value, marking[3]);
        this._fillValues(value, values);
        break;
      }

      default:
        break;
    }

    return values;
  };

  _processFractional(value, mulDigit, values) {
    switch(mulDigit) {
      case '8':
        value *= 0.01;
        values.push(`${value}${this.units[0]}`);
        return true;

      case '9':
        value = (value * 0.1).toFixed(1);
        values.push(`${value}${this.units[0]}`);
        return true;

      default:
        return false;
    }
  };
  _getMultiplied(value, mulDigit) {
    let mul = "1";
    for (let i = 0; i < mulDigit; i++) {
      mul += "0";
    }
    return value * parseInt(mul, 10);
  };
  _fillValues(value, values) {
    if (value > 1000000) {
      let u2 = value / 1000000;
      values.push(`${u2}${this.units[2]}`);
    }
    else if (value > 1000) {
      if (value > 100000) {
        let u2 = value / 1000000;
        values.push(`${u2}${this.units[2]}`);
      }

      let u1 = value / 1000;
      values.push(`${u1}${this.units[1]}`);

      if (value < 10000) {
        values.push(`${value}${this.units[0]}`);
      }
    }
    else if (value > 100) {
      let u1 = value / 1000;
      values.push(`${u1}${this.units[1]}`);
      values.push(`${value}${this.units[0]}`);
    }
    else {
      values.push(`${value}${this.units[0]}`);
    }
  };
}
