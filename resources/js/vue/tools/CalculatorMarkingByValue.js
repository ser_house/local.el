export class CalculatorMarkingByValue {
  constructor(units) {
    // [
    //   0 => базовая единица (ом, пикофарада),
    //   1 => средняя единица (КОм, нФ),
    //   2 => старшая единица (МОм, мкФ),
    // ]
    this.units = units;
  }

  get(value, unit) {
    // Если в маркировке присутствуют три цифры,
    // то первые две обозначают величину емкости, последняя – множитель нуля.
    // Если последняя цифра находится в диапазоне 0-6,
    // то к числу, состоящему из первых двух цифр, добавляют нули в указанном количестве.
    // Если последняя цифра – 8, то число из первых двух цифр умножают на 0,01, если 9, то – на 0,1.
    value = value.replace(",", ".");
    if (-1 !== value.indexOf(".")) {
      return this._calcFloat(value, unit);
    }
    else {
      return this._calcInt(value, unit);
    }
  };

  _calcFloat(value, unit) {
    switch (unit) {
      case this.units[1]:
        value *= 1000;
        return this._calcInt(value, this.units[0]);

      case this.units[2]:
        value *= 1000;
        return this._calcInt(value, this.units[1]);

      default:
        let parts = value.split(".");
        if ("0" === parts[0]) {
          if (1 === parts[1].length) {
            parts[1] += '0';
          }
          return `${parts[1]}8`;
        }
        else {
          value *= 10;
          return `${value}9`;
        }
    }
  };

  _calcInt(value, unit) {
    let parsed = this._parseValue(value.toString());

    switch (unit) {
      case this.units[1]:
        parsed.numEndZeros += 3;
        break;

      case this.units[2]:
        parsed.numEndZeros += 6;
        break;

      default:
        break;
    }

    let numZeros = "";
    if (parsed.numEndZeros) {
      numZeros = parsed.numEndZeros;
    }
    return `${parsed.withoutEndZeros}${numZeros}`;
  };

  _parseValue(value) {
    let result = {
      numEndZeros: 0,
      withoutEndZeros: value[0] + value[1],
    };

    for (let i = value.length - 1; i >= 2; i--) {
      if (value[i] === "0") {
        result.numEndZeros++;
      }
      else {
        break;
      }
    }

    return result;
  };
}
