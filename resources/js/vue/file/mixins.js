import {Formatter, MsgToUser} from "../mixins";
import axios from 'axios';

export let dragDrop = {
  mixins: [
    Formatter,
    MsgToUser
  ],
  data() {
    return {
      toUpload: [],
      uploadedItems: [],
      dragAndDropCapable: false,
      isUploading: false,
      uploadPercentage: 0,
      errors: new Map(),
    };
  },
  props: {
    client: Object,
    rules: Object,
  },
  methods: {
    determineDragAndDropCapable() {
      var div = document.createElement('div');
      return (('draggable' in div)
        || ('ondragstart' in div && 'ondrop' in div))
        && 'FormData' in window
        && 'FileReader' in window;
    },
    initDragDrop() {
      this.dragAndDropCapable = this.determineDragAndDropCapable();
      if (this.dragAndDropCapable) {
        ['drag', 'dragstart', 'dragend', 'dragover', 'dragenter', 'dragleave', 'drop'].forEach((evt) => {
          this.$refs.drop_zone.$el.addEventListener(evt, (e) => {
            e.preventDefault();
            e.stopPropagation();
          });
        });

        this.$refs.drop_zone.$el.addEventListener('drop', (e) => {
          this._addFiles(e.dataTransfer.files);
        });
      }
    },
    onDropZoneClicked() {
      this.$refs.input.click();
    },
    handleFilesUpload() {
      this._addFiles(this.$refs.input.files);
    },
    _addFiles(files) {
      this.errors = new Map;
      for (let i = 0; i < files.length; i++) {
        let file = files[i];

        if (!this.validateFileType(file)) {
          let fileErrors = {};
          if (this.errors.has('files')) {
            fileErrors = this.errors.get('files');
          }

          if (!file.type.length) {
            fileErrors[i] = `Неизвестный тип файла ${file.name}.`;
          }
          else {
            fileErrors[i] = `Недопустимый тип файла ${file.name}.`;
          }

          this.errors.set('files', fileErrors);

          this.errors = new Map(this.errors);
        }
        else if (!this.validateFileSize(file)) {
          let fileErrors = {};
          if (this.errors.has('files')) {
            fileErrors = this.errors.get('files');
          }

          let file_size = this.formatFileSize(file.size);
          let max_size = this.formatFileSize(this.rules.max_filesize);
          fileErrors[i] = `Размер файла ${file.name} (<strong>${file_size}</strong>) больше допустимого (<strong>${max_size}</strong>).`;
          this.errors.set('files', fileErrors);

          this.errors = new Map(this.errors);
        }

        if (!this.errors.size) {
          this._addFile(file);
        }
      }
    },
    removeToUploadFile(index) {
      this.toUpload.splice(index, 1);
      this.$emit('file-removed', index);
    },
    removeUploadedItem(index) {
      let url = this.uploadedItems[index].delete_url;
      axios.delete(url).then((response) => {
        this.processResponse(response);
        if ('success' === response.data.status) {
          this.uploadedItems.splice(index, 1);
          this.$emit('file-deleted', index);
        }
      });
    },
    validateFileSize(file) {
      return file.size <= this.rules.max_filesize;
    },
    validateFileType(file) {
      return 'undefined' !== typeof this.rules.types[file.type];
    },
    upload(url, formData) {
      this.isUploading = true;
      axios.post(url, formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          onUploadProgress: (progressEvent) => {
            this.uploadPercentage = parseInt(Math.round((progressEvent.loaded * 100) / progressEvent.total));
          }
        }
      ).then((response) => {
        this.processResponse(response);
      });
    },
  },
  mounted() {
    this.initDragDrop();
  },
};
