export let form = {
  methods: {
    inputName(fieldName, index, itemName) {
      return `${fieldName}[${index}][${itemName}]`;
    }
  }
};

export let autocomplete = {
  methods: {
    onArrowDown() {
      if (this.selectedIndex < this.results.length) {
        this.selectedIndex = this.selectedIndex + 1;
      }
    },
    onArrowUp() {
      if (this.selectedIndex > 0) {
        this.selectedIndex = this.selectedIndex - 1;
      }
    },
    onEsc() {
      this.clearResult();
      this.$emit('esc');
    },
    handleClickOutside(event) {
      if (!this.$el.contains(event.target)) {
        this.clearResult();
        this.selectedIndex = -1;
      }
    },
    resetFindTimeout(timer) {
      clearTimeout(timer);
    },
    clearResult() {
      this.results = [];
      this.isOpen = false;
    }
  },
  mounted() {
    document.addEventListener("click", this.handleClickOutside);
  },
  destroyed() {
    document.removeEventListener("click", this.handleClickOutside);
  }
};

let isJson = (str) => {
  try {
    JSON.parse(str);
  }
  catch (e) {
    return false;
  }
  return true;
};

export let MsgToUser = {
  methods: {
    showErrors: function (errors) {
      for (let error of errors) {
        this.$toasted.error(error);
      }
    },
    showError: function (error) {
      this.$toasted.error(error);
    },
    showInfo: function (text) {
      this.$toasted.info(text, {
        duration: 10000,
        fullWidth: false,
        action: {
          text: "закрыть",
          onClick: (e, toastObject) => {
            toastObject.goAway(0);
          }
        },
      });
    },
    processResponse: function (response) {
      if ("undefined" === typeof response.data.status) {
        this.showErrors([response.data.msg]);
      }

      switch (response.data.status) {
        case "success":
          this.processSuccessInResponse(response);
          break;

        case "error":
        case 'fail':
          this.processErrorsInResponse(response);
          break;

        case "warning":
          this.processWarningInResponse(response);
          break;

        default:
          this.showErrors([response.data.msg]);
          break;
      }
    },
    processErrorsInResponse: function (response) {
      this.$toasted.error(response.data.msg);
      if ("undefined" !== typeof response.data.errors) {
        this.showErrors(response.data.errors);
      }
    },
    processWarningInResponse: function (response) {
      this.$toasted.info(response.data.msg);
      if ("undefined" !== typeof response.data.warning) {
        this.$toasted.info(response.data.warning);
      }
    },
    processSuccessInResponse: function (response) {
      this.$toasted.success(response.data.msg);
      if ("undefined" !== typeof response.data.warning) {
        this.$toasted.info(response.data.warning);
      }
    },
    processCatchResponse(response) {
      this.$toasted.error(response);
    },
    processCatchError(error) {
      if (isJson(error.message)) {
        this.processResponse(JSON.parse(error.message));
      }
      else {
        this.showError(error.toString());
      }
    },
    processCatch(response) {
      if (isJson(response)) {
        this.processResponse(JSON.parse(response));
      }
      else if (typeof response === 'string' || response instanceof String || response instanceof Error) {
        this.showError(response.toString());
      }
      else {
        this.showError(response.data.msg.toString());
      }
    }
  }
};

export let RouteAdapter = {
  methods: {
    route: function (route_name) {
      return route(route_name);
    }
  }
};

export let Formatter = {
  methods: {
    formatAmount(amount, currency) {
      if ('string' === typeof currency) {
        currency = CURRENCIES[currency];
      }
      amount = parseFloat(amount).toFixed(2);
      let parts = amount.toString().split('.');
      let int = parts[0];
      let groups = int.toString().match(/(\d+?)(?=(\d{3})+(?!\d)|$)/g);
      let joined = groups ? groups.join(currency.thousandsSeparator) : int;

      if (parts.length > 1) {
        joined = joined + currency.decimalSeparator + parts[1];
      }

      if (currency.prefix) {
        joined = currency.prefix + joined;
      }
      if (currency.suffix) {
        joined += currency.suffix;
      }
      return joined;
    },
    formatNumber(number, decimals, dec_point, thousands_sep) {
      //  discuss at: http://phpjs.org/functions/number_format/
      // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
      // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      // improved by: davook
      // improved by: Brett Zamir (http://brett-zamir.me)
      // improved by: Brett Zamir (http://brett-zamir.me)
      // improved by: Theriault
      // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      // bugfixed by: Michael White (http://getsprink.com)
      // bugfixed by: Benjamin Lupton
      // bugfixed by: Allan Jensen (http://www.winternet.no)
      // bugfixed by: Howard Yeend
      // bugfixed by: Diogo Resende
      // bugfixed by: Rival
      // bugfixed by: Brett Zamir (http://brett-zamir.me)
      //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
      //  revised by: Luke Smith (http://lucassmith.name)
      //    input by: Kheang Hok Chin (http://www.distantia.ca/)
      //    input by: Jay Klehr
      //    input by: Amir Habibi (http://www.residence-mixte.com/)
      //    input by: Amirouche
      //   example 1: number_format(1234.56);
      //   returns 1: '1,235'
      //   example 2: number_format(1234.56, 2, ',', ' ');
      //   returns 2: '1 234,56'
      //   example 3: number_format(1234.5678, 2, '.', '');
      //   returns 3: '1234.57'
      //   example 4: number_format(67, 2, ',', '.');
      //   returns 4: '67,00'
      //   example 5: number_format(1000);
      //   returns 5: '1,000'
      //   example 6: number_format(67.311, 2);
      //   returns 6: '67.31'
      //   example 7: number_format(1000.55, 1);
      //   returns 7: '1,000.6'
      //   example 8: number_format(67000, 5, ',', '.');
      //   returns 8: '67.000,00000'
      //   example 9: number_format(0.9, 0);
      //   returns 9: '1'
      //  example 10: number_format('1.20', 2);
      //  returns 10: '1.20'
      //  example 11: number_format('1.20', 4);
      //  returns 11: '1.2000'
      //  example 12: number_format('1.2000', 3);
      //  returns 12: '1.200'
      //  example 13: number_format('1 000,50', 2, '.', ' ');
      //  returns 13: '100 050.00'
      //  example 14: number_format(1e-8, 8, '.', '');
      //  returns 14: '0.00000001'

      number = (number + '')
        .replace(/[^0-9+\-Ee.]/g, '');
      let n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          let k = Math.pow(10, prec);
          return '' + (Math.round(n * k) / k)
            .toFixed(prec);
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '')
        .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
          .join('0');
      }
      return s.join(dec);
    },
    formatFileSize (bytes) {
      if (typeof bytes !== 'number') {
        return '';
      }
      if (bytes >= 1000000000) {
        return (bytes / 1000000000).toFixed(0) + ' Gb';
      }
      if (bytes >= 1000000) {
        return (bytes / 1000000).toFixed(0) + ' Mb';
      }
      return (bytes / 1000).toFixed(0) + ' Kb';
    },
    /**
     *
     * @param {Date|String} date
     * @returns {string}
     */
    formatDateToHuman(date) {
      if ('string' === typeof date) {
        date = new Date(date);
      }
      const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
      return date.toLocaleDateString(undefined, options).split('-').reverse().join('.');
    },
    /**
     *
     * @param date Date
     * @returns {string}
     */
    formatDateToStorage(date) {
      const options = { year: 'numeric', month: '2-digit', day: '2-digit' };
      return date.toLocaleDateString(undefined, options).split('.').reverse().join('-');
    }
  }
};

export let InlineEdit = {
  computed: {
    placeholder() {
      let maxLen = null;
      if ('undefined' !== typeof this.data && 'undefined' !== typeof this.data.max_title_len) {
        maxLen = this.data.max_title_len;
      }
      else if ('undefined' !== typeof this.maxTitleLen) {
        maxLen = this.maxTitleLen;
      }

      if (null !== maxLen) {
        return 'До ' + maxLen + ' символов.';
      }

      return '';
    },
    hasEditing() {
      for(let i in this.items) {
        if (this.items[i].is_editing) {
          return true;
        }
      }

      return false;
    },
  },
  methods: {
    onEnter(item, index) {
      this.save(item, index);
    },
    onEsc(item) {
      this.errors.clear();

      if (item.is_new) {
        this.items.splice(this.items.length - 1, 1);
      }
      else {
        this.resetItemEditing(item);
      }
    },
    editOn(target_item, index) {
      this.resetEditing();
      target_item.is_editing = true;
      this.setFocus(index);
    },
    remove(item, index) {
      axios.post(this.data.urls.remove, {
        dictionary: this.data.dictionary,
        id: item.id,
      }).then((response) => {
        if ('fail' === response.data.status) {
          this.processErrorsInResponse(response);
        }
        else {
          this.items.splice(index, 1);
          this.processSuccessInResponse(response);
        }
      }).catch((response) => {
        this.processCatch(response);
      });
    },
    resetEditing() {
      for(let i in this.items) {
        this.onEsc(this.items[i]);
        // this.resetItemEditing(this.items[i]);
      }
    },
    setFocus(index) {
      this.$nextTick(() => {
        this.$refs['item_' + index][0].focus();
      });
    },
  },
};
