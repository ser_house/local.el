require('./bootstrap');

import Vue from 'vue';

window.Vue = Vue;

Vue.config.productionTip = false;

// https://github.com/shakee93/vue-toasted
import Toasted from 'vue-toasted';
Vue.use(Toasted, {
  duration: 5000,
  keepOnHover: true
});


function trimEmptyTextNodes (el) {
  for (let node of el.childNodes) {
    if (node.nodeType === Node.TEXT_NODE && node.data.trim() === '') {
      node.remove();
    }
    else {
      trimEmptyTextNodes(node);
    }
  }
}

Vue.directive('trim-whitespace', {
  inserted: trimEmptyTextNodes,
  componentUpdated: trimEmptyTextNodes
});
