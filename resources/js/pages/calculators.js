require('../app');

Vue.component('calc-cap-marking-by-value', require('../vue/tools/CalcCapMarkingByValue').default);
Vue.component('calc-res-marking-by-value', require('../vue/tools/CalcResMarkingByValue').default);
Vue.component('calc-cap-value-by-marking', require('../vue/tools/CalcCapValueByMarking').default);
Vue.component('calc-res-value-by-marking', require('../vue/tools/CalcResValueByMarking').default);

