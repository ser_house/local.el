<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 05.02.2020
 * Time: 12:24
 */


namespace App\Http\Middleware;


class JsonUnicode {
  public function handle($request, \Closure $next) {

    $json = $next($request);

    return $json->setEncodingOptions(JSON_UNESCAPED_UNICODE);
  }
}
