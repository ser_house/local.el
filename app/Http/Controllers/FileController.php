<?php

namespace App\Http\Controllers;

use App\Implement\File\Converter;
use App\Models\NoteFile;
use Core\File\IFileUrlResolver;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;
use Throwable;

class FileController extends Controller {
  private Converter $converter;
  private IFileUrlResolver $fileUrlResolver;

  /**
   * FileController constructor.
   *
   * @param Converter $converter
   * @param IFileUrlResolver $fileUrlResolver
   */
  public function __construct(Converter $converter, IFileUrlResolver $fileUrlResolver) {
    $this->converter = $converter;
    $this->fileUrlResolver = $fileUrlResolver;
  }

  /**
   * @param NoteFile $noteFile
   *
   * @return JsonResponse
   */
  public function delete(NoteFile $noteFile): JsonResponse {
    try {
      $filename = $noteFile->name;
      $filepath = $noteFile->path;
      Storage::delete("$filepath/$filename");

      $thumbnail_filename = $this->fileUrlResolver->getFileThumbFileName($this->converter->modelToEntity($noteFile));
      Storage::delete("$filepath/$thumbnail_filename");

      $noteFile->delete();

      return response()->json(new Success("Файл $filename удален"));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e), Response::HTTP_BAD_REQUEST);
    }
  }

  /**
   * @param NoteFile $noteFile
   * @param Request $request
   *
   * @return JsonResponse
   */
  public function rename(NoteFile $noteFile, Request $request): JsonResponse {
    try {
      $title = $request->get('title');

      $old_title = $noteFile->human_name;

      $noteFile->human_name = $title;
      $noteFile->save();

      return response()->json(new Success("Название файла '$old_title' изменено на '$title'"));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e), Response::HTTP_BAD_REQUEST);
    }
  }
}
