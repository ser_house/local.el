<?php

namespace App\Http\Controllers\Notes;

use App\Http\Controllers\Controller;
use Core\Note\INoteGateway;
use Core\Note\NoteId;
use Core\Note\View\NotePresenter;
use DomainException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ViewNote extends Controller {
  private INoteGateway $noteGateway;
  private NotePresenter $notePresenter;

  /**
   * ViewNote constructor.
   *
   * @param INoteGateway $noteGateway
   * @param NotePresenter $notePresenter
   */
  public function __construct(INoteGateway $noteGateway, NotePresenter $notePresenter) {
    $this->noteGateway = $noteGateway;
    $this->notePresenter = $notePresenter;
  }


  /**
   * @param Request $request
   * @param string $id
   *
   * @return Application|Factory|View
   */
  public function __invoke(Request $request, string $id) {
    $noteId = new NoteId($id);

    $note = $this->noteGateway->getById($noteId);
    if (null === $note) {
      throw new DomainException("Заметка с id '$noteId' не найдена.");
    }

    $view = $this->notePresenter->full($note);

    return view('notes.view', ['note' => $view]);
  }
}
