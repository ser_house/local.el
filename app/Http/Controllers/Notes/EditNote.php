<?php

namespace App\Http\Controllers\Notes;

use App\Http\Controllers\Controller;
use Core\Infrastructure\Note\NoteFormDataBuilder;
use Core\Note\INoteGateway;
use Core\Note\NoteId;
use DomainException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class EditNote extends Controller {
  private INoteGateway $noteGateway;
  private NoteFormDataBuilder $noteFormDataBuilder;

  /**
   * EditNote constructor.
   *
   * @param INoteGateway $noteGateway
   * @param NoteFormDataBuilder $noteFormDataBuilder
   */
  public function __construct(INoteGateway $noteGateway, NoteFormDataBuilder $noteFormDataBuilder) {
    $this->noteGateway = $noteGateway;
    $this->noteFormDataBuilder = $noteFormDataBuilder;
  }


  /**
   * @param string $id
   *
   * @return Application|Factory|View
   */
  public function __invoke(string $id) {
    $note = $this->noteGateway->getById(new NoteId($id));
    if (null === $note) {
      throw new DomainException("Заметка с id '$id' не найдена.");
    }

    return view('notes.edit', ['formData' => $this->noteFormDataBuilder->build($note)]);
  }
}
