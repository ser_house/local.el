<?php

namespace App\Http\Controllers\Notes;

use App\Http\Controllers\Controller;
use App\Implement\File\UploadedFileInfo;
use App\Implement\File\UploadedFileResizer;
use App\Implement\File\UploadedFileSaver;
use App\NoteFile\FileUploader;
use App\NoteFile\FileValidator;
use Core\Infrastructure\Response\Status\Fail;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Core\Note\UseCase\Update\Action;
use Core\Note\UseCase\Update\Input;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class UpdateNote extends Controller {
  private Action $action;
  private FileValidator $fileValidator;
  private FileUploader $fileUploader;

  /**
   * UpdateNote constructor.
   *
   * @param Action $action
   * @param FileValidator $fileValidator
   * @param FileUploader $fileUploader
   */
  public function __construct(Action $action, FileValidator $fileValidator, FileUploader $fileUploader) {
    $this->action = $action;
    $this->fileValidator = $fileValidator;
    $this->fileUploader = $fileUploader;
  }


  /**
   * @param Request $request
   * @param string $id
   *
   * @return JsonResponse
   */
  public function __invoke(Request $request, string $id) {
    $input = $request->all();

    try {
      $updateNoteInput = new Input($id, $input);
      $note = $this->action->run($updateNoteInput);
      $noteId = $note->getId();
      $files = $request->file('files');
      if ($files) {
        $validator = $this->fileValidator->getValidator($input, 'files');
        if ($validator->fails()) {
          return response()->json(new Fail('', ['errors' => $validator->errors()]));
        }
        foreach ($files as $i => $file) {
          $fileInfo = new UploadedFileInfo($file);
          $fileResizer = new UploadedFileResizer($file);
          $fileSaver = new UploadedFileSaver($file);
          $this->fileUploader->upload($fileInfo, $fileResizer, $fileSaver, $input['file_titles'][$i], $noteId);
        }
      }

      $redirect = route('notes.view', ['note_id' => $noteId]);

      return response()->json(new Success('Заметка обновлена.', ['redirect' => $redirect]), Response::HTTP_OK);
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e), Response::HTTP_BAD_REQUEST);
    }
  }
}
