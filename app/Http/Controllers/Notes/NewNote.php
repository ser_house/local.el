<?php

namespace App\Http\Controllers\Notes;

use App\Http\Controllers\Controller;
use Core\Infrastructure\Note\NewNoteFormDataBuilder;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class NewNote extends Controller {
  private NewNoteFormDataBuilder $noteFormDataBuilder;

  /**
   * NewNote constructor.
   *
   * @param NewNoteFormDataBuilder $noteFormDataBuilder
   */
  public function __construct(NewNoteFormDataBuilder $noteFormDataBuilder) {
    $this->noteFormDataBuilder = $noteFormDataBuilder;
  }


  /**
   *
   * @return Application|Factory|View
   */
  public function __invoke() {
    return view('notes.new', ['formData' => $this->noteFormDataBuilder->build()]);
  }
}
