<?php

namespace App\Http\Controllers\Notes;

use App\Http\Controllers\Controller;
use App\Implement\File\UploadedFileInfo;
use App\Implement\File\UploadedFileResizer;
use App\Implement\File\UploadedFileSaver;
use App\NoteFile\FileUploader;
use App\NoteFile\FileValidator;
use Core\Infrastructure\Response\Status\Fail;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Core\Note\UseCase\Add\Action;
use Core\Note\UseCase\Add\Input;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class AddNote extends Controller {
  private Action $action;
  private FileValidator $fileValidator;
  private FileUploader $fileUploader;

  /**
   * AddNote constructor.
   *
   * @param Action $action
   * @param FileValidator $fileValidator
   * @param FileUploader $fileUploader
   */
  public function __construct(Action $action, FileValidator $fileValidator, FileUploader $fileUploader) {
    $this->action = $action;
    $this->fileValidator = $fileValidator;
    $this->fileUploader = $fileUploader;
  }


  /**
   * @param Request $request
   *
   * @return JsonResponse
   */
  public function __invoke(Request $request) {
    $input = $request->all();

    try {
      $addNoteInput = new Input($input);
      $note = $this->action->run($addNoteInput);

      $noteId = $note->getId();

      $files = $request->file('files');
      if ($files) {
        $validator = $this->fileValidator->getValidator($input, 'files');
        if ($validator->fails()) {
          return response()->json(new Fail('', ['errors' => $validator->errors()]));
        }
        foreach($files as $i => $file) {
          $fileInfo = new UploadedFileInfo($file);
          $fileResizer = new UploadedFileResizer($file);
          $fileSaver = new UploadedFileSaver($file);
          $this->fileUploader->upload($fileInfo, $fileResizer, $fileSaver, $input['file_titles'][$i], $noteId);
        }
      }

      $op = $input['op'] ?? null;
      $redirect = ('save_and_more' === $op) ? route('notes.new') : route('notes.view', ['note_id' => $noteId]);
      return response()->json(new Success('Заметка добавлена.', ['redirect' => $redirect]), Response::HTTP_CREATED);
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e), Response::HTTP_BAD_REQUEST);
    }
  }
}
