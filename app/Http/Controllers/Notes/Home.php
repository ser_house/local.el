<?php

namespace App\Http\Controllers\Notes;

use App\Http\Controllers\Controller as BaseController;
use Core\Note\INoteGateway;
use Core\Note\View\NotePresenter;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class Home extends BaseController {
  private INoteGateway $noteGateway;
  private NotePresenter $notePresenter;

  /**
   * Home constructor.
   *
   * @param INoteGateway $noteGateway
   * @param NotePresenter $notePresenter
   */
  public function __construct(INoteGateway $noteGateway, NotePresenter $notePresenter) {
    $this->noteGateway = $noteGateway;
    $this->notePresenter = $notePresenter;
  }


  /**
   * @param Request $request
   *
   * @return Application|Factory|View
   */
  public function __invoke(Request $request) {
    $notes = $this->noteGateway->all();

    $items = [];
    foreach ($notes as $note) {
      $items[] = $this->notePresenter->shortWithFiles($note);
    }

    return view('notes.home', ['notes' => $items]);
  }
}
