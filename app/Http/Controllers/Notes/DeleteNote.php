<?php

namespace App\Http\Controllers\Notes;

use App\Http\Controllers\Controller;
use Core\Note\NoteId;
use Core\Note\UseCase\Delete;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Throwable;

class DeleteNote extends Controller {
  private Delete $action;

  /**
   * DeleteNote constructor.
   *
   * @param Delete $action
   */
  public function __construct(Delete $action) {
    $this->action = $action;
  }


  /**
   * @param Request $request
   * @param string $id
   *
   * @return Application|Redirector|RedirectResponse
   */
  public function __invoke(Request $request, string $id) {
    try {
      $noteId = new NoteId($id);
      $note = $this->action->run($noteId);
      $note_title = $note->getTitle();
      $msg = "Заметка '$note_title' удалена.";

      return redirect('notes')->with('success', $msg);
    }
    catch (Throwable $e) {
      return back()->withException($e);
    }
  }
}
