<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.03.2021
 * Time: 18:14
 */


namespace App\Http\Controllers\Tools;

use App\Http\Controllers\Controller;
use Core\Tools\Midi\Rttl;
use Core\Tools\Midi\RttlToDoor655;
use Illuminate\Http\Request;

class MidiRttl extends Controller {

  public function page() {
    return view('tools.midi_rttl', [
      'rttl' => '',
      'data' => '',
    ]);
  }

  public function convert(Request $request) {
    $midi_file = $request->file('midi');
    $filename = $midi_file->getClientOriginalName();
    $base_filename = pathinfo($filename, PATHINFO_FILENAME);

    $rttl = new Rttl();
    $rttl->importMid($midi_file->getRealPath());
    $text = $rttl->getRttl($base_filename);
    $rttlToDoor655 = new RttlToDoor655($text);

    return view('tools.midi_rttl', [
      'rttl' => $text,
      'data' => $rttlToDoor655->getNotes(),
    ]);
  }
}
