<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.03.2021
 * Time: 18:14
 */


namespace App\Http\Controllers\Tools;

use App\Http\Controllers\Controller;

class Calculators extends Controller {

  /**
   * @inheritDoc
   */
  public function __invoke() {
    return view('tools.calculators');
  }
}
