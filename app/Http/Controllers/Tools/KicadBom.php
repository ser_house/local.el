<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 16.03.2021
 * Time: 18:14
 */


namespace App\Http\Controllers\Tools;

use App\Http\Controllers\Controller;
use Core\Tools\KiCAD\KicadParser;
use Illuminate\Http\Request;

class KicadBom extends Controller {

  public function page() {
    return view('tools.kicad_bom', [
      'components' => [],
    ]);
  }

  public function generate(Request $request) {
    $kicad_file = $request->file('kicad_file');

    $content = $kicad_file->getContent();

    $parser = new KicadParser();
    $components = [];
    $ext = $kicad_file->getClientOriginalExtension();
    switch ($ext) {
      case 'net':
        $components = $parser->getComponentsFromNetFile($content);
        break;

      case 'sch':
        $components = $parser->getComponentsFromSchFile($content);
        break;

      default:
        break;
    }

    return view('tools.kicad_bom', [
      'components' => $components,
    ]);
  }
}
