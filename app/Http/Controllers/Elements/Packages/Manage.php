<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Packages;

use App\Http\Controllers\Controller;
use Core\Elements\Package\IPackageGateway;
use Illuminate\Contracts\View\View;

class Manage extends Controller {

  private IPackageGateway $packageGateway;

  /**
   * Manage constructor.
   *
   * @param IPackageGateway $packageGateway
   */
  public function __construct(IPackageGateway $packageGateway) {
    $this->packageGateway = $packageGateway;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(): View {
    $items = $this->packageGateway->getPackagesWithElementsCount();

    return view('elements.package.manage', [
      'items' => $items,
    ]);
  }
}
