<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Packages;

use App\Http\Controllers\Controller;
use Core\Elements\Package\UseCase\Add\Add as PackageAddService;
use Core\Elements\Package\UseCase\Add\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class Add extends Controller {
  private PackageAddService $service;

  /**
   * Add constructor.
   *
   * @param PackageAddService $service
   */
  public function __construct(PackageAddService $service) {
    $this->service = $service;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $title = $request->get('title');


    try {
      $input = new Input($title);
      $package = $this->service->run($input);
      $package_title = $package->getTitle();
      $msg = "Добавлен новый корпус '$package_title'.";

      return response()->json(new Success($msg, ['item' => $package]));

    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
