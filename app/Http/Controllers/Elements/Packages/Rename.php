<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Packages;

use App\Http\Controllers\Controller;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\UseCase\Rename\Input;
use Core\Elements\Package\UseCase\Rename\Rename as RenameService;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class Rename extends Controller {
  private IPackageGateway $packageGateway;
  private RenameService $service;

  /**
   * Rename constructor.
   *
   * @param IPackageGateway $packageGateway
   * @param RenameService $service
   */
  public function __construct(IPackageGateway $packageGateway, RenameService $service) {
    $this->packageGateway = $packageGateway;
    $this->service = $service;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $package_id = $request->get('id');
    $new_title = $request->get('title');

    try {
      $input = new Input($this->packageGateway, $package_id, $new_title);
      $result = $this->service->run($input);
      $original_package_title = $result->getOriginalPackage()->getTitle();
      $renamedPackage = $result->getRenamedPackage();
      $renamed_package_title = $renamedPackage->getTitle();
      $msg = "Корпус '$original_package_title' переименован в '$renamed_package_title'.";

      return response()->json(new Success($msg, ['item' => $renamedPackage]));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
