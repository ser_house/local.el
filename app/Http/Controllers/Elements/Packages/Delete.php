<?php

namespace App\Http\Controllers\Elements\Packages;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Core\Elements\Package\Exceptions\PackageHasElements;
use Core\Elements\Package\Exceptions\PackageNotFound;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\PackageId;
use Core\Elements\Package\UseCase\Delete\Delete as PackageDeleteService;
use Core\Elements\Package\UseCase\Delete\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Throwable;

class Delete extends Controller {
  private IPackageGateway $packageGateway;
  private PackageDeleteService $service;

  /**
   * Delete constructor.
   *
   * @param IPackageGateway $packageGateway
   * @param PackageDeleteService $service
   */
  public function __construct(IPackageGateway $packageGateway, PackageDeleteService $service) {
    $this->packageGateway = $packageGateway;
    $this->service = $service;
  }

  /**
   * @param Package $package
   *
   * @return JsonResponse
   */
  public function __invoke(Package $package) {
    try {
      $input = new Input($this->packageGateway, new PackageId($package->id));
      $deletedPackage = $this->service->run($input);
      $package_title = $deletedPackage->getTitle();

      return response()->json(new Success("Корпус '$package_title' удален"));
    }
    catch (PackageNotFound $e) {
      return response()->json(new FailThrowable($e), Response::HTTP_NOT_FOUND);
    }
    catch (PackageHasElements $e) {
      return response()->json(new FailThrowable($e), Response::HTTP_CONFLICT);
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e), Response::HTTP_FOUND);
    }
  }
}
