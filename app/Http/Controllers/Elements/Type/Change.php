<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Type;

use App\Http\Controllers\Controller;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\UseCase\Change\Change as TypeChangeService;
use Core\Elements\Type\UseCase\Change\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class Change extends Controller {
  private ITypeGateway $typeGateway;
  private TypeChangeService $service;

  /**
   * Change constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param TypeChangeService $service
   */
  public function __construct(ITypeGateway $typeGateway, TypeChangeService $service) {
    $this->typeGateway = $typeGateway;
    $this->service = $service;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $id = $request->get('id');
    $new_title = $request->get('title');
    $has_series = (bool)$request->get('has_series');
    $units_type = $request->get('units_type');

    try {
      $input = new Input($this->typeGateway, $id, $new_title, $has_series, $units_type);
      $result = $this->service->run($input);

      $originalTitle = $result->getOriginalType()->title();
      $newTitle = $result->getChangedType()->title();
      $has_series_str = $result->getChangedType()->hasSeries() ? 'да' : 'нет';
      $msg = "Тип '$originalTitle' изменён на 'Название: $newTitle, значения по рядам: $has_series_str'.";

      return response()->json(new Success($msg));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
