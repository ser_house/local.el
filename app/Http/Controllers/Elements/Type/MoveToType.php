<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Type;

use App\Http\Controllers\Controller;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\UseCase\MoveToType\Input;
use Core\Elements\Type\UseCase\MoveToType\MoveToType as MoveToTypeService;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class MoveToType extends Controller {

  private ITypeGateway $typeGateway;
  private MoveToTypeService $service;

  /**
   * MoveToType constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param MoveToTypeService $service
   */
  public function __construct(ITypeGateway $typeGateway, MoveToTypeService $service) {
    $this->typeGateway = $typeGateway;
    $this->service = $service;
  }

  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $id = $request->get('id');
    $new_parent_id = $request->get('parent_id');

    try {
      $input = new Input($this->typeGateway, $id, $new_parent_id);
      $result = $this->service->run($input);

      $type_title = $result->getType()->title();
      $parent = $result->getParent();
      if ($parent) {
        $parent_title = $parent->title();
        $msg = "Тип '$type_title' перемещен в '$parent_title'.";
      }
      else {
        $msg = "Тип '$type_title' перемещен в корень.";
      }

      return response()->json(new Success($msg));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
