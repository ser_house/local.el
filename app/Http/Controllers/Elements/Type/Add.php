<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Type;

use App\Http\Controllers\Controller;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\UseCase\Add\Add as AddTypeService;
use Core\Elements\Type\UseCase\Add\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class Add extends Controller {
  private ITypeGateway $typeGateway;
  private AddTypeService $service;

  /**
   * Add constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param AddTypeService $service
   */
  public function __construct(ITypeGateway $typeGateway, AddTypeService $service) {
    $this->typeGateway = $typeGateway;
    $this->service = $service;
  }

  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $title = $request->get('title');
    $parent_id = $request->get('parent_id');
    $has_series = (bool)$request->get('has_series', false);
    $units_type = $request->get('units_type');

    try {
      $input = new Input($this->typeGateway, $title, $has_series, $parent_id, $units_type);
      $type = $this->service->run($input);
      $type_title = $type->title();
      $msg = "Добавлен новый тип '$type_title'.";

      return response()->json(new Success($msg, ['item' => $type]));

    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
