<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Type;

use App\Http\Controllers\Controller;
use App\Models\Type;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\TypeId;
use Core\Elements\Type\UseCase\Delete\Delete as TypeDeleteService;
use Core\Elements\Type\UseCase\Delete\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Throwable;

class Delete extends Controller {
  private ITypeGateway $typeGateway;
  private TypeDeleteService $service;

  /**
   * Delete constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param TypeDeleteService $service
   */
  public function __construct(ITypeGateway $typeGateway, TypeDeleteService $service) {
    $this->typeGateway = $typeGateway;
    $this->service = $service;
  }

  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Type $type): JsonResponse {
    try {
      $input = new Input($this->typeGateway, new TypeId($type->id));
      $removedType = $this->service->run($input);
      $type_title = $removedType->title();

      return response()->json(new Success("Тип '$type_title' удалён."));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
