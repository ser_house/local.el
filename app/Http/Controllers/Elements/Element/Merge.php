<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use Core\Elements\UseCase\Merge\Context;
use Core\Elements\UseCase\Merge\Input;
use Core\Elements\UseCase\Merge\Merge as MergeService;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class Merge extends Controller {
  private Context $context;
  private MergeService $service;

  /**
   * Merge constructor.
   *
   * @param Context $context
   * @param MergeService $service
   */
  public function __construct(Context $context, MergeService $service) {
    $this->context = $context;
    $this->service = $service;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $src_element_id = $request->get('src_element_id');
    $src_package_id = $request->get('src_package_id');
    $target_element_id = $request->get('target_element_id');
    $target_package_id = $request->get('target_package_id');

    try {
      $input = new Input($this->context, $src_element_id, $target_element_id, $src_package_id, $target_package_id);
      $result = $this->service->run($input);

      $src_element_title = $result->getSrcElement()->getTitle();
      $target_element_title = $result->getTargetElement()->getTitle();

      $qty = $result->getQty()->asInt();
      $msg = "Элемент '$src_element_title' объединен с элементом в '$target_element_title'. Новое кол-во: $qty";

      return response()->json(new Success($msg, ['qty' => $qty]));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
