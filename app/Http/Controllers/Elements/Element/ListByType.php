<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use App\Models\Type;
use Core\Elements\IElementGateway;
use Core\Elements\Type\TypeId;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;

class ListByType extends Controller {

  private IElementGateway $elementGateway;

  /**
   * ListByType constructor.
   *
   * @param IElementGateway $elementGateway
   */
  public function __construct(IElementGateway $elementGateway) {
    $this->elementGateway = $elementGateway;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return JsonResponse
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Type $type) {
    $data = $this->elementGateway->listByType(new TypeId($type->id));
    return response()->json(new Success('', ['items' => $data]));
  }
}
