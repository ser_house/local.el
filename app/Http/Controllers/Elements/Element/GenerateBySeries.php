<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Type;
use Core\Elements\UseCase\GenerateBySeries\Context;
use Core\Elements\UseCase\GenerateBySeries\GenerateBySeries as GenerateBySeriesService;
use Core\Elements\UseCase\GenerateBySeries\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class GenerateBySeries extends Controller {
  private Context $context;
  private GenerateBySeriesService $generateService;

  /**
   * GenerateBySeries constructor.
   *
   * @param Context $context
   * @param GenerateBySeriesService $generateService ;
   */
  public function __construct(Context $context, GenerateBySeriesService $generateService) {
    $this->context = $context;
    $this->generateService = $generateService;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request, Package $package, Type $type): JsonResponse {
    $series = $request->get('series');
    $unit = (string)$request->get('unit');
    $multiplier = $request->get('multiplier');
    $qty = $request->get('qty');
    $type_id = $type->id;
    $package_id = $package->id;

    $items = [];
    $total_qty = 0;
    try {
      $input = new Input($this->context, $series, $type_id, $package_id, $unit, $multiplier, $qty);
      $generated_items = $this->generateService->run($input);

      foreach ($generated_items as $generatedItem) {
        $qty = $generatedItem->getQty()->asInt();

        $items[] = $generatedItem;
        $total_qty += $qty;
      }

      $msg = "Сгенерирован ряд '$series', по $qty каждого номинала, всего $total_qty.";

      return response()->json(new Success($msg, ['items' => $items]));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
