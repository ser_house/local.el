<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Core\Elements\IElementGateway;
use Core\Elements\Package\PackageId;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;

class ListWithPackage extends Controller {
  private IElementGateway $elementGateway;

  /**
   * ListWithPackage constructor.
   *
   * @param IElementGateway $elementGateway
   */
  public function __construct(IElementGateway $elementGateway) {
    $this->elementGateway = $elementGateway;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Package $package): JsonResponse {
    $items = $this->elementGateway->listByPackage(new PackageId($package->id));
    $response = new Success('', ['items' => $items]);

    return response()->json($response);
  }
}
