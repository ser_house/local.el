<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use Core\Elements\UseCase\AddPackageQty\AddPackageQty as AddPackageQtyService;
use Core\Elements\UseCase\AddPackageQty\Context;
use Core\Elements\UseCase\AddPackageQty\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class AddPackageQty extends Controller {
  private Context $context;
  private AddPackageQtyService $service;

  /**
   * AddPackageQty constructor.
   *
   * @param Context $context
   * @param AddPackageQtyService $service
   */
  public function __construct(Context $context, AddPackageQtyService $service) {
    $this->context = $context;
    $this->service = $service;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $element_id = $request->get('element_id');
    $package_id = $request->get('package_id');
    $qty = (int)$request->get('qty');


    try {
      $input = new Input($this->context, $element_id, $package_id, $qty);
      $result = $this->service->run($input);
      $element_title = $result->getElement()->getTitle();
      $package_title = $result->getPackage()->getTitle();
      $qty = $result->getQty()->asInt();
      $msg = "Новое кол-во компонентов '$element_title' в корпусе '$package_title': $qty.";
      return response()->json(new Success($msg, [
        'item' => $result,
      ]));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
