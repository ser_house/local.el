<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use Core\Elements\UseCase\ChangeQty\Context;
use Core\Elements\UseCase\ChangeQty\Increase;
use Core\Elements\UseCase\ChangeQty\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class IncreaseQty extends Controller {
  private Context $context;
  private Increase $service;

  /**
   * Decrease constructor.
   *
   * @param Context $context
   * @param Increase $service
   */
  public function __construct(Context $context, Increase $service) {
    $this->context = $context;
    $this->service = $service;
  }

  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $element_id = $request->get('element_id');
    $package_id = $request->get('package_id');
    $qty = $request->get('qty');

    try {
      $input = new Input($this->context, $element_id, $package_id, $qty);
      $result = $this->service->run($input);

      $element_title = $result->getElement()->getTitle();
      $package_title = $result->getPackage()->getTitle();
      $new_qty = $result->getQty()->asInt();

      $msg = "Новое кол-во компонентов '$element_title' в корпусе '$package_title': $new_qty.";

      return response()->json(new Success($msg, ['qty' => $new_qty]));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
