<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use Core\Elements\UseCase\Rename\Context;
use Core\Elements\UseCase\Rename\Input;
use Core\Elements\UseCase\Rename\Rename as RenameService;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Core\View\IdentifiedItemView;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class Rename extends Controller {
  private Context $context;
  private RenameService $service;

  /**
   * Rename constructor.
   *
   * @param Context $context
   * @param RenameService $service
   */
  public function __construct(Context $context, RenameService $service) {
    $this->context = $context;
    $this->service = $service;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $id = $request->get('id');
    $new_title = $request->get('title');

    try {
      $input = new Input($this->context, $id, $new_title);
      $result = $this->service->run($this->context, $input);

      $element = $result->getElement();
      $old_title = $result->getOldTitle();
      $new_title = $element->getTitle();

      $msg = "Элемент '$old_title' переименован в '$new_title'.";

      $item = new IdentifiedItemView($id, $new_title);

      return response()->json(new Success($msg, ['item' => $item]));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
