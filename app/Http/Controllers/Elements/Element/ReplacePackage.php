<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use Core\Elements\UseCase\ReplacePackage\Context;
use Core\Elements\UseCase\ReplacePackage\Input;
use Core\Elements\UseCase\ReplacePackage\ReplacePackage as ReplacePackageService;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class ReplacePackage extends Controller {
  private ReplacePackageService $service;
  private Context $context;

  /**
   * ReplacePackage constructor.
   *
   * @param ReplacePackageService $service
   * @param Context $context
   */
  public function __construct(ReplacePackageService $service, Context $context) {
    $this->service = $service;
    $this->context = $context;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $element_id = $request->get('element_id');
    $current_package_id = $request->get('current_package_id');
    $new_package_id = $request->get('new_package_id');

    try {
      $input = new Input($this->context, $element_id, $current_package_id, $new_package_id);
      $result = $this->service->run($input);

      $element_title = $result->getElement()->getTitle();
      $current_package_title = $result->getCurrentPackage()->getTitle();
      $newPackage = $result->getNewPackage();
      $new_package_title = $newPackage->getTitle();
      $msg = "Корпус '$current_package_title' компонента '$element_title' изменен с '$current_package_title' на '$new_package_title'.";

      return response()->json(new Success($msg, [
        'item' => [
          'id' => (string)$newPackage->getId(),
          'title' => $new_package_title,
        ],
      ]));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
