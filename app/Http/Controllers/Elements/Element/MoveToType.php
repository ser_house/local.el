<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use Core\Elements\UseCase\MoveToType\Context;
use Core\Elements\UseCase\MoveToType\Input;
use Core\Elements\UseCase\MoveToType\MoveToType as MoveToTypeService;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;


class MoveToType extends Controller {

  private Context $context;
  private MoveToTypeService $service;

  /**
   * MoveToType constructor.
   *
   * @param Context $context
   * @param MoveToTypeService $service
   */
  public function __construct(Context $context, MoveToTypeService $service) {
    $this->context = $context;
    $this->service = $service;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $id = $request->get('id');
    $new_type_id = $request->get('type_id');

    try {
      $input = new Input($this->context, $id, $new_type_id);
      $result = $this->service->run($this->context, $input);

      $element_title = $result->getElement()->getTitle();
      $current_type_title = $result->getCurrentType()->title();
      $new_type_title = $result->getNewType()->title();

      return response()->json(new Success("Тип элемента '$element_title' изменен с '$current_type_title' на '$new_type_title'."));
    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
