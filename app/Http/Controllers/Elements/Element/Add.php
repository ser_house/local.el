<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use Core\Elements\UseCase\Add\Add as UseCaseAdd;
use Core\Elements\UseCase\Add\Context;
use Core\Elements\UseCase\Add\Input;
use Core\Infrastructure\Response\Status\FailThrowable;
use Core\Infrastructure\Response\Status\Success;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class Add extends Controller {
  private Context $context;
  private UseCaseAdd $useCase;

  /**
   * Add constructor.
   *
   * @param Context $context
   * @param UseCaseAdd $useCase
   */
  public function __construct(Context $context, UseCaseAdd $useCase) {
    $this->context = $context;
    $this->useCase = $useCase;
  }

  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return mixed
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke(Request $request): JsonResponse {
    $title = $request->get('title');
    $type_id = $request->get('type_id');

    $qty = (int)$request->get('qty');
    $package_id = $request->get('package_id');

    try {
      $input = new Input($this->context, $title, $type_id, $package_id, $qty);
      $elementData = $this->useCase->run($input);

      $package = $elementData->getPackage();
      $package_title = $package->getTitle();
      $qty = $elementData->getQty()->asInt();

      $msg = "Добавлено {$qty}шт. компонента '$title' в корпусе '$package_title'.";

      return response()->json(new Success($msg, ['data' => $elementData]));

    }
    catch (Throwable $e) {
      return response()->json(new FailThrowable($e));
    }
  }
}
