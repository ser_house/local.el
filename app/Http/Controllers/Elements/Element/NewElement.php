<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements\Element;

use App\Http\Controllers\Controller;
use App\Models\Package;
use App\Models\Type;
use Core\View\PresenterSimpleItem;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class NewElement extends Controller {

  private PresenterSimpleItem $presenterSimpleItem;

  /**
   * NewElement constructor.
   *
   * @param PresenterSimpleItem $presenterSimpleItem
   */
  public function __construct(PresenterSimpleItem $presenterSimpleItem) {
    $this->presenterSimpleItem = $presenterSimpleItem;
  }

  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return Application|Factory|View
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke() {
    $element = [
      'id' => '',
      'title' => '',
      'type_id' => null,
      'qty' => 1,
    ];
    $types = Type::all();
    $type_items = $this->presenterSimpleItem->presentMulti($types);

    $packages = Package::all();
    $package_items = $this->presenterSimpleItem->presentMulti($packages);

    return view('elements.new', [
      'element' => $element,
      'types' => $type_items,
      'packages' => $package_items,
    ]);
  }
}
