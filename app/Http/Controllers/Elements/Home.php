<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.12.2020
 * Time: 9:59
 */


namespace App\Http\Controllers\Elements;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Core\Elements\Type\ITypeGateway;
use Core\Elements\Units\UnitsType;
use Core\View\PresenterSimpleItem;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class Home extends Controller {
  private ITypeGateway $typeGateway;
  private PresenterSimpleItem $presenterSimpleItem;

  /**
   * Home constructor.
   *
   * @param ITypeGateway $typeGateway
   * @param PresenterSimpleItem $presenterSimpleItem
   */
  public function __construct(ITypeGateway $typeGateway, PresenterSimpleItem $presenterSimpleItem) {
    $this->typeGateway = $typeGateway;
    $this->presenterSimpleItem = $presenterSimpleItem;
  }


  /**
   * The __invoke method is called when a script tries to call an object as a function.
   *
   * @return Application|Factory|View
   * @link https://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.invoke
   */
  public function __invoke() {
    $types_tree = $this->typeGateway->allAsTree();

    $packages = Package::all()->sortBy('title');
    $package_items = $this->presenterSimpleItem->presentMulti($packages);

    return view('home', [
      'types_tree' => $types_tree,
      'packages' => $package_items,
      'units' => UnitsType::data(),
    ]);
  }
}
