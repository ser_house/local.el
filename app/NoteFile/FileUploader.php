<?php

namespace App\NoteFile;


use Core\File\File;
use Core\File\FileId;
use Core\File\IFileGateway;
use Core\File\IFileUploader;
use Core\File\IFileUrlResolver;
use Core\File\IUploadedFileInfo;
use Core\File\IUploadedFileResizer;
use Core\File\IUploadedFileSaver;
use Core\Id;
use Illuminate\Support\Facades\Storage;

class FileUploader implements IFileUploader {
  private IFileGateway $fileGateway;
  private IFileUrlResolver $fileUrlResolver;

  /**
   * FileUploader constructor.
   *
   * @param IFileGateway $fileGateway
   * @param IFileUrlResolver $fileUrlResolver
   */
  public function __construct(IFileGateway $fileGateway, IFileUrlResolver $fileUrlResolver) {
    $this->fileGateway = $fileGateway;
    $this->fileUrlResolver = $fileUrlResolver;
  }


  public function upload(IUploadedFileInfo $uploadedFileInfo, IUploadedFileResizer $uploadedFileResizer, IUploadedFileSaver $uploadedFileSaver, ?string $human_name, Id $entityId): File {
    $filename = $uploadedFileInfo->getFilename();
    $base_filename = pathinfo($filename, PATHINFO_FILENAME);

    $destination = $this->fileUrlResolver->getTargetUploads();
    $uploadedFileSaver->save($destination, $filename);
    $mime = $uploadedFileInfo->getMime();

    $file = new File(new FileId(), $entityId, $filename, $destination, $human_name ?: $base_filename, $mime);
    $this->fileGateway->add($file);

    if ('application/pdf' !== $mime) {
      $resized = $uploadedFileResizer->resize(null, 128);
      $thumbnail_filename = $this->fileUrlResolver->getFileThumbFileName($file);
      Storage::put("$destination/$thumbnail_filename", $resized);
    }

    return $file;
  }
}
