<?php

namespace App\NoteFile;

use Core\Infrastructure\Formatter\FileSize as FileSizeFormatter;
use Validator;

class FileValidator {
	protected const TYPES = [
		'application/pdf' => 'pdf',
		'image/jpeg' => 'jpg,jpeg',
		'image/png' => 'png',
		'image/webp' => 'webp',
	];

	protected const MAX_FILE_SIZE_BYTES = 1024 * 5;
	protected const MAX_FILESIZE = self::MAX_FILE_SIZE_BYTES * 1024;

	protected FileSizeFormatter $fileSizeFormatter;

  /**
   * FileValidator constructor.
   *
   * @param FileSizeFormatter $fileSizeFormatter
   */
  public function __construct(FileSizeFormatter $fileSizeFormatter) {
    $this->fileSizeFormatter = $fileSizeFormatter;
  }

  /**
   *
   * @return string[]
   */
	public function getHelp(): array {
		$valid_extensions_str = $this->getExtensionsAsString();
		return [
			"Допускаются файлы одного из следующих типов: $valid_extensions_str",
			'Максимальный размер файла: ' . $this->fileSizeFormatter->format(static::MAX_FILESIZE),
		];
	}

  /**
   *
   * @return array
   */
  public function getTypes(): array {
		return static::TYPES;
	}

  /**
   * @param string $delimeter
   *
   * @return string
   */
  public function getMimeTypesAsString(string $delimeter = ','): string {
		return implode($delimeter, array_keys(static::TYPES));
	}

  /**
   *
   * @return array
   */
  public function getExtensions(): array {
		return array_values(static::TYPES);
	}

  /**
   * @param string $delimeter
   *
   * @return string
   */
  public function getExtensionsAsString(string $delimeter = ','): string {
		$valid_extensions = $this->getExtensions();
		return implode($delimeter, $valid_extensions);
	}

  /**
   *
   * @return int
   */
  public function getMaxFileSize(): int {
		return static::MAX_FILESIZE;
	}

  /**
   *
   * @return int
   */
  public function getMaxFileSizeBytes(): int {
		return static::MAX_FILE_SIZE_BYTES;
	}

  /**
   * @param $data
   * @param string $field_name
   *
   * @return \Illuminate\Contracts\Validation\Validator
   */
  public function getValidator($data, string $field_name): \Illuminate\Contracts\Validation\Validator {
		$valid_extensions_str = $this->getExtensionsAsString();
		$mime_types_str = $this->getMimeTypesAsString();
		$max_filesize_bytes = $this->getMaxFileSizeBytes();

		$messages = [
			'max' => 'The file may not be greater than :max kilobytes.',
			'mimes' => 'The file must be a file of type: :values.',
			'mimetypes' => 'The file must be a file of type: :values.',
		];

		return Validator::make($data, [
			$field_name => 'required|array',
			"$field_name.*" => "required|file|mimes:$valid_extensions_str|mimetypes:$mime_types_str|max:$max_filesize_bytes",
		], $messages);
	}
}
