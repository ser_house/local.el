<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Element
 *
 * @property string $id
 * @property string $type_id
 * @property string $title
 * @property Type $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Package[] $packages
 * @property-read int|null $packages_count
 * @method static \Illuminate\Database\Eloquent\Builder|Element newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Element newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Element query()
 * @method static \Illuminate\Database\Eloquent\Builder|Element whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Element whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Element whereTypeId($value)
 * @mixin \Eloquent
 */
class Element extends Model {
  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'element';
  /**
   * The "type" of the auto-incrementing ID.
   *
   * @var string
   */
  protected $keyType = 'string';
  /**
   * @var array
   */
  protected $fillable = ['type_id', 'title'];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function type() {
    return $this->belongsTo('App\Models\Type');
  }

  public function packages() {
    return $this->belongsToMany('App\Models\Package', 'storage', 'element_id', 'package_id');
  }

  public function storages() {
    return $this->belongsToMany('App\Models\Storage', 'storage', 'element_id');
  }
}
