<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Package
 *
 * @property string $id
 * @property string $title
 * @property-read \App\Models\Storage|null $storage
 * @method static Builder|Package newModelQuery()
 * @method static Builder|Package newQuery()
 * @method static Builder|Package query()
 * @method static Builder|Package whereId($value)
 * @method static Builder|Package whereTitle($value)
 * @mixin \Eloquent
 */
class Package extends Model {
  public $timestamps = false;
  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'package';
  /**
   * The "type" of the auto-incrementing ID.
   *
   * @var string
   */
  protected $keyType = 'string';
  /**
   * @var array
   */
  protected $fillable = ['title'];

  public function storage() {
    return $this->hasOne('App\Models\Storage');
  }
}
