<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Note
 *
 * @property string $id
 * @property string $value
 * @property string $title
 * @method static Builder|Element newModelQuery()
 * @method static Builder|Element newQuery()
 * @method static Builder|Element query()
 * @method static Builder|Element whereId($value)
 * @method static Builder|Element whereTitle($value)
 * @method static Builder|Element whereTypeId($value)
 * @mixin \Eloquent
 */
class Note extends Model {
  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'note';
  /**
   * The "type" of the auto-incrementing ID.
   *
   * @var string
   */
  protected $keyType = 'string';
  /**
   * @var array
   */
  protected $fillable = ['value'];

  public function files() {
    return $this->belongsToMany('App\Models\NoteFile', 'note_file', 'note_id');
  }
}
