<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Type
 *
 * @property string $id
 * @property string $title
 * @property string $parent_id
 * @property int $level
 * @property bool $has_series
 * @property string $units_type
 * @property Element[] $elements
 * @property-read int|null $elements_count
 * @method static Builder|Type newModelQuery()
 * @method static Builder|Type newQuery()
 * @method static Builder|Type query()
 * @method static Builder|Type whereId($value)
 * @method static Builder|Type whereTitle($value)
 * @mixin \Eloquent
 */
class Type extends Model {
  public $timestamps = false;
  /**
   * Indicates if the IDs are auto-incrementing.
   *
   * @var bool
   */
  public $incrementing = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'type';
  /**
   * The "type" of the auto-incrementing ID.
   *
   * @var string
   */
  protected $keyType = 'string';
  /**
   * @var array
   */
  protected $fillable = ['title', 'parent_id', 'level', 'has_series', 'units_type'];
}
