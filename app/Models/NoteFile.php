<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\NoteFile
 *
 * @mixin Eloquent
 * @property string $id
 * @property string $note_id
 * @property string $path Внутренний, файловый, путь к файлу.
 * @property string $name Название файла вместе с расширением.
 * @property string $human_name
 * @property string $mime
 * @method static Builder|NoteFile whereCreatedAt($value)
 * @method static Builder|NoteFile whereId($value)
 * @method static Builder|NoteFile whereName($value)
 * @method static Builder|NoteFile wherePath($value)
 * @method static Builder|NoteFile whereUpdatedAt($value)
 * @method static Builder|NoteFile whereHumanName($value)
 * @method static Builder|NoteFile newModelQuery()
 * @method static Builder|NoteFile newQuery()
 * @method static Builder|NoteFile query()
 */
class NoteFile extends Model {
  public $timestamps = false;
  public $incrementing = false;
  protected $table = 'note_file';
  protected $keyType = 'string';

	protected $fillable = ['id', 'note_id', 'path', 'name', 'human_name', 'mime'];
}
