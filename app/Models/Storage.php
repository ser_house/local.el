<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Storage
 *
 * @property string $element_id
 * @property string $package_id
 * @property int $qty
 * @method static Builder|Storage newModelQuery()
 * @method static Builder|Storage newQuery()
 * @method static Builder|Storage query()
 * @method static Builder|Storage whereElementId($value)
 * @method static Builder|Storage whereQty($value)
 * @method static Builder|Storage wherePackageId($value)
 * @mixin \Eloquent
 */
class Storage extends Model {
  public $timestamps = false;
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'storage';
  protected $primaryKey = null;
  public $incrementing = false;
  /**
   * @var array
   */
  protected $fillable = ['element_id', 'package_id', 'qty'];

  public function package() {
    return $this->belongsTo('App\Models\Package');
  }
}
