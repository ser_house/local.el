<?php

namespace App\Providers;

use App\Implement\Elements\ElementGateway;
use App\Implement\Elements\Package\PackageGateway;
use App\Implement\Elements\Storage\StorageGateway;
use App\Implement\Elements\Type\TypeGateway;
use App\Implement\File\UploadedFileInfo;
use App\Implement\File\UploadedFileResizer;
use App\Implement\Infrastructure\Transaction;
use App\Implement\Note\NoteFileGateway;
use App\Implement\Note\NoteGateway;
use Core\Elements\IElementGateway;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Storage\IStorageGateway;
use Core\Elements\Type\ITypeGateway;
use Core\File\IUploadedFileInfo;
use Core\File\IUploadedFileResizer;
use Core\Infrastructure\ITransaction;
use Core\Note\INoteFileGateway;
use Core\Note\INoteGateway;
use Illuminate\Support\ServiceProvider;
use Core\File\IFileGateway;
use App\Implement\File\FileGateway;
use Core\File\IUploadedFileSaver;
use App\Implement\File\UploadedFileSaver;
use Core\File\IFileUrlResolver;
use App\Implement\File\FileUrlResolver;


class AppServiceProvider extends ServiceProvider {
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register() {
    $this->app->bind(INoteGateway::class, NoteGateway::class, true);
    $this->app->bind(INoteFileGateway::class, NoteFileGateway::class, true);
    $this->app->bind(ITypeGateway::class, TypeGateway::class, true);
    $this->app->bind(IPackageGateway::class, PackageGateway::class, true);
    $this->app->bind(IElementGateway::class, ElementGateway::class, true);
    $this->app->bind(IStorageGateway::class, StorageGateway::class, true);
    $this->app->bind(ITransaction::class, Transaction::class, true);
    $this->app->bind(IFileGateway::class, FileGateway::class, true);
    $this->app->bind(IUploadedFileInfo::class, UploadedFileInfo::class, true);
    $this->app->bind(IUploadedFileResizer::class, UploadedFileResizer::class, true);
    $this->app->bind(IUploadedFileSaver::class, UploadedFileSaver::class, true);
    $this->app->bind(IFileUrlResolver::class, FileUrlResolver::class, true);
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot() {
    //
  }
}
