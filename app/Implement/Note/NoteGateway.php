<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.11.2020
 * Time: 13:00
 */


namespace App\Implement\Note;


use Core\Note\INoteGateway;
use Core\Note\Note;
use Core\Note\NoteBuilder;
use Core\Note\NoteId;
use DateTimeImmutable;
use Illuminate\Support\Facades\DB;

class NoteGateway implements INoteGateway {

  /**
   * @param NoteId $noteId
   *
   * @return Note|null
   * @throws \Exception
   */
  public function getById(NoteId $noteId): ?Note {
    $data = DB::select("SELECT * FROM note WHERE id = '$noteId'");
    if (empty($data)) {
      return null;
    }

    return NoteBuilder::buildFromStdObject($data[0]);
  }

  /**
   * @inheritDoc
   */
  public function getByTitle(string $title): ?Note {
    $data = DB::select("SELECT * FROM note WHERE title = '$title'");
    if (empty($data)) {
      return null;
    }

    return NoteBuilder::buildFromStdObject($data[0]);
  }

  /**
   *
   * @return array
   * @throws \Exception
   */
  public function all(): array {
    $data = DB::select('SELECT * FROM note ORDER BY added_at');
    if (empty($data)) {
      return [];
    }

    $notes = [];
    foreach ($data as $datum) {
      $notes[] = NoteBuilder::buildFromStdObject($datum);
    }

    return $notes;
  }


  /**
   * @param Note $note
   */
  public function add(Note $note): void {
    $id = $note->getId();
    $title = $note->getTitle();
    $text = $note->getText();
    $added_at = (new DateTimeImmutable())->format('Y-m-d H:i:s.v');
    DB::insert("INSERT INTO note (id, title, value, added_at) VALUES('$id', '$title', '$text', '$added_at')");
  }

  /**
   * @param Note $note
   */
  public function update(Note $note): void {
    $id = $note->getId();
    $title = $note->getTitle();
    $text = $note->getText();
    DB::update("UPDATE note SET title = '$title', value = '$text' WHERE id = '$id'");
  }

  /**
   * @param NoteId $noteId
   */
  public function remove(NoteId $noteId): void {
    DB::delete("DELETE FROM note WHERE id = '$noteId'");
  }
}
