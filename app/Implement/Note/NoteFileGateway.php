<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 07.04.2021
 * Time: 9:02
 */


namespace App\Implement\Note;


use App\Implement\File\Converter;
use App\Models\NoteFile;
use Core\File\IFileUrlResolver;
use Core\Note\INoteFileGateway;
use Core\Note\NoteId;
use Illuminate\Support\Facades\Storage;

final class NoteFileGateway implements INoteFileGateway {
  private Converter $converter;
  private IFileUrlResolver $fileUrlResolver;

  /**
   * NoteFileGateway constructor.
   *
   * @param Converter $converter
   * @param IFileUrlResolver $fileUrlResolver
   */
  public function __construct(Converter $converter, IFileUrlResolver $fileUrlResolver) {
    $this->converter = $converter;
    $this->fileUrlResolver = $fileUrlResolver;
  }


  /**
   * @inheritDoc
   */
  public function remove(NoteId $noteId): void {
    $models = NoteFile::where('note_id', $noteId)->get();
    foreach ($models as $model) {
      $filename = $model->name;
      $filepath = $model->path;
      Storage::delete("$filepath/$filename");

      $noteFile = $this->converter->modelToEntity($model);
      $thumbnail_filename = $this->fileUrlResolver->getFileThumbFileName($noteFile);
      Storage::delete("$filepath/$thumbnail_filename");

      $model->delete();
    }
  }

  /**
   * @inheritDoc
   */
  public function get(NoteId $noteId): array {
    $models = NoteFile::where('note_id', (string)$noteId)->get();
    $files = [];
    foreach ($models as $model) {
      $files[] = $this->converter->modelToEntity($model);
    }

    return $files;
  }
}
