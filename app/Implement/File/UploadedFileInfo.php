<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 11:53
 */


namespace App\Implement\File;


use Core\File\IUploadedFileInfo;
use Illuminate\Http\UploadedFile;

class UploadedFileInfo implements IUploadedFileInfo {
  private UploadedFile $uploadedFile;

  /**
   * UploadedFileInfo constructor.
   *
   * @param UploadedFile $uploadedFile
   */
  public function __construct(UploadedFile $uploadedFile) {
    $this->uploadedFile = $uploadedFile;
  }

  /**
   * @inheritDoc
   */
  public function getFilename(): string {
    return $this->uploadedFile->getClientOriginalName();
  }

  /**
   * @inheritDoc
   */
  public function getMime(): string {
    return $this->uploadedFile->getMimeType();
  }

  /**
   * @inheritDoc
   */
  public function getExtension(): string {
    return $this->uploadedFile->getExtension();
  }
}
