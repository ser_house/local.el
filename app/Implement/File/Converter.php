<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 14:15
 */


namespace App\Implement\File;


use App\Models\NoteFile;
use Core\File\File;
use Core\File\FileId;
use Core\Id;

class Converter {

  public function modelToEntity(NoteFile $model): File {
    return new File(
      new FileId($model->id),
      new Id($model->note_id),
      $model->name,
      $model->path,
      $model->human_name,
      $model->mime
    );
  }

  public function entityToModel(File $file): NoteFile {
    $model = new NoteFile();
    $model->id = (string)$file->getId();
    $model->note_id = (string)$file->getEntityId();
    $model->name = $file->getFilename();
    $model->path = $file->getPath();
    $model->human_name = $file->getHumanName();
    $model->mime = $file->getMime();
    return $model;
  }
}
