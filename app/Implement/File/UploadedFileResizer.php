<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 11:58
 */


namespace App\Implement\File;


use Core\File\IUploadedFileResizer;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class UploadedFileResizer implements IUploadedFileResizer {
  private UploadedFile $uploadedFile;

  /**
   * UploadedFileResizer constructor.
   *
   * @param UploadedFile $uploadedFile
   */
  public function __construct(UploadedFile $uploadedFile) {
    $this->uploadedFile = $uploadedFile;
  }

  /**
   * @inheritDoc
   */
  public function resize(int $width = null, int $height = null): string {
    $ext = $this->uploadedFile->getExtension();
    return Image::make($this->uploadedFile)->resize(null, $height, function ($constraint) {
      $constraint->aspectRatio();
    })->encode($ext);
  }


}
