<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 13:37
 */


namespace App\Implement\File;


use Core\File\IUploadedFileSaver;
use Illuminate\Http\UploadedFile;

class UploadedFileSaver implements IUploadedFileSaver {
  private UploadedFile $uploadedFile;

  /**
   * UploadedFileSaver constructor.
   *
   * @param UploadedFile $uploadedFile
   */
  public function __construct(UploadedFile $uploadedFile) {
    $this->uploadedFile = $uploadedFile;
  }

  /**
   * @inheritDoc
   */
  public function save(string $destination, string $filename): void {
    $this->uploadedFile->storePubliclyAs($destination, $filename);
  }
}
