<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 09.04.2021
 * Time: 18:37
 */


namespace App\Implement\File;


use Core\File\File;
use Core\File\IFileUrlResolver;
use DateTimeImmutable;
use Illuminate\Support\Facades\Storage;

class FileUrlResolver implements IFileUrlResolver {
  /**
   * @inheritDoc
   */
  public function getTargetUploads(): string {
    $date = new DateTimeImmutable();

    return 'public/uploads/' . $date->format('Y');
  }

  /**
   * @inheritDoc
   */
  public function resolveFileUrl(File $file): string {
    $path = $file->getPath();
    $name = $file->getFilename();

    return Storage::url("$path/$name");
  }

  /**
   * @inheritDoc
   */
  public function resolveFileThumbUrl(File $file): string {
    $path = $file->getPath();
    if ('application/pdf' !== $file->getMime()) {
      $thumb_filename = $this->getFileThumbFileName($file);

      return Storage::url("$path/$thumb_filename");
    }

    return $this->getPdfThumbUrl();
  }

  /**
   * @inheritDoc
   */
  public function getFileThumbFileName(File $file): string {
    $filename = $file->getFilename();
    $base_filename = pathinfo($filename, PATHINFO_FILENAME);
    $ext = pathinfo($filename, PATHINFO_EXTENSION);

    return "{$base_filename}_thumb.$ext";
  }

  /**
   * @inheritDoc
   */
  public function getPdfThumbUrl(): string {
    $path = 'public/' . IFileUrlResolver::PDF_THUMBNAIL_FILE;
    return Storage::url($path);
  }


}
