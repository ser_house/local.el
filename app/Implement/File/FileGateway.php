<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 08.04.2021
 * Time: 12:21
 */


namespace App\Implement\File;


use Core\File\File;
use Core\File\IFileGateway;

class FileGateway implements IFileGateway {
  private Converter $converter;

  /**
   * FileGateway constructor.
   *
   * @param Converter $converter
   */
  public function __construct(Converter $converter) {
    $this->converter = $converter;
  }


  /**
   * @inheritDoc
   */
  public function add(File $file): void {
    $model = $this->converter->entityToModel($file);
    $model->save();
  }
}
