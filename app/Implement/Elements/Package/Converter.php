<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 13:17
 */


namespace App\Implement\Elements\Package;


use App\Models\Package as Model;
use Core\Elements\Package\Package as Entity;
use Core\Elements\Package\PackageId;

class Converter {
  public function modelToEntity(Model $model): Entity {
    return new Entity(new PackageId($model->id), $model->title);
  }

  public function entityToModel(Entity $entity): Model {
    $model = new Model();
    $model->id = (string)$entity->getId();
    $model->title = $entity->getTitle();
    return $model;
  }
}
