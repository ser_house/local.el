<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 18.01.2021
 * Time: 19:59
 */


namespace App\Implement\Elements\Package;


use App\Models\Package as PackageModel;
use App\Models\Storage;
use Core\Elements\Package\IPackageGateway;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;
use Core\Elements\Package\PackageWithElementsQty;
use Core\Elements\Qty;
use Illuminate\Support\Facades\DB;

class PackageGateway implements IPackageGateway {
  /**
   * @inheritDoc
   */
  public function add(Package $package): void {
    $model = new PackageModel();
    $model->id = (string)$package->getId();
    $model->title = $package->getTitle();
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function getById(PackageId $packageId): ?Package {
    $model = PackageModel::find($packageId);
    if (!$model) {
      return null;
    }

    return new Package(new PackageId($model->id), $model->title);
  }

  /**
   * @inheritDoc
   */
  public function getByTitle(string $title): ?Package {
    $model = PackageModel::where(['title' => $title])->first();
    if (!$model) {
      return null;
    }

    return new Package(new PackageId($model->id), $model->title);
  }

  /**
   * @inheritDoc
   */
  public function getElementsCount(PackageId $packageId): int {
    return Storage::wherePackageId($packageId)->count();
  }

  /**
   * @inheritDoc
   */
  public function getPackagesWithElementsCount(): array {
    $sql = 'SELECT
      package.id AS package_id,
      package.title AS package_title,
      COUNT(storage.element_id) AS qty
    FROM package
    LEFT JOIN storage ON storage.package_id = package.id
    GROUP BY package.id
    ORDER BY package.title';
    $data = \DB::select($sql);

    $items = [];

    foreach ($data as $datum) {
      $package = new Package(new PackageId($datum->package_id), $datum->package_title);
      $qty = new Qty($datum->qty);
      $items[] = new PackageWithElementsQty($package, $qty);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function update(Package $package): void {
    $model = new PackageModel();
    $model->id = (string)$package->getId();
    $model->title = $package->getTitle();
    $model->exists = true;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function replace(PackageId $targetPackageId, PackageId $newPackageId): void {
    $sql = "UPDATE storage SET package_id = '$targetPackageId' WHERE package_id = '$newPackageId'";
    DB::update($sql);
  }

  /**
   * @inheritDoc
   */
  public function remove(PackageId $packageId): void {
    $model = PackageModel::find($packageId);
    $model->delete();
  }
}
