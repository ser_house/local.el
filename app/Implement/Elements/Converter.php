<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 9:10
 */


namespace App\Implement\Elements;

use App\Models\Element as Model;
use Core\Elements\Element as Entity;
use Core\Elements\ElementId;
use Core\Elements\Type\TypeId;

class Converter {

  public function modelToEntity(Model $model): Entity {
    return new Entity(new ElementId($model->id), $model->title, new TypeId($model->type_id));
  }

  public function entityToModel(Entity $entity): Model {
    $model = new Model();
    $model->id = (string)$entity->getId();
    $model->title = $entity->getTitle();
    $model->type_id = (string)$entity->getTypeId();

    return $model;
  }
}
