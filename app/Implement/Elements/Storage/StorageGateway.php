<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 9:48
 */


namespace App\Implement\Elements\Storage;


use App\Models\Storage;
use Core\Elements\ElementId;
use Core\Elements\Package\PackageId;
use Core\Elements\Qty;
use Core\Elements\Storage\IStorageGateway;
use DB;

class StorageGateway implements IStorageGateway {
  /**
   * @inheritDoc
   */
  public function has(ElementId $elementId, PackageId $packageId): bool {
    $storage = Storage::where([
      'element_id' => (string)$elementId,
      'package_id' => (string)$packageId]
    )->first();
    return (bool)$storage;
  }

  /**
   * @inheritDoc
   */
  public function getQty(ElementId $elementId, PackageId $packageId): ?Qty {
    $storage = Storage::where([
      'element_id' => (string)$elementId,
      'package_id' => (string)$packageId]
    )->first();
    return $storage ? new Qty($storage->qty) : null;
  }

  /**
   * @inheritDoc
   */
  public function addQty(ElementId $elementId, PackageId $packageId, Qty $qty): void {
    $storage = new Storage;
    $storage->element_id = (string)$elementId;
    $storage->package_id = (string)$packageId;
    $storage->qty = $qty->asInt();
    $storage->save();
  }

  /**
   * @inheritDoc
   */
  public function updateQty(ElementId $elementId, PackageId $packageId, Qty $qty): void {
    DB::table('storage')->where([
      'element_id' => (string)$elementId,
      'package_id' => (string)$packageId]
    )->update(['qty' => $qty->asInt()]);
  }

  /**
   * @inheritDoc
   */
  public function replacePackage(ElementId $elementId, PackageId $currentPackageId, PackageId $newPackageId): void {
    DB::table('storage')->where([
      'element_id' => (string)$elementId,
      'package_id' => (string)$currentPackageId,
      ])->update(['package_id' => (string)$newPackageId]);
  }

  /**
   * @inheritDoc
   */
  public function remove(ElementId $elementId, PackageId $packageId): void {
    DB::table('storage')->where([
        'element_id' => (string)$elementId,
        'package_id' => (string)$packageId]
    )->delete();
  }


}
