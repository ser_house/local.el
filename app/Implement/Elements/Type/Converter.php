<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 13:17
 */


namespace App\Implement\Elements\Type;


use App\Models\Type as Model;
use Core\Elements\Type\Type as Entity;
use Core\Elements\Type\TypeId;
use Core\Elements\Units\UnitsType;

class Converter {
  public function modelToEntity(Model $model): Entity {
    $parentId = $model->parent_id ? new TypeId($model->parent_id) : null;
    $unitsType = $model->units_type ? new UnitsType($model->units_type): null;
    return new Entity(new TypeId($model->id), $parentId, $model->title, $model->has_series, $unitsType);
  }

  public function entityToModel(Entity $entity): Model {
    $model = new Model();
    $model->id = (string)$entity->id();
    $model->title = $entity->title();
    $model->has_series = $entity->hasSeries();
    $parentId = $entity->parentId();
    $model->parent_id = $parentId ? (string)$parentId : null;
    $unitsType = $entity->unitsType();
    $model->units_type = $unitsType ? (string)$unitsType : null;
    return $model;
  }
}
