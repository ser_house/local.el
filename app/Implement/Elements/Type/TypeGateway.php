<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 18.01.2021
 * Time: 19:59
 */


namespace App\Implement\Elements\Type;


use Core\Elements\Type\ITypeGateway;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;
use Illuminate\Support\Facades\DB;
use App\Models\Type as TypeModel;

class TypeGateway implements ITypeGateway {
  private Converter $converter;

  /**
   * TypeGateway constructor.
   *
   * @param Converter $converter
   */
  public function __construct(Converter $converter) {
    $this->converter = $converter;
  }


  /**
   * @inheritDoc
   */
  public function add(Type $type): void {
    $model = $this->converter->entityToModel($type);
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function getById(TypeId $typeId): ?Type {
    $model = TypeModel::find($typeId);
    if (!$model) {
      return null;
    }

    return $this->converter->modelToEntity($model);
  }

  /**
   * @inheritDoc
   */
  public function getByTitle(string $title): ?Type {
    $model = TypeModel::where(['title' => $title])->first();
    if (!$model) {
      return null;
    }

    return $this->converter->modelToEntity($model);
  }

  /**
   * @inheritDoc
   */
  public function getElementsCount(TypeId $typeId): int {
    $sql = "SELECT COUNT(*) AS num FROM element WHERE type_id = '$typeId'";
    $data = DB::select($sql);
    return (int)$data[0]->num;
  }

  /**
   * @inheritDoc
   */
  public function getChildrenIds(TypeId $typeId): array {
    $sql = "SELECT id FROM type WHERE parent_id = '$typeId'";
    $data = DB::select($sql);
    $type_ids = [];
    foreach($data as $datum) {
      $type_ids[] = new TypeId($datum->id);
    }
    return $type_ids;
  }

  /**
   * @inheritDoc
   */
  public function allAsFlat(): array {
    $db_items = DB::select('SELECT * FROM type ORDER BY title');
    if (empty($db_items)) {
      return [];
    }

    return $db_items;
  }

  /**
   * @inheritDoc
   */
  public function allAsTree(): array {
    $sql = 'WITH RECURSIVE a AS (
  SELECT id, parent_id, title, has_series, units_type
  FROM type
  WHERE parent_id IS NULL
  UNION ALL
  SELECT child.id, child.parent_id, child.title, child.has_series, child.units_type
  FROM type AS child
  JOIN a ON a.id = child.parent_id
  WHERE child.parent_id IS NOT NULL)
SELECT * FROM a ORDER BY title';

    $db_items = \DB::select($sql);
    if (empty($db_items)) {
      return [];
    }

    return $this->buildtree($db_items);
  }

  /**
   * @inheritDoc
   */
  public function update(Type $type): void {
    $model = $this->converter->entityToModel($type);
    $model->exists = true;
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function remove(TypeId $typeId): void {
    $model = TypeModel::find($typeId);
    $model->delete();
  }

  /**
   * @param array $items
   * @param string|null $parent_id
   *
   * @return array
   */
  private function buildTree(array &$items, string $parent_id = null): array {
    $branch = [];

    foreach ($items as $element) {
      if ($element->parent_id === $parent_id) {
        $element->children = $this->buildTree($items, $element->id);
        $branch[$element->id] = $element;
        unset($items[$element->id]);
      }
    }

    return $branch;
  }
}
