<?php
/**
 * Created by PhpStorm.
 * User: Vasiliy Matyukhov (matyukhov@gmail.com)
 * Date: 03.04.2021
 * Time: 9:07
 */


namespace App\Implement\Elements;


use App\Models\Element as Model;
use Core\Elements\Element;
use Core\Elements\ElementData;
use Core\Elements\ElementId;
use Core\Elements\IElementGateway;
use Core\Elements\Package\Package;
use Core\Elements\Package\PackageId;
use Core\Elements\Qty;
use Core\Elements\Type\Type;
use Core\Elements\Type\TypeId;
use Core\Elements\Units\UnitsType;
use DB;

class ElementGateway implements IElementGateway {

  private Converter $converter;

  /**
   * ElementGateway constructor.
   *
   * @param Converter $converter
   */
  public function __construct(Converter $converter) {
    $this->converter = $converter;
  }

  /**
   * @inheritDoc
   */
  public function add(Element $element): void {
    $model = $this->converter->entityToModel($element);
    $model->save();
  }

  /**
   * @inheritDoc
   */
  public function getById(ElementId $elementId): ?Element {
    $model = Model::find((string)$elementId);

    return $model ? $this->converter->modelToEntity($model) : null;
  }

  /**
   * @inheritDoc
   */
  public function getByTitleAndType(string $title, TypeId $typeId): ?Element {
    $model = Model::where(['title' => $title, 'type_id' => (string)$typeId])->first();

    return $model ? $this->converter->modelToEntity($model) : null;
  }

  /**
   * @inheritDoc
   */
  public function listByType(TypeId $typeId): array {
    $sql = 'SELECT
       element.id AS element_id,
       element.title AS element_title,
       type.id AS type_id,
       type.parent_id AS type_parent_id,
       type.title AS type_title,
       type.has_series AS type_has_series,
       type.units_type AS type_units_type,
       storage.qty,
       package.id AS package_id,
       package.title AS package_title
    FROM element
    INNER JOIN type ON type.id = element.type_id
    LEFT JOIN storage ON storage.element_id = element.id
    INNER JOIN package ON package.id = storage.package_id
    WHERE element.type_id = :type_id
    ORDER BY type_title, element.title';
    $data = DB::select($sql, ['type_id' => (string)$typeId]);
    $items = [];

    foreach ($data as $datum) {
      $typeId = new TypeId($datum->type_id);
      $element = new Element(new ElementId($datum->element_id), $datum->element_title, $typeId);
      $parentId = $datum->type_parent_id ? new TypeId($datum->type_parent_id) : null;
      $unitsType = $datum->type_units_type ? new UnitsType($datum->type_units_type) : null;
      $type = new Type($typeId, $parentId, $datum->type_title, $datum->type_has_series, $unitsType);
      $package = new Package(new PackageId($datum->package_id), $datum->package_title);
      $qty = new Qty($datum->qty);
      $items[] = new ElementData($element, $type, $package, $qty);
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function listByPackage(PackageId $packageId): array {
    $package_id = (string)$packageId;
    $sql = "SELECT element.title
    FROM storage
    INNER JOIN element ON element.id = storage.element_id
    WHERE storage.package_id = '$package_id'
    ORDER BY element.title";
    $data = DB::select($sql);

    $items = [];
    foreach ($data as $datum) {
      $items[] = $datum->title;
    }

    return $items;
  }

  /**
   * @inheritDoc
   */
  public function remove(ElementId $elementId): void {
    DB::table('element')->where(['id' => (string)$elementId])->delete();
  }

  /**
   * @inheritDoc
   */
  public function update(Element $element): void {
    $model = $this->converter->entityToModel($element);
    $model->exists = true;
    $model->save();
  }


}
