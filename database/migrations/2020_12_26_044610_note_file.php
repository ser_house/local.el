<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NoteFile extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('note_file', function (Blueprint $table) {
      $table->char('id', 26)->primary();
      $table->char('note_id', 26);
      $table->string('path', 1024)->comment = 'Внутренний, файловый, путь к файлу.';
      $table->string('name', 255)->comment = 'Название файла вместе с расширением.';
      $table->string('human_name')->default('');
      $table->string('mime', 255);

      $table->foreign('note_id')->references('id')->on('note')->onDelete('cascade')->onUpdate('restrict');
    });

    $path = resource_path() . '/img/pdf_thumbnail.png';
    if (File::exists($path)) {
      Storage::putFileAs('public', $path, 'pdf_thumbnail.png');
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('note_file');
    // Закомментировал, чтобы после тестов не удалялся.
    //Storage::delete('public/pdf_thumbnail.png');
  }
}
