<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ulid\Ulid;

class Init extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('type', function (Blueprint $table) {
      $table->char('id', 26)->primary();
      $table->string('title')->unique();
    });
    Schema::create('package', function (Blueprint $table) {
      $table->char('id', 26)->primary();
      $table->string('title')->unique();
    });
    Schema::create('element', function (Blueprint $table) {
      $table->char('id', 26)->primary();
      $table->char('type_id', 26);
      $table->string('title');

      $table->unique(['type_id', 'title'], 'element_unique');
      $table->foreign('type_id')->references('id')->on('type')->onDelete('cascade')->onUpdate('restrict');
    });

    Schema::create('storage', function (Blueprint $table) {
      $table->char('element_id', 26);
      $table->char('package_id', 26);
      $table->unsignedInteger('qty');

      $table->foreign('element_id')->references('id')->on('element')->onDelete('cascade')->onUpdate('restrict');
      $table->foreign('package_id')->references('id')->on('package')->onDelete('cascade')->onUpdate('restrict');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('storage');
    Schema::dropIfExists('element');
    Schema::dropIfExists('package');
    Schema::dropIfExists('type');
  }
}
