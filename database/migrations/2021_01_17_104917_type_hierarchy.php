<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Ulid\Ulid;

class TypeHierarchy extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('type', function (Blueprint $table) {
      $table->char('parent_id', 26)->nullable();
      $table->unsignedTinyInteger('level')->default(0);

      $table->foreign('parent_id')
        ->references('id')->on('type')
        ->onDelete('cascade')
        ->onUpdate('restrict');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('type', function (Blueprint $table) {
      $table->dropColumn('parent_id');
      $table->dropColumn('level');
    });
  }
}
