<?php

namespace Database\Seeders;

use App\Models\Element;
use App\Models\Package;
use App\Models\Storage;
use App\Models\Type;
use Illuminate\Database\Seeder;

class ElementSeeder extends Seeder {
  public function run() {
    $type = Type::find('type_id_1');

    $element = new Element();
    $element->id = 'element_id_1';
    $element->title = 'Тестовый компонент 1';
    $element->type_id = $type->id;
    $element->save();

    $package = Package::find('package_id_1');
    $storage = new Storage;
    $storage->element_id = $element->id;
    $storage->package_id = $package->id;
    $storage->qty = 5;

    $storage->save();

    $element = new Element();
    $element->id = 'element_id_2';
    $element->title = 'Тестовый компонент 2';
    $element->type_id = $type->id;
    $element->save();

    $package = Package::find('package_id_2');
    $storage = new Storage;
    $storage->element_id = $element->id;
    $storage->package_id = $package->id;
    $storage->qty = 6;
    $storage->save();

    $type = Type::find('type_id_2');
    $element = new Element();
    $element->id = 'element_id_3';
    $element->title = 'Тестовый компонент 3';
    $element->type_id = $type->id;
    $element->save();
  }
}
