<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Seeder;
use Ulid\Ulid;

class PackageSeeder extends Seeder {
  public function run() {
    $package = new Package();
    $package->id = 'package_id_1';
    $package->title = 'Тестовый корпус 1';
    $package->save();

    $package = new Package();
    $package->id = 'package_id_2';
    $package->title = 'Тестовый корпус 2';
    $package->save();

    $package = new Package();
    $package->id = 'package_id_e24';
    $package->title = 'Корпус для E24';
    $package->save();
  }
}
