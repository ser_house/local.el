<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder {
  public function run() {
    $type = new Type();
    $type->id = 'type_id_1';
    $type->title = 'Тестовый тип';
    $type->save();

    $type = new Type();
    $type->id = 'type_id_2';
    $type->title = 'Тестовый тип 2';
    $type->save();

    $type = new Type();
    $type->id = 'type_id_e24';
    $type->title = 'Тип для E24';
    $type->has_series = true;
    $type->units_type = 'resistance';
    $type->save();
  }
}
